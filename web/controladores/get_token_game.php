<?php
session_start();

require_once("conexion.php");
require_once("tools.php");

$id_token = $_POST["id_token"];

$id_usuario_actual = $_SESSION["id_usuario"];

$array_respuesta = array();

$consulta1 = "SELECT id_tokenjuego, juegos, configuracion1, configuracion2, configuracion3, configuracion4, configuracion5, configuracion6, configuracion7, configuracion8, configurable, id_usuario_creador, fecha_creacion, fecha_modificacion FROM tokenjuegos WHERE id_tokenjuego = $id_token";

$query1 = mysqli_query($conexion,$consulta1);

if($query1){
	
	if(mysqli_num_rows($query1)) {
		
		
		$resultado = mysqli_fetch_array($query1);
		
		echo json_encode(array("id_tokenjuego"=>$resultado["id_tokenjuego"],
							   "juegos"=>$resultado["juegos"],
							   "configuracion1"=>$resultado["configuracion1"],
							   "configuracion2"=>$resultado["configuracion2"],
							   "configuracion3"=>$resultado["configuracion3"],
							   "configuracion4"=>$resultado["configuracion4"],
							   "configuracion5"=>$resultado["configuracion5"],
							   "configuracion6"=>$resultado["configuracion6"],
							   "configuracion7"=>$resultado["configuracion7"],
							   "configuracion8"=>$resultado["configuracion8"],
							   "configurable"=>$resultado["configurable"],
							   "id_usuario_creador"=>$resultado["id_usuario_creador"],
							   "id_usuario_actual"=>$id_usuario_actual,
							   "fecha_creacion"=>$resultado["fecha_creacion"],
							   "fecha_modificacion"=>$resultado["fecha_modificacion"]
							  
							  ));
		
	} // si consigue resultados
	else{
		echo "error1";
	}
	
	
} // si se completa la query

else{
	echo "error2";
}


?>