<?php

require_once("conexion.php");

$bugs_0 = "SELECT COUNT(id_bug) as cantidad FROM bugs WHERE estado = 0";
$bugs_1 = "SELECT COUNT(id_bug) as cantidad FROM bugs WHERE estado = 1";
$bugs_2 = "SELECT COUNT(id_bug) as cantidad FROM bugs WHERE estado = 2";


$query1 = mysqli_query($conexion, $bugs_0);
$query2 = mysqli_query($conexion, $bugs_1);
$query3 = mysqli_query($conexion, $bugs_2);


$resultado1 = mysqli_fetch_array($query1);
$resultado2 = mysqli_fetch_array($query2);
$resultado3 = mysqli_fetch_array($query3);

echo json_encode(array(
	"estado_0" => $resultado1["cantidad"],
	"estado_1" => $resultado2["cantidad"],
	"estado_2" => $resultado3["cantidad"]
)); 

