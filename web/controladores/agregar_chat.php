<?php
session_start();

require_once("conexion.php");
require_once("tools.php");


$id_usuario = $_SESSION["id_usuario"];

$id_amigo = $_POST["id_amigo"];


// ** CONSULTAR AMIGOS ACTUALES **
$consulta_existente = "SELECT chats FROM usuarios WHERE id_usuario = $id_usuario";

$query = mysqli_query($conexion, $consulta_existente);

if($query){
	
	$resultado = mysqli_fetch_array($query);
	
	// AGREGAR AMIGOS AL ARRAY PARA COMPARARLOS
	$amigos = explode(",",$resultado["chats"]);
	
	// BUSCAR AMIGO EXISTENTE
	if(array_search($id_amigo, $amigos) === false){
		
		array_push($amigos,$id_amigo);
		
		$nuevos_amigos = join(",",$amigos);
		$nuevos_amigos = trim($nuevos_amigos,",");
		
		// *** INGRESAR NUEVO AMIGO ***
		$consulta_nuevos_amigos = "UPDATE usuarios SET chats = '$nuevos_amigos' WHERE id_usuario = $id_usuario";
		
		$query_nuevos_amigos = mysqli_query($conexion, $consulta_nuevos_amigos);
		
		if($query_nuevos_amigos){
			echo "1"; // nuevo chat agregado
			exit();
		}
		else{
			echo mysqli_error($conexion);
			echo "2"; // error abriendo chat
			exit();
		}
		
	}
	else{
		echo "3"; // ya lo tiene en chat
		exit();
	}
	
}
else{
	echo mysqli_error($conexion);
}

?>