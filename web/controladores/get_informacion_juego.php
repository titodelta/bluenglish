<?php
session_start();

require_once("conexion.php");
require_once("tools.php");


$id_game = $_POST["id_game"];

$array_juegos = array();    // array de juegos



// Cconsulta basica del juego
$consulta1 = "SELECT * FROM juegos WHERE id_juego in ($id_game)";

$query1 = mysqli_query($conexion, $consulta1);

if($query1){
    
    
   while($resultado1 = mysqli_fetch_array($query1)){
    
    
	 $id_juego = $resultado1["id_juego"];
    
    // Consulta de las preguntas
    $consulta2 = "SELECT * FROM preguntas WHERE id_juego = $id_juego"; 
    $query2 = mysqli_query($conexion,$consulta2);
    
    
    
    $array_preguntas = array();  // array de preguntas
    
    
    if($query2){
        
        while($resultado2 = mysqli_fetch_array($query2)){   // preguntas
            
        
              
            
                $id_pregunta = $resultado2["id_pregunta"];
            
            
                $consulta3 = "SELECT * FROM respuestas WHERE id_pregunta = $id_pregunta"; 
    
                $query3 = mysqli_query($conexion,$consulta3);
            
                
                $array_respuestas = array();  // array de respuestas
                
                if($query3){
                    
                    while($resultado3 = mysqli_fetch_array($query3)){   // respuestas
                     
                        
                        // agregar las respuestas en un array
                        
                        array_push($array_respuestas,array(
                            "id_respuesta"=> $resultado3["id_respuesta"],
                            "respuesta"=> $resultado3["respuesta"],
                            "id_pregunta"=> $resultado3["id_pregunta"],
                            "fecha_creacion"=> $resultado3["fecha_creacion"],
                            "fecha_modificacion"=> $resultado3["fecha_modificacion"]
                        ));
                        
                        
                        
                    }
                    
                }
                

                // agregar las pregunas en un array que contienen al final las respuestas en array
            
                array_push($array_preguntas,array(
                    "id_pregunta"=> $resultado2["id_pregunta"],
                    "pregunta"=> $resultado2["pregunta"],
                    "imagen"=> $resultado2["imagen"],
                    "audio"=> $resultado2["audio"],
                    "id_juego"=> $resultado2["id_juego"],
                    "fecha_creacion"=> $resultado2["fecha_creacion"],
                    "fecha_modificacion"=> $resultado2["fecha_modificacion"],
                    "respuestas"=> $array_respuestas
                ));
            
            
            
            
            
        } // fin while preguntas
        
    } // fin Query2
    
    
    
                    // ingreso de datos basicos de juego, y agregar preguntas que contienen respuestas
    
                    array_push($array_juegos,array(
                    "id_juego"=>$resultado1["id_juego"],
                    "nombre"=>$resultado1["nombre"],
                    "descripcion"=>$resultado1["descripcion"],
                    "idioma"=>$resultado1["idioma"],
                    "tipo"=>$resultado1["tipo"],
                    "quiz"=>$resultado1["quiz"],
                    "posicion"=>$resultado1["posicion"],
                    "usuario"=>$resultado1["id_usuario"],
                    "modificaciones"=>$resultado1["modificaciones"],
                    "fecha_creacion"=>$resultado1["fecha_creacion"],
                    "fecha_modificacion"=>$resultado1["fecha_modificacion"],
                    "preguntas"=> $array_preguntas
                    ));
    
    
    
    
    } // fin de while resultado1 busqueda de todos los juegos
    
    
} // fin query1 de juego




echo json_encode($array_juegos);


?>