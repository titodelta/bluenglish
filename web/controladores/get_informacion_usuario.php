<?php
session_start();

require_once("conexion.php");
require_once("tools.php");


// ** VARIABLES EXTERNAS ** //
// * id_usuario  ->  informacion de un usuario o varios separados por coma(,). Si no se declara se mostrara info del solicitante
// * todos -> valor boleano, true para traer todos los resultados
// * palabras -> valor boleano, true para traer palabras

// USUARIO
$id_usuario = $_SESSION["id_usuario"];

$id_consultante = $_SESSION["id_usuario"];

$busqueda_con_parametros = false;



// *** VERIFICAR ID_USUARIO COMO PARAMETRO ***
if(isset($_POST["id_usuario"]) && !empty($_POST["id_usuario"])){
	
 	$id_usuario = $_POST["id_usuario"];	
	$busqueda_con_parametros = true;
}


// SUBCONSULTA
$subconsulta = "WHERE id_usuario in ($id_usuario)";

// ** MOSTRAR TODOS LOS USUARIOS ***
if(isset($_POST["todos"]) && !empty($_POST["todos"]) && $_POST["todos"] == 'true'){
	
		$subconsulta = '';	
		$busqueda_con_parametros = true;
}


// PALABRAS
$palabras = false;

// ** MOSTRAR TODOS LOS USUARIOS ***
if(isset($_POST["palabras"]) && !empty($_POST["palabras"]) && $_POST["palabras"] == 'true'){
	
	$palabras = true;
}


// MODIFICAR ULTIMA ACTIVIDAD
if($busqueda_con_parametros == false){
	
	update_ultima_conexion($id_consultante);
}


// *** CONSULTA SQL USUARIOS ***

$consulta = "SELECT id_usuario, rango, nombre, usuario, password, email, id_avatar, fecha_creacion, fecha_modificacion, nivel, apellido, pais, ciudad, nickname, aciertos, errores, telefono, fecha_nacimiento, racha, id_tokenjuego_actual, amigos, bloqueados, chats, ultima_conexion FROM usuarios $subconsulta";

$query = mysqli_query($conexion, $consulta);

$array_usuarios = array();

$cantidad_juegos = get_cantidad_juegos();

if($query){

	while($resultado = mysqli_fetch_array($query)){

		array_push($array_usuarios, array("id_usuario"=>$resultado["id_usuario"],
										  "rango"=>(int)$resultado["rango"],
										  "nombre"=>$resultado["nombre"],
										  "usuario"=>$resultado["usuario"],
										  "password"=>$resultado["password"],
										  "email"=>$resultado["email"],
										  "id_avatar"=>(int)$resultado["id_avatar"],
										  "fecha_creacion"=>$resultado["fecha_creacion"],
										  "fecha_modificacion"=>$resultado["fecha_modificacion"],
										  "nivel"=>$resultado["nivel"],
										  "apellido"=>$resultado["apellido"],
										  "pais"=>$resultado["pais"],
										  "ciudad"=>$resultado["ciudad"],
										  "nickname"=>$resultado["nickname"],
										  "aciertos"=>(int)$resultado["aciertos"],
										  "errores"=>(int)$resultado["errores"],
										  "telefono"=>$resultado["telefono"],
										  "fecha_nacimiento"=>$resultado["fecha_nacimiento"],
										  "cantidad_juegos"=> (int)$cantidad_juegos,
										  "cantidad_juegos_aprendidos"=> (int)get_cantidad_juegos_aprendidos($resultado["id_usuario"]),
										  "cantidad_juegos_completados"=> (int)get_cantidad_juegos_completados($resultado["id_usuario"]),
										  "promedio"=> round(get_promedio_velocidad($resultado["id_usuario"]),2),
										  "racha"=> (int)$resultado["racha"],
										  "id_consultante"=>(int)$_SESSION["id_usuario"],
										  "id_tokenjuego_actual"=>$resultado["id_tokenjuego_actual"],
										  "cantidad_palabras"=>get_cantidad_palabras($resultado["id_usuario"]),
										  "palabras"=> $palabras == true ? get_palabras($resultado["id_usuario"]): null,
										  "ultima_conexion"=>get_segundos_ultima_conexion((int)$resultado["ultima_conexion"]),
										  "amigos"=>$resultado["amigos"],
										  "cantidad_amigos"=>get_cantidad_amigos($resultado["id_usuario"]),
										  "bloqueados"=>$resultado["bloqueados"],
										  "chats"=>$resultado["chats"],
										  "mensajes_pendientes"=> (int)get_mensajes_por_persona((int)$_SESSION["id_usuario"], $resultado["id_usuario"])

										 ));
		
	}
	echo json_encode($array_usuarios);
}
else{
	echo "1";
}


?>