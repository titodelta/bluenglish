<?php

require_once("conexion.php");


$estado = "0,1,2,3";


if(isset($_POST["estado"])){
	
	$estado = $_POST["estado"];
}

$consulta = "SELECT id_bug, bugs.descripcion as descripcion, bugs.id_juego as id_juego, bugs.id_pregunta as id_pregunta, verificaciones, estado, id_usuario_creador, bugs.fecha_creacion as fecha_creacion, bugs.fecha_modificacion as fecha_modificacion, usuarios.nickname as nombre_usuario_creador, juegos.nombre as nombre_juego, preguntas.pregunta as pregunta FROM bugs LEFT JOIN usuarios on bugs.id_usuario_creador = usuarios.id_usuario LEFT JOIN juegos on bugs.id_juego = juegos.id_juego LEFT JOIN preguntas on bugs.id_pregunta = preguntas.id_pregunta WHERE estado in ($estado)";


if(isset($_POST["id_bug"]) && !empty($_POST["id_bug"])){
	
	$id_bug = (int) $_POST["id_bug"];
	
	$consulta = "SELECT id_bug, bugs.descripcion as descripcion, bugs.id_juego as id_juego, bugs.id_pregunta as id_pregunta, verificaciones, estado, id_usuario_creador, bugs.fecha_creacion as fecha_creacion, bugs.fecha_modificacion as fecha_modificacion, usuarios.nickname as nombre_usuario_creador, juegos.nombre as nombre_juego, preguntas.pregunta as pregunta FROM bugs LEFT JOIN usuarios on bugs.id_usuario_creador = usuarios.id_usuario LEFT JOIN juegos on bugs.id_juego = juegos.id_juego LEFT JOIN preguntas on bugs.id_pregunta = preguntas.id_pregunta WHERE id_bug = $id_bug";

}


$query = mysqli_query($conexion, $consulta);

if($query){
	
	$bugs = array();
	
	while($resultado = mysqli_fetch_array($query)){
		
		array_push($bugs, array("id_bug"=> $resultado["id_bug"],
							 	"descripcion"=> $resultado["descripcion"],
								"id_juego"=> $resultado["id_juego"],
								"id_pregunta"=> $resultado["id_pregunta"],
								"verificaciones"=> $resultado["verificaciones"],
								"estado"=> $resultado["estado"],
								"id_usuario_creador"=> $resultado["id_usuario_creador"],
								"fecha_creacion"=> $resultado["fecha_creacion"],
								"fecha_modificacion"=> $resultado["fecha_modificacion"],
								"nombre_usuario_creador"=> $resultado["nombre_usuario_creador"],
								"nombre_juego"=> $resultado["nombre_juego"],
								"pregunta"=> $resultado["pregunta"],
							   )
				  );
		
	} 
	
	echo json_encode($bugs);
}
else{
	echo "2";
	echo mysqli_error($conexion);
}


?>