<?php
session_start();

require_once("conexion.php");
require_once("tools.php");


$id_usuario = $_SESSION["id_usuario"];

$id_amigo = $_POST["id_amigo"];


// ** CONSULTAR AMIGOS ACTUALES **
$consulta_existente = "SELECT bloqueados FROM usuarios WHERE id_usuario = $id_usuario";

$query = mysqli_query($conexion, $consulta_existente);

if($query){
	
	$resultado = mysqli_fetch_array($query);
	
	// AGREGAR AMIGOS AL ARRAY PARA COMPARARLOS
	$amigos = explode(",",$resultado["bloqueados"]);
	
	// BUSCAR AMIGO EXISTENTE
	if(array_search($id_amigo, $amigos) !== false){
		
		array_splice($amigos,array_search($id_amigo, $amigos),1);
		
		$nuevos_amigos = implode(",",$amigos);
		$nuevos_amigos = trim($nuevos_amigos,",");
		
		// *** GUARDAR NUEVOS AMIGOS ***
		$consulta_nuevos_amigos = "UPDATE usuarios SET bloqueados = '$nuevos_amigos' WHERE id_usuario = $id_usuario";
		
		$query_nuevos_amigos = mysqli_query($conexion, $consulta_nuevos_amigos);

		if($query_nuevos_amigos){
			echo "1"; // amigo eliminado
		}
		else{
			echo "2"; // error al eliminar amigo
		}
		
	}
	else{
		echo "3"; // No existe como amigo
	}
}
else{
	echo "4";
	exit();
}

?>