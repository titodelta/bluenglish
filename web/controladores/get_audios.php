<?php

require_once("conexion.php");
require_once("tools.php");

$busqueda = clearvar($_POST["busqueda"]);

$consulta = "SELECT id_audio, nombre, descripcion FROM audios WHERE (nombre like '%$busqueda%') OR (descripcion like '%$busqueda%')";

$query = mysqli_query($conexion, $consulta);

$audios = array();

if($query){
	
	while($resultado = mysqli_fetch_array($query)){
		
		$ruta_audio = $resultado["id_audio"];
		
		if(!file_exists("../audios/".$resultado["id_audio"].".mp3")){
			
			$ruta_audio = "default";
		}
		
		
			array_push($audios,array("id_imagen"=>$resultado["id_audio"],"nombre_imagen"=>$resultado["nombre"],"descripcion_imagen"=>$resultado["descripcion"],"ruta_imagen"=>$ruta_audio));
		
	}
	
	echo json_encode($audios);
}
else{
	echo "error"; // mensaje de error
}

?>