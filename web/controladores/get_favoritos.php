<?php
session_start();

require_once("conexion.php");

$id_usuario = $_SESSION["id_usuario"];

$consulta = "SELECT id_favorito, nombre, id_tokenjuego FROM favoritos WHERE id_usuario = $id_usuario";

$query = mysqli_query($conexion, $consulta);

$juegos_favoritos = array();

if(!$query){
	
	echo json_encode(array("type"=>"fail"));
	exit();	
}
else{
	
	while($resultado = mysqli_fetch_array($query)){
		
		array_push($juegos_favoritos, array("id_favorito"=>$resultado["id_favorito"],
										    "nombre"=> $resultado["nombre"],
				   							"id_tokenjuego"=> $resultado["id_tokenjuego"]
										   ));
	}
	
	echo json_encode(array("type"=>"success", "juegos"=> $juegos_favoritos));
	exit();
}

?>