<?php
	require_once("conexion.php");

    function clearvar($str){

        $str = trim($str);
        $str = strip_tags($str);
        $str = stripslashes($str);
        $str = addcslashes($str,"'");
        $str = addcslashes($str,'"');
        $str = htmlspecialchars($str);
        return $str;
    };



    function checkString($str){
        
        if(!preg_match("/^[a-zA-ZñÑ ]*$/",$_POST["email"])){

                return false;
        }
        else{
            return true;
        }
    };


    function checkNumber($str){
      
        if(!preg_match("/^[0-9]*$/",$str)){

                return false;
        }
        else{
            return true;
        }
    };

    function checkMixed($str){
        
        if(!preg_match("/^[a-zA-ZñÑ0-9- ]*$/",$str)){

                return false;
        }
        else{
            return true;
        }
    };

    function checkEmail($str){
        
		if(!preg_match('/^.+@.+\..+$/',$str) == false){
			return true;
		}
		else{
			return false;
		}
    };


    function checkPostParams($arrayparam){
        
        foreach($arrayparam as $clave){
            
            if(!isset($clave)){
                return false;
            }
            if(empty($clave)){
                return false;
            }
            
        }
            return true;
        
    };
    

	function diferencia_dias($fecha1,$fecha2){

	   $fecha1 = strtotime($fecha1);
	   $fecha2 = strtotime($fecha2);

	   $resultado = ($fecha2 - $fecha1) / 86400;

	   $resultado =  abs(round($resultado));
		
	   return $resultado;
	};



	// *** CONSULTAS SQL ***

	function get_cantidad_juegos(){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT count(id_juego) as juegos FROM juegos");
		
		if($query){
			$resultado = mysqli_fetch_array($query);
			
			return $resultado["juegos"];
		}
		else{
			return 0;
		}
		
	};


	function get_cantidad_juegos_aprendidos($id_usuario){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT COUNT(DISTINCT id_juego) as cantidad FROM estadisticas WHERE id_usuario = $id_usuario AND ganador = 1");
		
		if($query){
			$resultado = mysqli_fetch_array($query);
			
			return $resultado["cantidad"];
		}
		else{
			return 0;
		}
	};


	function get_cantidad_juegos_completados($id_usuario){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT COUNT(id_juego) as cantidad FROM estadisticas WHERE id_usuario = $id_usuario AND ganador = 1");
		
		if($query){
			
			$resultado = mysqli_fetch_array($query);
			
			return $resultado["cantidad"];
		}
		else{
			return 0;
		}
	};

	function get_cantidad_amigos($id_usuario){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT amigos FROM usuarios WHERE id_usuario = $id_usuario");
		
		if($query){
			
			$resultado = mysqli_fetch_array($query);
			
			if(strlen($resultado["amigos"]) < 1){
				return 0;
			}
			
			$resultado = count(explode(",",$resultado["amigos"]));
			
			return $resultado;
		}
		else{
			return 0;
		}
	};


	function get_promedio_velocidad($id_usuario){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT AVG(promedio) as promedio FROM estadisticas WHERE id_usuario = $id_usuario AND ganador = 1 ORDER BY id_estadistica DESC LIMIT 3");
		
		if($query){
			
			$resultado = mysqli_fetch_array($query);
			
			return $resultado["promedio"];
		}
		else{
			return 0;
		}
	};

	
	function get_cantidad_palabras($id_usuario){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT COUNT(id_palabra) as cantidad FROM palabras WHERE id_usuario = $id_usuario");
		
		if($query){
			
			$resultado = mysqli_fetch_array($query);
			
			return $resultado["cantidad"];
		}
		else{
			return 0;
		}
	};



	function get_palabras($id_usuario){
	
		global $conexion;
		
		$fecha_actual = date("Y-m-d h:i:s");
		
		$query = mysqli_query($conexion,"SELECT palabra, practica, fecha_creacion, fecha_modificacion FROM palabras WHERE id_usuario = $id_usuario");
		
		$array_palabras = array();
		
		if($query){
			
			while($resultado = mysqli_fetch_array($query)){
				
				$ultima_fecha = strlen($resultado["fecha_modificacion"]) > 0 ? $resultado["fecha_modificacion"] : $resultado["fecha_creacion"];

				$dias_resultado = diferencia_dias($ultima_fecha,$fecha_actual);
				
				$dias_resultado = $dias_resultado < 1 ? "hoy" : $dias_resultado." dias";
				
				array_push($array_palabras,array(
												"palabra"=>$resultado["palabra"],
												"practica"=>$resultado["practica"],
												"diferencia"=> $dias_resultado
												
												));
			}
			return $array_palabras;
		}
		else{
			return 0;
		}
	};



	function get_estrellas_por_juego($id_usuario, $id_juego){
	
		global $conexion;
		
		$stmt = mysqli_prepare($conexion,"SELECT estrellas FROM estadisticas WHERE id_usuario = ? AND id_juego = ? ORDER BY id_estadistica DESC LIMIT 1");
		
		mysqli_stmt_bind_param($stmt,"ii",$b_id_usuario, $b_id_juego);
		
		// INDEXAR USUARIO Y JUEGO
		$b_id_usuario = (int)$id_usuario;
		$b_id_juego = (int)$id_juego;
		
		// EJECUTAR CONSULTA
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);
		
		// INDEXAR RESULTADOS
		mysqli_stmt_bind_result($stmt, $r_estrellas);
		mysqli_stmt_fetch($stmt);
		
		if(mysqli_stmt_num_rows($stmt) > 0){
			
			return $r_estrellas;
		}
		else{
			return 0;
		}
		
		mysqli_stmt_close($stmt);
	};



	function update_ultima_conexion($id_usuario){
		
		global $conexion;
		
		$tiempo_actual = time();
		
		$consulta = "UPDATE usuarios SET ultima_conexion = $tiempo_actual WHERE id_usuario = $id_usuario";
		
		$query = mysqli_query($conexion, $consulta);
		
	};

	function get_segundos_ultima_conexion($fecha_inicial){
		
		$fecha_actual = time();
		
		$resultado = ($fecha_actual - $fecha_inicial);
		
		return $resultado;
		
	};





	function get_mensajes_por_persona($id_usuario, $id_amigo){
	
		global $conexion;
		
		$query = mysqli_query($conexion,"SELECT COUNT(id_mensaje) as cantidad FROM mensajes WHERE id_usuario_final = $id_usuario AND id_usuario_inicial = $id_amigo AND visto = 0");
		
		if($query){
			
			$resultado = mysqli_fetch_array($query);
			
			return $resultado["cantidad"];
		}
		else{
			return 0;
		}
	};


?>