<?php
session_start();

require_once("conexion.php");
require_once("tools.php");


$id_usuario_inicial = $_SESSION["id_usuario"];

$id_usuario_final = null;

$tipo = (int)$_POST["tipo"];

$mensajes = array();

if(isset($_POST["id_usuario_final"]) && !empty($_POST["id_usuario_final"])){
	
	$id_usuario_final = $_POST["id_usuario_final"];
}


if($tipo === 0){
	
	$consulta = "SELECT mensaje, id_usuario_inicial, id_usuario_final, mensajes.fecha_creacion as fecha_creacion, tipo, usuarios.nickname as nickname FROM mensajes LEFT JOIN usuarios on usuarios.id_usuario = mensajes.id_usuario_inicial WHERE tipo = 0 ORDER BY id_mensaje DESC LIMIT 0,100";
}
else if($tipo === 1){
	
	$consulta = "SELECT mensaje, id_usuario_inicial, id_usuario_final, mensajes.fecha_creacion as fecha_creacion, tipo, usuarios.nickname as nickname FROM mensajes LEFT JOIN usuarios on mensajes.id_usuario_final = usuarios.id_usuario WHERE (tipo = 1) AND (id_usuario_inicial = $id_usuario_inicial AND id_usuario_final = $id_usuario_final OR id_usuario_inicial = $id_usuario_final AND id_usuario_final = $id_usuario_inicial) ORDER BY id_mensaje DESC LIMIT 0,100";
}


if($tipo !== 0){
	
	$consulta_ver_mensajes = "UPDATE mensajes SET visto = 1 WHERE id_usuario_inicial = $id_usuario_final AND id_usuario_final = $id_usuario_inicial";
	
	$query_ver_mensajes = mysqli_query($conexion, $consulta_ver_mensajes);
}


$query = mysqli_query($conexion, $consulta);

if($query){
	
	while($resultado = mysqli_fetch_array($query)){
		
		array_push($mensajes,array("mensaje"=>$resultado["mensaje"],
								   "fecha"=>$resultado["fecha_creacion"],
								   "id_usuario_inicial"=>$resultado["id_usuario_inicial"],
								   "id_usuario_final"=>$resultado["id_usuario_final"],
								   "tipo"=>$resultado["tipo"],
								   "nickname"=>$resultado["nickname"],
								   
								  ));
		
	}
	
	// ** RETORNAR MENSAJES **
	
	$mensajes = array_reverse($mensajes);
	
	echo json_encode($mensajes);
	exit();
	
}
else{
	echo "error";
	echo mysqli_error($conexion);
	exit();
}

?>
