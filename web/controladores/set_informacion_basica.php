<?php
session_start();  

require_once("conexion.php");

require_once("tools.php");


// id_usuario POST para guardar informacion

$id_usuario = $_SESSION["id_usuario"];

update_ultima_conexion($id_usuario);

if(isset($_POST["id_usuario"]) && !empty($_POST["id_usuario"])){
	
	$id_usuario = $_POST["id_usuario"];
}


// limpieza de variables

$email = clearvar($_POST["email"]);
$usuario = clearvar($_POST["usuario"]);
$nickname = clearvar($_POST["nickname"]);
$password = clearvar($_POST["password"]);
$nombre = clearvar($_POST["nombre"]);
$apellido = clearvar($_POST["apellido"]);
$pais = clearvar($_POST["pais"]);
$ciudad = clearvar($_POST["ciudad"]);
$telefono = clearvar($_POST["telefono"]);
$fecha_nacimiento = $_POST["fecha_nacimiento"] == null ? 'null' : "'$_POST[fecha_nacimiento]'";
$rango = 1;


if(isset($_POST["rango"]) && !empty($_POST["rango"])){
	$rango = $_POST["rango"];
}

$miarray = array($email, $usuario, $nickname, $password);

if(!checkPostParams($miarray)){
    
    echo "2";  // datos incompletos
    exit();
}


if(!checkEmail($email)){
    
    echo "6";  // Ingresar email valido
    exit();
}



if(strlen($usuario) > 20){
    
    echo "10";  // usuario mayor a 20 caracteres
    exit();
}
if(strlen($nickname) > 20){
    
    echo "11";  // usuario mayor a 20 caracteres
    exit();
}



// *** EMAIL UNICO ***

$consulta1 = "SELECT id_usuario FROM usuarios WHERE email = '$email'";

$query1 = mysqli_query($conexion,$consulta1);

if(mysqli_num_rows($query1) > 0){
	
	if($id_usuario != mysqli_fetch_array($query1)["id_usuario"]){
		
		echo "7";
		exit();
	}
}


// *** USUARIO UNICO ***

$consulta2 = "SELECT id_usuario FROM usuarios WHERE usuario = '$usuario'";

$query2 = mysqli_query($conexion,$consulta2);

if(mysqli_num_rows($query2) > 0){
	
	if($id_usuario != mysqli_fetch_array($query2)["id_usuario"]){
		echo "8";
		exit();
	}
}

//*** NICKNAME UNICO ***
$consulta2 = "SELECT id_usuario FROM usuarios WHERE nickname = '$nickname'";

$query2 = mysqli_query($conexion,$consulta2);

if(mysqli_num_rows($query2) > 0){
	
	if($id_usuario != mysqli_fetch_array($query2)["id_usuario"]){
		echo "12";
		exit();
	}
}


// ingreso de datos


$consulta = "UPDATE usuarios SET email = '$email', usuario = '$usuario', nickname = '$nickname', password = '$password', nombre = '$nombre', apellido = '$apellido', pais = '$pais', ciudad = '$ciudad', telefono = '$telefono', fecha_nacimiento = $fecha_nacimiento, rango = $rango WHERE id_usuario = $id_usuario";



$query = mysqli_query($conexion,$consulta);

if($query){
	
	echo "1";
}
else{
	echo "error";
	echo mysqli_error($conexion);
}





?>