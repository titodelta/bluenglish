<?php
session_start();


require_once("conexion.php");
require_once("tools.php");


$tipo = 0;
$idioma = 0;
$id_usuario = $_SESSION["id_usuario"];

if(isset($_POST["tipo"]) && !empty($_POST["tipo"])){
	$tipo = (int)$_POST["tipo"];
}
if(isset($_POST["idioma"]) && !empty($_POST["idioma"])){
	$idioma = (int)$_POST["idioma"];
}


// PREPARAR CONSULTA


if(isset($_POST["quiz"]) && $_POST["quiz"] === "true"){
	
	$consulta = "SELECT id_juego, nombre, descripcion, idioma, tipo, quiz, posicion, id_usuario, modificaciones, fecha_creacion, fecha_modificacion, terminado FROM juegos WHERE quiz = 1 AND idioma = ?";	

}
else{
	// CONSULTA
	$consulta = "SELECT id_juego, nombre, descripcion, idioma, tipo, quiz, posicion, id_usuario, modificaciones, fecha_creacion, fecha_modificacion, terminado FROM juegos WHERE tipo = ? AND idioma = ?";


}

$stmt1 = mysqli_prepare($conexion,$consulta);

if(isset($_POST["quiz"]) && $_POST["quiz"] === "true"){
	
	mysqli_stmt_bind_param($stmt1,"i", $b_idioma);

	$b_idioma = $idioma;
	
}
else{
	
	mysqli_stmt_bind_param($stmt1,"ii",$b_tipo, $b_idioma);

	// ENLAZAR PARAMETROS
	$b_tipo = $tipo;
	$b_idioma = $idioma;	
}



// EJECUTAR CONSULTA
mysqli_stmt_execute($stmt1);

mysqli_stmt_store_result($stmt1);

// ENLAZAR RESULTADO
mysqli_stmt_bind_result($stmt1,$r_id_juego,$r_nombre, $r_descripcion, $r_idioma, $r_tipo, $r_quiz, $r_posicion, $r_id_usuario, $r_modificaciones, $r_fecha_creacion, $r_fecha_modificacion, $r_terminado);


        
    $array_juegos = array();
    
    while(mysqli_stmt_fetch($stmt1)){
        
                
                array_push($array_juegos,array(
                    "id_juego"=>$r_id_juego,
                    "nombre"=>$r_nombre,
                    "descripcion"=>$r_descripcion,
                    "idioma"=>$r_idioma,
                    "tipo"=>$r_tipo,
                    "quiz"=>$r_quiz,
                    "posicion"=>$r_posicion,
                    "usuario"=>$r_id_usuario,
                    "modificaciones"=>$r_modificaciones,
                    "fecha_creacion"=>$r_fecha_creacion,
                    "fecha_modificacion"=>$r_fecha_modificacion,
                    "terminado"=> $r_terminado,
					"estrellas"=>get_estrellas_por_juego($id_usuario,$r_id_juego)
                    ));
           
    } // fin bucle
    
    echo json_encode($array_juegos);
    
    




?>

