<?php
session_start();

require_once("conexion.php");

require_once("tools.php");

$id_usuario = $_SESSION["id_usuario"];

$errores = $_POST["errores"];
$aciertos = $_POST["aciertos"];

//*** DIVIDIR PALABRAS POR ESPACIOS ***
$palabras = clearvar($_POST["palabras"]);
$palabras = explode(" ",$palabras);
$len = count($palabras);

$consulta_inicial = "UPDATE usuarios SET errores = errores + $errores, aciertos = aciertos + $aciertos WHERE id_usuario = $id_usuario";

$query = mysqli_query($conexion, $consulta_inicial);



// ** CONSULTA SI EXISTE
$consulta_palabra_existente = "SELECT id_palabra FROM palabras WHERE palabra = ? AND id_usuario = ?";

$stmt2 = mysqli_prepare($conexion, $consulta_palabra_existente);


// ** INGRESA NUEVA
$consulta_ingreso_palabra = "INSERT INTO palabras (palabra, id_usuario) VALUES (?, ?)";

$stmt1 = mysqli_prepare($conexion, $consulta_ingreso_palabra);


// ** MODIFICA EXISTENTE
$consulta_ingreso_existente = "UPDATE palabras SET practica = practica +1 WHERE id_palabra = ?";

$stmt3 = mysqli_prepare($conexion, $consulta_ingreso_existente);


// VERIFICA CADA PALABRA Y TRATA DE INGRESARLA
for($i = 0; $i < $len; $i++){
	
	// ELIMINA ESPACIOS
	$palabra = trim($palabras[$i]);
	
	
	if(preg_match("/^[a-zA-Z]*$/",$palabra)){
		
		mysqli_stmt_bind_param($stmt2, "si", $palabra,$id_usuario);
		
		mysqli_stmt_execute($stmt2);
		
    	mysqli_stmt_store_result($stmt2);
		
		mysqli_stmt_bind_result($stmt2, $result_id_palabra);
		
		mysqli_stmt_fetch($stmt2);
		
		
		//** SI NO EXISTEN RESULTADOS 
		if(mysqli_stmt_num_rows($stmt2) < 1){
			
			mysqli_stmt_bind_param($stmt1, "si", $palabra, $id_usuario);
			mysqli_stmt_execute($stmt1);
			
		}
		else{
			
			mysqli_stmt_bind_param($stmt3, "i", $result_id_palabra);
			mysqli_stmt_execute($stmt3);
			
		}
		
	}
	
}


update_ultima_conexion($id_usuario);

?>