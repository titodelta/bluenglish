<?php

require_once("conexion.php");
require_once("tools.php");

$busqueda = clearvar($_POST["busqueda"]);

$consulta = "SELECT id_imagen, nombre, descripcion FROM imagenes WHERE (nombre like '%$busqueda%') OR (descripcion like '%$busqueda%')";

$query = mysqli_query($conexion, $consulta);

$imagenes = array();

if($query){
	
	while($resultado = mysqli_fetch_array($query)){
		
		$ruta_imagen = $resultado["id_imagen"];
		
		if(!file_exists("../imagenes/listas/".$resultado["id_imagen"].".jpg")){
			
			$ruta_imagen = "default";
		}
		
		
			array_push($imagenes,array("id_imagen"=>$resultado["id_imagen"],"nombre_imagen"=>$resultado["nombre"],"descripcion_imagen"=>$resultado["descripcion"],"ruta_imagen"=>$ruta_imagen));
		
	}
	
	echo json_encode($imagenes);
}
else{
	echo "error"; // mensaje de error
}

?>