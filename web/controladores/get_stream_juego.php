<?php
session_start();

require_once("conexion.php");

$id_token = $_POST["id_token"];
$id_usuario = $_SESSION["id_usuario"];

$consulta = "SELECT id_juego_stream, id_tokenjuego, ganadas, perdidas, completadas, configuracion1, configuracion2, configuracion3, configuracion4, configuracion5, configuracion6, configuracion7, configuracion8, vidas_actuales, desordenar, id_usuario, estado FROM juegos_stream WHERE id_tokenjuego = $id_token AND id_usuario = $id_usuario AND estado = 0";

$query = mysqli_query($conexion, $consulta);

$informacion_stream = array();

if($query){
	
	if(mysqli_num_rows($query) > 0){
		
		$resultado = mysqli_fetch_array($query);
		
		array_push($informacion_stream, array(
										"id_juego_stream"=>$resultado["id_juego_stream"],
				   						"id_tokenjuego"=>$resultado["id_tokenjuego"],
				   						"ganadas"=>$resultado["ganadas"],
				   						"perdidas"=>$resultado["perdidas"],
				   						"completadas"=>$resultado["completadas"],
				   						"configuracion1"=>$resultado["configuracion1"],
				   						"configuracion2"=>$resultado["configuracion2"],
				   						"configuracion3"=>$resultado["configuracion3"],
				   						"configuracion4"=>$resultado["configuracion4"],
				   						"configuracion5"=>$resultado["configuracion5"],
				   						"configuracion6"=>$resultado["configuracion6"],
				   						"configuracion7"=>$resultado["configuracion7"],
				   						"configuracion8"=>$resultado["configuracion8"],
				   						"vidas_actuales"=>$resultado["vidas_actuales"],
				   						"desordenar"=>$resultado["desordenar"],
				   						"id_usuario"=>$resultado["id_usuario"],
				   						"estado"=>$resultado["estado"]
										)
				  
				  );
		
		echo json_encode($informacion_stream);
		
	}
	else{
		echo "ninguno";
	}
	
}
else{
	echo "ninguno";
}


?>