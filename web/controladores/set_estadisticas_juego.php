<?php
session_start();

require_once("conexion.php");
require_once("tools.php");


$id_juego = explode(",",$_POST["id_juego"]);
$segundos = $_POST["segundos"];
$aciertos = $_POST["aciertos"];
$errores = $_POST["errores"];
$remake = $_POST["remake"];
$preguntas = $_POST["preguntas"];
$id_tokenjuego = $_POST["id_tokenjuego"];
$id_tokenunico = mt_rand();
$ganador = $_POST["ganador"];
$promedio = $_POST["promedio"];
$estrellas = $_POST["estrellas"];

$id_usuario = $_SESSION["id_usuario"];



$consulta = "INSERT INTO estadisticas (id_juego, segundos, aciertos, errores, remake, id_usuario, preguntas, id_tokenjuego, id_token_unico, ganador, promedio, estrellas) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

$stmt = mysqli_prepare($conexion, $consulta);

$len = count($id_juego);

for($i = 0; $i < $len; $i++){
	
	mysqli_stmt_bind_param($stmt, "iiiiiiiiiidi", $id_juego[$i], $segundos, $aciertos, $errores, $remake, $id_usuario, $preguntas, $id_tokenjuego, $id_tokenunico, $ganador, $promedio, $estrellas);

	mysqli_stmt_execute($stmt);
}

update_ultima_conexion($id_usuario);

?>

