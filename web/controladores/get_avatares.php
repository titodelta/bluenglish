<?php

require_once("conexion.php");


$consulta = "SELECT id_avatar, nombre, ruta, requisito FROM avatares";

$query = mysqli_query($conexion, $consulta);

$avatares = array();

if($query){
	
	while($result = mysqli_fetch_array($query)){
		
	
		array_push($avatares, array("id_avatar"=> $result["id_avatar"],
									"nombre"=> $result["nombre"],
									"ruta"=> $result["ruta"],
									"requisito"=> $result["requisito"]
								   ));
	
	}
	
	echo json_encode($avatares);
	
}
else{
	echo "error";
}


?>