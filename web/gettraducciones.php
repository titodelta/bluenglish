<!DOCTYPE html>
<html>

	<head>
	
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="estilos/boostrap/css/bootstrap.min.css">
		
		<script>
		
			window.addEventListener('load',inicio);
			function inicio(){
			
				document.getElementById('btn_calcular').addEventListener('click',calcular);
				
				function calcular(){
				
					var texto1 = document.getElementById('textarea1').value;
	
					texto1 = texto1.split("\n");

					for(var i = 0; i < texto1.length; i++){
						if(texto1[i].length < 1){
							continue;
						}
						texto1[i] = texto1[i].split("\t")[1].split("-----------");
						for(var b = 0; b < texto1[i].length; b++){
							texto1[i][0] =texto1[i][0].replace(new RegExp("[0-9]$","gi"),"");
							texto1[i][0] =texto1[i][0].trim();
							texto1[i][0] =texto1[i][0].replace(new RegExp("pr $","gi"),"?");
							texto1[i][0] =texto1[i][0].trim();
							texto1[i][0] =texto1[i][0].replace(new RegExp("pr$","gi"),"?");
							texto1[i][0] =texto1[i][0].trim("");
							texto1[i][0] =texto1[i][0].split(".").join("");	
						}
					}
					console.log(texto1);
					
					
					var texto2 = document.getElementById('textarea2').value;
	
					texto2 = texto2.split("\n");

					for(var i = 0; i < texto2.length; i++){
						if(texto2[i].length < 1){
							continue;
						}
						texto2[i] = texto2[i].split("\t")[1].split("-----------");
						
							texto2[i][0] =texto2[i][0].replace(new RegExp("[0-9]$","gi"),"");
							texto2[i][0] =texto2[i][0].trim();
							texto2[i][0] =texto2[i][0].replace(new RegExp("pr $","gi"),"?");
							texto2[i][0] =texto2[i][0].trim();
							texto2[i][0] =texto2[i][0].replace(new RegExp("pr$","gi"),"?");
							texto2[i][0] =texto2[i][0].trim("");
							texto2[i][0] =texto2[i][0].split(".").join("");	
						
					}

					console.log(texto2);
					
					var lentotal = texto1.length;
					for(var i = 0; i < lentotal; i++){
						
						
						if(texto1[i].length < 1){
							continue;
						}
						
						var current_origin = texto1[i][0];
						
							current_origin = current_origin.replace(new RegExp("\\?","gi"),"");
							current_origin = current_origin.trim();
						
						
						var len_old = texto2.length;
						for(var b = 0; b < len_old; b++){
							
							if(texto2[b].length < 1){
								continue;
							}
							var current_old = texto2[b][0];
							
							current_old = current_old.replace(new RegExp("\\?","gi"),"");
							current_old = current_old.trim();
							
							
							if(current_origin == current_old){
								texto1[i][1] = texto2[b][1];
								continue;
							}
							
							
						}
						texto1[i] = texto1[i].join("-----------");
						
					}
					
					var conteo = 0;
					for(var i = 0; i < lentotal; i++){
						if(texto1[i] == "" || texto1[i] == undefined || texto1[i] == null){
							//texto1.splice(i,1);
						}
						else{
							
							conteo++;
							texto1[i] = conteo+".\t"+texto1[i];
						}
					}
					
					
					texto1 = texto1.join("\n");
					
					document.getElementById('textarea3').value = texto1;
					
					
				};
				
			};
		</script>
	</head>

	<body>
		
		<div>
		
			<div class="form-group">
			<div class="page-header"></div>
			<div class="container">
				<h3>Nueva lista</h3>
				<textarea id="textarea1" class="form-control"></textarea>

				<h3>Antigua lista</h3>
				<textarea class="form-control" id="textarea2"></textarea>

				<div class="page-header"></div>
			</div>
			<div class="btn btn-info btn-block" id="btn_calcular">Calcular</div>
			
			<div class="container">
				<h3>Resultado</h3>
				<textarea class="form-control" id="textarea3"></textarea>
			</div>
				
			</div>
			
		</div>
		
	</body>
	
</html>