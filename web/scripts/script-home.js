

window.addEventListener('load',init_home);
                        
function init_home(){   // empieza cuando se carga la pagina

	
var informacion_jugador;
	
var informacion_envio = {
	"tipo" : 0,
	"idioma" : 0,
	"quiz" : "false"
};
	
var jasonJuegos = [];
	
var remake = false;
	
var juegos_remake = [];
	
var id_ultima_lista;
	
// intervalo usado para SCROLL JUEGOS
var intervalo;
	
var first_view = true;
	
var primer_juego_remake = undefined;
var ultimo_juego_remake = undefined;
	
	
window.addEventListener('resize',resizeHandlerHome);   // attach evento de resize para responsive design
    
document.getElementById('select_idioma_1').addEventListener('click',cambiar_idioma);
document.getElementById('select_idioma_2').addEventListener('click',cambiar_idioma);
document.getElementById('select_idioma_3').addEventListener('click',cambiar_idioma);
	
document.getElementById('btn_vocabulario').addEventListener('click',cambiar_categoria);
document.getElementById('btn_inmersion').addEventListener('click',cambiar_categoria);
document.getElementById('btn_quiz').addEventListener('click',cambiar_categoria);
	
document.getElementById('btn_remake').addEventListener('click',seleccionar_remake);
	
document.getElementById('btn_aceptar_remake').addEventListener('click',comenzar_remake);
document.getElementById('btn_cancelar_remake').addEventListener('click',cancelar_remake);
	
document.getElementById('scrollUp').addEventListener('mouseover',empezarScrollUp);
document.getElementById('scrollUp').addEventListener('mouseout',terminarScrollUp);
document.getElementById('scrollUp').addEventListener('click',clickScrollUp);
	
document.getElementById('scrollDown').addEventListener('mouseover',empezarScrollDown);
document.getElementById('scrollDown').addEventListener('mouseout',terminarScrollDown);
document.getElementById('scrollDown').addEventListener('click',clickScrollDown);
	
	
resizeHandlerHome();    // Funciones para el responsive
    
iniciar_get_informacion_usuario();

getInformacion();  // Ejecutar todas las funciones de carga asincronas
    
// conseguir id_ultima lista
get_ultima_lista();

	

//*** CONSEGUIR INFORMACION JUGADOR Y MOSTRARLA ****

function iniciar_get_informacion_usuario(){

	get_informacion_usuario(resultado_get_informacion_usuario);

};

// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
function resultado_get_informacion_usuario(param){

	informacion_jugador = JSON.parse(param)[0];

	console.log(informacion_jugador);

	verificar_stream_pendiente();
	
	render_datos_perfil();
};
	
	
function verificar_stream_pendiente(){
	
	if(informacion_jugador.id_tokenjuego_actual != null){
		
		$('#modal_stream_juego').modal("show");
		
		document.getElementById('btn_retomar_stream').addEventListener('click',retomar_stream_pendiente);
		document.getElementById('btn_cancelar_stream').addEventListener('click',cancelar_stream_pendiente);
		
	}
}
	
// VERIFICAR STREAM EN CURSO
function retomar_stream_pendiente(){

	window.location.href = "play.php?game="+informacion_jugador.id_tokenjuego_actual;
};
	
// CANCELAR STREAM EN CURSO
function cancelar_stream_pendiente(){

	delete_stream_juego(resultado_cancelar_stream_pendiente);
};

function resultado_cancelar_stream_pendiente(respuesta){

};
	
	
// RENDER DATOS PERFIL Y ESTADISTICAS
function render_datos_perfil(){

	var porcentaje = Math.round((parseInt(informacion_jugador.cantidad_juegos_aprendidos) * 100) / parseInt(informacion_jugador.cantidad_juegos));

		document.getElementById('txt_perfil_aprendidas').innerHTML = informacion_jugador.cantidad_juegos_aprendidos;
		document.getElementById('txt_perfil_nickname').innerHTML = informacion_jugador.nickname;
		
		document.getElementById('bar_perfil_porcentaje').style.width = porcentaje+"%";
		document.getElementById('txt_perfil_porcentaje').innerHTML = porcentaje + "%";
		document.getElementById('imagen_perfil').setAttribute("src","../imagenes/avatares/"+informacion_jugador.id_avatar+".png");
		
		document.getElementById('txt_juegos_aprendidos').innerHTML = informacion_jugador.cantidad_juegos_aprendidos + " / " + informacion_jugador.cantidad_juegos;
		document.getElementById('txt_juegos_completados').innerHTML = informacion_jugador.cantidad_juegos_completados;
		document.getElementById('txt_cantidad_palabras').innerHTML = informacion_jugador.cantidad_palabras;
		document.getElementById('txt_velocidad_promedio').innerHTML = informacion_jugador.promedio;
		document.getElementById('txt_mejor_racha').innerHTML = informacion_jugador.racha;
		document.getElementById('txt_amigos').innerHTML = informacion_jugador.cantidad_amigos;
};



	

// CAMBIAR IDIOMA
function cambiar_idioma(e){

	e.preventDefault();	
	
	
	switch(e.target.id){
			
		case "select_idioma_1":{
		
			informacion_envio.idioma = 0; // Ingles
			break;
		}
			
		case "select_idioma_2":{
		
			informacion_envio.idioma = 1; // Español
			break;
		}
			
		case "select_idioma_3":{
		
			informacion_envio.idioma = 2; // Frances
			break;
		}
			
	} // fin switch
	
	getInformacion();
	
};
	
	
	
// CAMBIAR CATEGORIA
function cambiar_categoria(e){

	
	document.getElementById('btn_vocabulario').classList.remove("button-disabled");
	document.getElementById('btn_inmersion').classList.remove("button-disabled");
	document.getElementById('btn_quiz').classList.remove("button-disabled");
	
	var elemento = e.target.tagName == "DIV" ? e.target : e.target.parentElement;
	
	informacion_envio.quiz = "false";
	
	switch(elemento.id){
			
		case "btn_vocabulario":{
			
			elemento.classList.add("button-disabled");
			informacion_envio.tipo = 0; // Vocabulario
			break;
		}		
		case "btn_inmersion":{
			
			elemento.classList.add("button-disabled");
			informacion_envio.tipo = 1; // inmersion
			break;
		}
			
		case "btn_quiz":{
		
			elemento.classList.add("button-disabled");
			informacion_envio.quiz = "true"; // quiz
			break;
		}
		
			
	} // fin switch
	
	getInformacion();
	
};
	
	
	
// CREAR NUEVO TOKEN DE JUEGO
function setNuevoToken(param){   // ingreso de token
    
    set_token_game(getActualToken,param);
    
}
    
// CONSEGUIR JUEGOS DE TOKEN RECIEN CREADO
function getActualToken(param){  // conseguir el id del token la base de datos
  
	set_scroll();
	window.location.assign("play.php?game="+param);
};
    
	
// CONSEGUIR JUEGOS DISPONIBLES
function getInformacion(){
    
    get_lista_games(getListaJuegos,informacion_envio);
}
    

   

// CONSEGUIR JUEGOS POR ID PARA RENDERIZARLOS
function getListaJuegos(param){
    
    jasonJuegos = JSON.parse(param);    // transformar a formato JSON la respuesta de php para las listas
	
	jasonJuegos.sort(ordenar_por_posicion);
	
    dibujarJuegos(jasonJuegos);             // dibujar la lista de los juegos
    
    //console.log(jasonJuegos); // Mostrar informacion jason de los juegos
    
};
    
    

// RENDER JUEGOS DISPONIBLES
function dibujarJuegos(param) {
  
    var tabla = document.getElementById('tabla_juegos');
    var len = param.length;
	
	tabla.innerHTML = "";
    
    for(var i = 0; i < len; i++){
       
        
        var tr_main = document.createElement("tr");
        var td_num = document.createElement("td");
        var td_nombre = document.createElement("td");
        var td_estrellas = document.createElement("td");
        var td_iconplay = document.createElement("td");
        
                
        td_num.classList.add("fieldid");
        td_nombre.classList.add("fieldname");
        td_estrellas.classList.add("fieldstars");
        td_iconplay.classList.add("fieldplay","colorspecial");
        
        
        // creacion de iconos y agregar tipo de icono
        
        var estrella1 = document.createElement("i");
        var estrella2 = document.createElement("i");
        var estrella3 = document.createElement("i");
        
        var iconoplay = document.createElement("i");
        
        iconoplay.setAttribute("class","fa fa-mortar-board");
        
        
        // Ingresar informacion de los juegos
        td_num.innerHTML = i+1;
        td_nombre.innerHTML = param[i].nombre;
        td_estrellas.dibujar_estrellas(param[i].estrellas);
        
		// VERIFICAR ULTIMA LISTA JUGADA
		if(remake !== true){
			
			if(id_ultima_lista !== "" || id_ultima_lista !== undefined){
				if(parseInt(param[i].id_juego) == id_ultima_lista){
					tr_main.classList.add("tr_selected");
				}
			}
		}
		
        td_iconplay.appendChild(iconoplay);
        
        tr_main.appendChild(td_num);
        tr_main.appendChild(td_nombre);
        tr_main.appendChild(td_estrellas);
        tr_main.appendChild(td_iconplay);
        
        tabla.appendChild(tr_main);
		
		
		
		(function(){
        
            var indice = i;
            var current_game = param[indice];
			
			if(juegos_remake.research(current_game.id_juego)){
				
				tr_main.classList.add("tr_selected");
			}
			
            tr_main.addEventListener('click',function(event) {
                
				attach_juego.call(this,indice,current_game);
            }); // fin acciones click tr_main
			
			
			
        })(); // fin auto funcion
		
		
        
    } // fin for de creacion de listas
    
    
	// RECUPERAR SCROLL
	if(first_view === true){
		
		get_scroll();
		first_view = false;
	}
	
};
    
    
function attach_juego(indice,current_game){
	
					if(remake == true){
						
						if(juegos_remake.toggle(current_game.id_juego,true)){

							this.classList.add("tr_selected");
							
								// verificar si esta realizando anclaje
								if(event.shiftKey){

									// verificar si existen un anclaje inicial
									if(primer_juego_remake != null || primer_juego_remake != undefined){
										
										// selecciona ultimo anclaje inicial
										ultimo_juego_remake = indice;
										extraer_anclaje();
										
									}
									else{
										
										// escoge el juego clickeado como anclaje inicial
										primer_juego_remake = indice;
									}
								}
								else{
									primer_juego_remake = indice;
								}
							
						}
					
						else{

							this.classList.remove("tr_selected");
						}
					
					
					
					conteo_remake();
						
				}
				else{
					
					set_ultima_lista(current_game.id_juego);
                	setNuevoToken(current_game.id_juego); // ingresar nuevo token con el id de juego actual
				}
				
};
	
	

function extraer_anclaje(){
	
	var primero = primer_juego_remake;
	
	var ultimo = ultimo_juego_remake +1;
	
	if(primero > ultimo){
		
		primero = ultimo_juego_remake +1;
		
		ultimo = primer_juego_remake;
	}
	
	console.log(primero);
	console.log(ultimo);
	
	var extraccion = jasonJuegos.slice(primero,ultimo);
	
	var len = extraccion.length;
	
	for(var i = 0; i < len; i++){
		
		juegos_remake.toggle(extraccion[i].id_juego);
		
	}
	
	dibujarJuegos(jasonJuegos);
};
	
	
function comenzar_remake(){

	if(juegos_remake.length < 1){
		
		openAlerta("alert-danger","Debes escoger al menos 1 juego");
		return;
	}
	
	var juegos_to_play = juegos_remake.join(",");
	
	setNuevoToken(juegos_to_play);
	
};
	
	
function seleccionar_remake(){
	
	remake = true;
	
	categoria = 0;
	
	getInformacion();
	
	conteo_remake();
	
	document.getElementById('dialogo_remake').style.display = "block";
	
	document.getElementById('boxoptionsplay').style.display = "none";

};
	
	
	
function cancelar_remake(){

	remake = false;
	
	juegos_remake = [];
	
	limpiar_juegos();
	
	document.getElementById('boxoptionsplay').style.display = "block";
	
	document.getElementById('dialogo_remake').style.display = "none";
	
	document.getElementById('contenedorlistas').classList.remove("remake_active_movil");
};
	
	
function limpiar_juegos(){
	
	var elementos = document.querySelectorAll(".tr_selected");
	
	var len = elementos.length;
	
	for(var i = 0; i < len; i++){
		
		elementos[i].classList.remove("tr_selected");
	}
	
};
	
	
function conteo_remake(){
	
	document.getElementById('cantidad_remake').innerHTML = "("+juegos_remake.length+") Juegos escogidos";
};

	

  
	
function ordenar_por_posicion(a,b){

		a = parseInt(a.posicion);	
		b = parseInt(b.posicion);
	
		if(a < b){
			return -1;
		}
		else if(a > b){
			return 1;
		}
		else{
			return 0;
		}
		
};
	
	
	
	


function empezarScrollUp(){
	intervalo = setInterval(scrollUp,100);
}
	
function terminarScrollUp(){
	clearInterval(intervalo);
}
	
function scrollUp(tipo){
	
	document.getElementById('scrollerlist').scrollTop -= 10;
}
function clickScrollUp(){
	
	clearInterval(intervalo);
	document.getElementById('scrollerlist').scrollTop = 0;
}
	
function empezarScrollDown(){
	intervalo = setInterval(scrollDown,100);
}
	
function terminarScrollDown(){
	clearInterval(intervalo);
}
	
function scrollDown(tipo){
	document.getElementById('scrollerlist').scrollTop += 10;
}
function clickScrollDown(){
	
	clearInterval(intervalo);
	document.getElementById('scrollerlist').scrollTop = document.getElementById('scrollerlist').scrollHeight;
}
	
    
    
function set_scroll(){

	if(window.localStorage){
		window.localStorage.setItem("scrollGame",document.getElementById('scrollerlist').scrollTop);
	}
};
    
function get_scroll(){

	document.getElementById('scrollerlist').scrollTop = window.localStorage.getItem("scrollGame");
};
	
	
function get_ultima_lista(){

	console.log(window.localStorage.getItem("id_ultima_lista"));
	if(window.localStorage){
		if(window.localStorage.getItem("id_ultima_lista") !== "" || window.localStorage.getItem("id_ultima_lista") !== undefined){
			id_ultima_lista = parseInt(window.localStorage.getItem("id_ultima_lista"));
		}
	}
	
};
function set_ultima_lista(param){
	if(window.localStorage){
		if(remake){
			window.localStorage.setItem("id_ultima_lista","");
		}
		else{
			window.localStorage.setItem("id_ultima_lista",param);
		}
	}
};
	
	

    /*  Funciones para responsive */
    
    
    
function resizeHandlerHome(){
    
    var ancho = window.innerWidth;
    var alto = window.innerHeight;
	
	var nueva_altura = alto - 215;
	
	document.getElementById('scrollerlist').style.maxHeight = nueva_altura+"px";
    
};
  
    
    
};   // empieza cuando se carga la pagina