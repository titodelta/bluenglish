
window.addEventListener('load',inicio_community);


function inicio_community(){

// ** INFORMACION DE JUGADORES
var jugadores;

// INFORMACION PROPIA
var informacion_propia;
	
// TIPO DE BUSQUEDA AMIGOS-TODOS
var busqueda_general = true;
	
// SECCION USERS CHAT
var table_chats = document.getElementById('list_chat_users');
	
// ID USUARIO ACTUAL OPEN CHAT
var info_open_chat = {
	"id_usuario_final":"0",
	"tipo": 0
};
	
var first_charge = true;
	
var abierto_por_click = false;
	
var nueva_ventana = false;
	
var sound_notification = new Audio();
sound_notification.src = "../sonidos/notification.mp3";

function play_notification(){

	sound_notification.currentTime = 0;
	sound_notification.play();
};
	
	
document.getElementById('icon_alarm_general').parentNode.parentNode.classList.add("hidden");
	
window.addEventListener('resize',resizeHandlerHome);
	
document.getElementById('btn_buscar_usuario').addEventListener('keyup',render_usuarios);
	
document.getElementById('tab_todos').addEventListener('click',busqueda_todos);
document.getElementById('tab_amigos').addEventListener('click',busqueda_amigos);
	
document.getElementById('btn_enviar_mensaje').addEventListener('click',iniciar_set_mensaje);

document.getElementById('txt_mensaje_chat').addEventListener('keydown',key_iniciar_set_mensaje);
	
document.getElementById('btn_chat_general').addEventListener('click',open_chat_general);


	

// CONSEGUIR INFORMACION PROPIA, Y LUEGO LA DEL RESTO DE USUARIOS
iniciar_get_informacion_propia();

// CARGAR INFORMACION CADA MINUTO
setInterval(iniciar_get_informacion_usuario,20000);
	
// INICIAR COMO CHAT GENERAL **
open_chat_general();
	
// ** INICIAR STREAM MENSAJES **
var intervalo_mensaje = setInterval(iniciar_get_mensajes,1000);
var intervalo_nuevos_chats = setInterval(iniciar_get_chats,1000);
var intervalo_ver_mensajes_generales = setInterval(ver_mensajes_generales,10000);

	
// ** SCREEN RESPONSIVE ALTURA **
resizeHandlerHome();

	
	
	
	function iniciar_get_chats(){
	
		get_chats(resultado_get_chats);
		
	};
	
	function resultado_get_chats(respuesta){
	
		if(respuesta.indexOf("error") !== -1){
			return;
		}
		if(JSON.parse(respuesta)){
			
			var proximos_chats = [];
			
			
			if(JSON.parse(respuesta).respuesta === null){
				
				return;
			}
			
			respuesta = JSON.parse(respuesta).respuesta.split(",");
			
			
			var len = respuesta.length;
			
			for(var i = 0; i < len; i++){
			
				if(document.querySelector('[data-profile="'+respuesta[i]+'"]') === null){
					if(respuesta[i].length > 0){
						
						proximos_chats.push(respuesta[i]);
					}
					
				}
			}
			// fin for
			
			if(proximos_chats.length > 0){
				var datos = {
					"id_usuario" : proximos_chats.join(",")
				};
				get_informacion_usuario(tester,datos);
			}
			
		}
		
	};
	
	function tester(respuesta_proximos_chats){
		
		if(JSON.parse(respuesta_proximos_chats)){
			respuesta_proximos_chats = JSON.parse(respuesta_proximos_chats);
			var len = respuesta_proximos_chats.length;
			
			for(var i = 0; i < len; i++){
				
				if(document.querySelector('[data-profile="'+respuesta_proximos_chats[i].id_usuario+'"]') === null){
	
					chat_individual(respuesta_proximos_chats[i]);
				}
			}
		}
	};
	
	
	
	
	
	
	
	
	
	// FILTRAR CARGA DE USUARIOS POR AMIGOS O GENERAL
	function busqueda_todos(e){
		
		e.preventDefault();
		document.getElementById('tab_todos').classList.add("active");
		document.getElementById('tab_amigos').classList.remove("active");
		busqueda_general = true;
		iniciar_get_informacion_usuario();
	};
	function busqueda_amigos(e){
		
		e.preventDefault();
		document.getElementById('tab_todos').classList.remove("active");
		document.getElementById('tab_amigos').classList.add("active");
		busqueda_general = false;
		iniciar_get_informacion_usuario();
	}
	
	
	
	// ** CARGAR INFORMACION PROPIA **
	function iniciar_get_informacion_propia(){
		
		get_informacion_usuario(iniciar_get_informacion_usuario);
	};
	
	
	//*** CONSEGUIR INFORMACION JUGADORES Y MOSTRARLA ****
	
	function iniciar_get_informacion_usuario(param){

			if(param){
				informacion_propia = JSON.parse(param)[0];
			}
					
			var datos = {
				"todos": 'true'
			}
			get_informacion_usuario(resultado_get_informacion_usuario,datos);
				
	};
	
	// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
	function resultado_get_informacion_usuario(param){
		
		if(JSON.parse(param).length < 1){
			
			window.location.href = "home.php";
			return;
		}
		
		jugadores = JSON.parse(param);
	
		render_usuarios();
		
	};
	
	

	function render_usuarios(){
	
		var len = jugadores.length;
		
		var tabla = document.getElementById('contenedorcomunidad');
		
		var palabra_busqueda = document.getElementById("btn_buscar_usuario").value.trim();
		
		var chats_abiertos = [];
		
		tabla.innerHTML = "";
		
		for(var i = 0; i < len; i++){
			
			// CONCIDENCIAS CON LA BUSQUEDA
			if(jugadores[i].nickname.toLocaleLowerCase().indexOf(palabra_busqueda.toLocaleLowerCase()) == -1){
				continue;
			}
			// NO MOSTRARSE ASI MISMO
			if(parseInt(jugadores[i].id_usuario) == (informacion_propia.id_usuario)){
				continue;
			}
			
			// VERIFICAR VENTANAS DE CHATS ABIERTAS
			if(informacion_propia.chats !== null && informacion_propia.chats.split(",").research(jugadores[i].id_usuario) !== false){
				chats_abiertos.push(jugadores[i]);
			}

			if(jugadores[i].bloqueados !== null && jugadores[i].bloqueados.split(",").research(informacion_propia.id_usuario)){
			   continue;
			}
			
			// VERIFICAR BUSQUEDA GENERAL O AMIGOS
			if(!busqueda_general){
				if(informacion_propia.amigos !== null){
					if(informacion_propia.amigos.split(",").research(jugadores[i].id_usuario) == false){
						continue;
					}
				}
			}
			
			
			var box_main = document.createElement("div");
			
			var box_avatar = document.createElement("div");
			var img_avatar = document.createElement("img");
			var icon_lock = document.createElement("i");
			
			var box_informacion = document.createElement("div");
			var box_nombre = document.createElement("a");
			
			var box_cont_percent = document.createElement("div");
			var box_percent = document.createElement("div");
			var bar_back = document.createElement("div");
			var bar_front = document.createElement("div");
			var number_percent = document.createElement("div");
			
			var box_cont_connect = document.createElement("div");
			var icon_last_connect = document.createElement("i");
			var texto_last_connect = document.createElement("span");
			
			var box_actions = document.createElement("div");
			var box_cont_dropdown = document.createElement("div");
			var btn_dropdown = document.createElement("button");
			var i_caret_btn = document.createElement("i");
			var list_dropdown = document.createElement("ul");
			
			var icon_chat = document.createElement("i");
			
			var item_1 = document.createElement("li");
			var texto_item_1 = document.createElement("a");
			
			var item_2 = document.createElement("li");
			var texto_item_2 = document.createElement("a");
			
			var item_3 = document.createElement("li");
			var texto_item_3 = document.createElement("a");
			
			
			box_main.className = "perfilcommunity";
			box_avatar.className = "avatar";
			icon_lock.className = "fa fa-lock lock";
			
			box_informacion.className = "informacion";
			box_nombre.className = "nombre";
			box_nombre.setAttribute("target","_blank");
			
			box_cont_percent.className = "percent_container";
			box_percent.className = "percentgame";
			bar_back.className = "barpercentback";
			bar_front.className = "barpercentfront";
			number_percent.className = "numberpercent";
			
			box_cont_connect.className = "lastconnection";
			icon_last_connect.className = "fa fa-circle";
			
			box_actions.className = "actions";
			box_cont_dropdown.className = "dropdown moreoptions";
			btn_dropdown.className = "btn backgroundprimary colorblanco dropdown-toggle";
			btn_dropdown.setAttribute("type","button");
			btn_dropdown.setAttribute("data-toggle","dropdown");
			i_caret_btn.className = "fa fa-caret-down";
			list_dropdown.className = "dropdown-menu pull-right";
			
			icon_chat.className = "icon-init-chat fa fa-commenting";
			
			texto_item_1.innerHTML = "Ver perfil";
			texto_item_2.innerHTML = "Agregar amigo";
			texto_item_3.innerHTML = "Bloquear";
			
			box_avatar.appendChild(img_avatar);
			box_avatar.appendChild(icon_lock);
			
			box_informacion.appendChild(box_nombre);
			
			bar_back.appendChild(bar_front);
			box_percent.appendChild(bar_back);
			box_percent.appendChild(number_percent);
			box_cont_percent.appendChild(box_percent);
			
			box_informacion.appendChild(box_cont_percent);
			
			box_cont_connect.appendChild(icon_last_connect);
			box_cont_connect.appendChild(texto_last_connect);
			
			icon_chat.setAttribute("title","chat");
			icon_chat.setAttribute("data-toggle","tooltip");
			
			texto_item_1.setAttribute("target","_blank");
			texto_item_2.setAttribute("target","_blank");
			texto_item_3.setAttribute("target","_blank");
			
			item_1.appendChild(texto_item_1);
			item_2.appendChild(texto_item_2);
			item_3.appendChild(texto_item_3);
			
			list_dropdown.appendChild(item_1);
			list_dropdown.appendChild(item_2);
			list_dropdown.appendChild(item_3);
			
			btn_dropdown.appendChild(i_caret_btn);
			
			box_cont_dropdown.appendChild(btn_dropdown);
			box_cont_dropdown.appendChild(list_dropdown);
			
			box_actions.appendChild(box_cont_dropdown);
			
			
			box_main.appendChild(box_avatar);
			box_main.appendChild(box_informacion);
			box_main.appendChild(box_cont_connect);
			box_main.appendChild(box_actions);
			box_main.appendChild(icon_chat);
			
			var porcentaje = Math.round((parseInt(jugadores[i].cantidad_juegos_aprendidos) * 100) / parseInt(jugadores[i].cantidad_juegos));
			
			// ** INFORMACION BOX USUARIO **
			box_nombre.innerHTML = jugadores[i].nickname;
			box_nombre.href = "configuracion.php?profile="+jugadores[i].id_usuario;
			img_avatar.src = "../imagenes/avatares/"+jugadores[i].id_avatar+".png";
			number_percent.innerHTML = porcentaje+"%";
			bar_front.style.width = porcentaje+"%";
			texto_last_connect.innerHTML = get_mensaje_conexion(jugadores[i].ultima_conexion);
			icon_last_connect.classList.add(get_color_conexion(jugadores[i].ultima_conexion));
			
			
			texto_item_1.href = "configuracion.php?profile="+jugadores[i].id_usuario;
			texto_item_2.href = "#"; 
			texto_item_3.href = "#"; 
			
			if(jugadores[i].mensajes_pendientes > 0){
				icon_chat.classList.add("animation_basic1");
				icon_chat.classList.add("colorprimary");
			}
			
			
			(function(){
				
				var jugador = jugadores[i];
				
				// ** BOTON AGREGAR AMIGO **
				check_btn_agregar_amigo.call(texto_item_2,jugador);
				
				// ** BOTON BLOQUEAR AMIGO **
				check_btn_bloquear_amigo.call(texto_item_3,jugador);
				
				// ** BOTON INICIAR CHAT **
				check_btn_iniciar_chat.call(icon_chat,jugador);

				
				// VERIFICAR SI SON AMIGOS
				check_amigo.call(btn_dropdown,jugador);
				
				// VERIFICAR SI ESTA BLOQUEADO
				check_bloqueado.call(icon_lock,jugador);
				
			})();
			
			
			tabla.appendChild(box_main);
		}
		// fin loop
		
		// SOLO CARGAR UNA VEZ EL CHAT
		if(first_charge === true){
		if(chats_abiertos.length > 0){
			
			render_chats(chats_abiertos);
		}
		}
		
		first_charge = false;
		
		
		$('[data-toggle="tooltip"]').tooltip();
		
		
	};
	
	

	
	
	
	// ACCIONES BOTONES COMUNIDAD ***
	function check_btn_agregar_amigo(info_amigo){
				
				if(informacion_propia.amigos !== null && informacion_propia.amigos.split(",").research(info_amigo.id_usuario) == true){
				
						this.addEventListener('click',function(e){ e.preventDefault(); iniciar_eliminar_amigo.call(this, info_amigo);});
						this.innerHTML = "Eliminar amigo";
				}
				else{
						this.addEventListener('click',function(e){ e.preventDefault(); iniciar_agregar_amigo.call(this, info_amigo);});
						this.innerHTML = "Agregar amigo";
				}
	};
	
	function iniciar_agregar_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		agregar_amigo(resultado_agregar_amigo, datos);
		
	};
	
	function resultado_agregar_amigo(respuesta){

		iniciar_get_informacion_propia();
			
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Agregado a tu lista de amigos");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido agregarlo a tu lista de amigos");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario ya es tu amigo");
					break;
				}
				default:{
					break;
				}
			}
		}
	};
	
	
	function iniciar_eliminar_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		eliminar_amigo(resultado_eliminar_amigo, datos);
		
	};
	
	function resultado_eliminar_amigo(respuesta){
	
		iniciar_get_informacion_propia();
		
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Eliminado de tu lista de amigos");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido eliminarlo de tu lista de amigos");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario No es tu amigo");
					break;
				}
				default:{
					break;
				}
			}
		}
	
	};
	
	
	// *** ACCIONES BOTON BLOQUEAR AMIGO **
	
	function check_btn_bloquear_amigo(info_amigo){
		
				if(informacion_propia.bloqueados !== null && informacion_propia.bloqueados.split(",").research(info_amigo.id_usuario) == true){
				
						// ** DESBLOQUEAR **
						this.addEventListener('click',function(e){ 
							e.preventDefault(); 
							iniciar_desbloquear_amigo.call(this, info_amigo);
						});
						this.innerHTML = "Desbloquear";
				}
				else{
						//** BLOQUEAR **
						this.addEventListener('click',function(e){ 
							e.preventDefault(); 
							iniciar_bloquear_amigo.call(this, info_amigo);
						});
						this.innerHTML = "Bloquear";
				}
	};
	
	
		function iniciar_bloquear_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		bloquear_amigo(resultado_bloquear_amigo, datos);
		
	};
	
	function resultado_bloquear_amigo(respuesta){

		//console.log(respuesta);
		iniciar_get_informacion_propia();
			
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Este usuario ha sido bloqueado");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido bloquearlo");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario ya esta bloqueado");
					break;
				}
				default:{
					break;
				}
			}
		}
	};
	
	
	function iniciar_desbloquear_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		desbloquear_amigo(resultado_desbloquear_amigo, datos);
		
	};
	
	function resultado_desbloquear_amigo(respuesta){
	
		iniciar_get_informacion_propia();
		
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Has desbloqueado este usuario");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido desbloquear este usuario");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario ya esta desbloqueado");
					break;
				}
				default:{
					break;
				}
			}
		}
	
	};
	

	
		
	// VERIFICAR SI SON AMIGOS
	function check_amigo(info_usuario){
		
		if(informacion_propia.amigos !== null && informacion_propia.amigos.split(",").research(info_usuario.id_usuario)){
			this.classList.add("backgroundspecial");
		}
	}
	
	// VERIFICAR SI ESTA BLOQUEADO
	function check_bloqueado(info_usuario){
		
		if(informacion_propia.bloqueados !== null && informacion_propia.bloqueados.split(",").research(info_usuario.id_usuario)){
			this.style.display = "inline-block";
		}
	}
	
	
	
	
	
	// FUNCIONES PARA SABER TIEMPO DE CONEXION Y ESTADO ACTUAL
	
	function get_mensaje_conexion(segundos){
			
		if(segundos == null){
			return "";
		}
		
		segundos = Math.round(segundos / 60);
		
		var mensaje = "";
		
		if(segundos < 1){
			mensaje = " Hace un momento";
		}
		else if(segundos == 1){
			mensaje = " Hace 1 minuto";
		}
		else if(segundos > 0 && segundos < 10){
			mensaje = " Hace "+ segundos + " minutos";
		}
		else{
			mensaje = " Desconectado";
		}
		return mensaje;
	};
	
	function get_color_conexion(segundos){

		if(segundos == null){
			return "";
		}

		segundos = Math.round(segundos / 60);
		
		if(segundos < 10){
			return "colorsuccess";
		}
		else{
			return "colorcancel";
		}
	};
	
	
	
	

	
	
		
	
		// ACCIONES BOTONES COMUNIDAD ***
	function check_btn_iniciar_chat(info_amigo){
				
				this.addEventListener('click',function(){
					
					nueva_ventana = true;
					this.classList.remove("animation_basic1");
					this.classList.remove("colorprimary");
					
					iniciar_agregar_chat(info_amigo);
				});
	};
	

	
	function iniciar_agregar_chat(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		agregar_chat(resultado_agregar_chat, datos, info_amigo);	
		
		//open_chat_general();
	};
	
	function resultado_agregar_chat(respuesta,info_nuevo_chat){

		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					
					var nuevo_chat = new chat_individual(info_nuevo_chat);
					
					if(nueva_ventana == true){
					 
						nuevo_chat.firstlook();
						nueva_ventana = false;
					}
					
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido iniciar la conversacion");
					break;
				}
				case 3:{
					// ** CHAT ABIERTO ANTERIORMENTE **
					
					if(document.querySelector('[data-profile="'+info_nuevo_chat.id_usuario+'"]')){
						
						document.querySelector('[data-profile="'+info_nuevo_chat.id_usuario+'"]').retomar();
					}
					
					break;
				}
				default:{
					break;
				}
			}
		}
	};
	
	
	
	
	
	
	// *** ENVIAR MENSAJE **
	function key_iniciar_set_mensaje(e){
	
		if(e.keyCode == 13){
			e.preventDefault();
			iniciar_set_mensaje();
			return false;
		}
	
	};
	
	
	function iniciar_set_mensaje(){
		
		var mensaje = document.getElementById('txt_mensaje_chat').value;
		
		document.getElementById('txt_mensaje_chat').scrollTop = document.getElementById('txt_mensaje_chat').scrollHeight;
		
		if(mensaje.length < 1){
			return;
		}
		
		clear_mensaje();
		
		var datos = {
		
			"id_usuario_final":info_open_chat.id_usuario_final,
			"tipo": info_open_chat.tipo,
			"mensaje" : mensaje
		};
		
		set_mensaje(resultado_set_mensaje, datos);
		
	};
	
	
	function resultado_set_mensaje(respuesta){
		
		iniciar_get_mensajes();
	};
	
	
	
	function clear_mensaje(){
		document.getElementById('txt_mensaje_chat').value = "";
	};
	
	
	
	
	
	// INICIAR STREAM GET MENSAJES ***
	
	function iniciar_get_mensajes(){
		
		var datos = {
		
			"id_usuario_final": info_open_chat.id_usuario_final,
			"tipo": parseInt(info_open_chat.tipo)
		};
		
		
		get_mensajes(resultado_get_mensajes,datos);
		
	};
	
	function resultado_get_mensajes(mensajes){
	
		try{
			mensajes = JSON.parse(mensajes);
		}
		catch(err){
			console.log("Error al cargar informacion de chat");
			return;
		}
		
		//console.log(mensajes);
		
		render_mensajes(mensajes);
		
	};
	
	function render_mensajes(mensajes){
		
		var tabla_mensajes = document.getElementById('panel_mensajes');
		
		tabla_mensajes.innerHTML = "";
		
		var len = mensajes.length;
		
		var current_date = new Date();
		
		for(var i = 0; i < len; i++){
			
			var contenedor_mensaje = document.createElement("div");
			var desc_mensaje = document.createElement("span");
			var tiempo = document.createElement("div");
			
			
			contenedor_mensaje.classList.add("message");
			desc_mensaje.className = "description-mesage";
			tiempo.className = "time";
			
			// ** VERIFICAR SI ES MENSAJE GLOBAL **
			if(mensajes[i].tipo == 0){
				contenedor_mensaje.classList.add("message-type2");
			}
			else{
				
				// ** VERIFICAR SI REDACTOR DEL MENSAJE **
				if(parseInt(mensajes[i].id_usuario_inicial) == parseInt(informacion_propia.id_usuario)){
					
					contenedor_mensaje.classList.add("message-type2");
				}
				else{
					
					contenedor_mensaje.classList.add("message-type1");
				}
				
			}
			// ** VERIFICAR SI ES MENSAJE GLOBAL **
			
			
			// VERIFICAR SI ES LA MISMA FECHA PARA SOLO MOSTRAR LA HORA
			var fecha_mensaje = mensajes[i].fecha.split(" ");
			var fecha_a_mostrar;
			 
			if((parseInt(fecha_mensaje[0].split("-")[0]) == current_date.getFullYear()) && (parseInt(fecha_mensaje[0].split("-")[1]) == current_date.getMonth() +1) &&  (parseInt(fecha_mensaje[0].split("-")[2]) == current_date.getDate())){
				fecha_a_mostrar = fecha_mensaje[1];
			}
			else{
				fecha_a_mostrar = fecha_mensaje.join(" ");
			}
			
			tiempo.innerHTML = fecha_a_mostrar;
			
			if(info_open_chat.tipo == 0){
					tiempo.innerHTML = "<strong>"+ mensajes[i].nickname + "</strong> " + fecha_a_mostrar;	
				
			}
			
			
			var texto_mensaje = document.createTextNode(mensajes[i].mensaje);
			
			desc_mensaje.appendChild(tiempo);
			desc_mensaje.appendChild(texto_mensaje);
			
			
			
			contenedor_mensaje.appendChild(desc_mensaje);
			tabla_mensajes.appendChild(contenedor_mensaje);
			
		}	
		
		
		
		// ** SCROLL **
		var actual_scroll = (tabla_mensajes.scrollTop+ tabla_mensajes.clientHeight);
		
		var actual = (actual_scroll * 100) / tabla_mensajes.scrollHeight;
		
		if(Math.round(actual) > 90){
			
			tabla_mensajes.scrollTop = tabla_mensajes.scrollHeight;
		}
		if(abierto_por_click == true){
			
			tabla_mensajes.scrollTop = tabla_mensajes.scrollHeight;
			
			abierto_por_click = false;
		}
		
	};
	
	
	
	
	
		
	function render_chats(jugadores){
		
		var len = jugadores.length;
		
		table_chats.innerHTML = "";
		
		for(var i = 0; i < len; i++){
			if(document.querySelector('[data-profile="'+jugadores[i].id_usuario+'"]') === null){
				
				var nuevo_chat = new chat_individual(jugadores[i]);
			}
		}
		
	};
	
	function chat_individual(informacion_jugador){
	
		var this_chat = this;
		var info_chat = informacion_jugador;
		var cola_mensajes = 0;
		this.intervalo = setInterval(iniciar_get_cantidad_mensajes,(Math.random()*1000)+3000);
		
		var box_main = document.createElement("div");
		var icon_alarm = document.createElement("i");
		var icon_close = document.createElement("i");
		var img_avatar = document.createElement("img");
		var texto_nombre = document.createElement("span");
		
		box_main.className = "box_user_chat";
		icon_alarm.className = "alarm fa fa-bell";
		icon_close.className = "close";
		texto_nombre.className = "nombre_usuario";
		
		img_avatar.setAttribute("width","45");
		img_avatar.setAttribute("height","45");
		
		icon_close.innerHTML = "X";
		
		box_main.setAttribute("data-profile",info_chat.id_usuario);
		
		box_main.appendChild(icon_alarm);
		box_main.appendChild(icon_close);
		box_main.appendChild(img_avatar);
		box_main.appendChild(texto_nombre);
		
		table_chats.appendChild(box_main);
		
		render_informacion_chat();
		
		// ** METODOS **
		
		// ** RENDER INFORMACION ICONO **
		function render_informacion_chat(){
		
			img_avatar.src = "../imagenes/avatares/"+info_chat.id_avatar+".png";
			texto_nombre.innerHTML = info_chat.nickname;
		};
		

		// ** CLICK BTN CLOSE
		function iniciar_cerrar_chat(e){
			
				cerrar_chat(resultado_cerrar_chat,{"id_amigo":info_chat.id_usuario});
			
				try{
					table_chats.removeChild(box_main);
				}
				catch(err){
				}
			
				if(parseInt(info_open_chat.id_usuario_final) == parseInt(info_chat.id_usuario)){
					
					open_chat_general();
				}
		};
		
		function resultado_cerrar_chat(){
		};
		
		
		// ABRIR VENTANA DE CHAT 
		function mostrar_chat(e){
			
			// NO ACCION EN CASO DE BOTON CLOSE CHAT
			if(e){
				if(e.target.tagName == "I"){
					e.preventDefault();
					return;
				}
				
			}
			
			abierto_por_click = true;
			// REMOVER HOVER SHADOW A TODOS LOS CHATS
			no_seleccionar_chats();
			
			cola_mensajes = 0;
			
			info_open_chat = {
				"id_usuario_final":info_chat.id_usuario,
				"tipo": 1
			};
			
			document.getElementById('panel_mensajes').innerHTML = '<div class="preloader"> </div>'
			document.getElementById('avatar_current_chat').src = "../imagenes/avatares/"+info_chat.id_avatar+".png";
			document.getElementById('name_current_chat').innerHTML = info_chat.nickname;
			box_main.classList.add('box_user_chat_selected');
			document.getElementById('panel_mensajes').scrollTop = document.getElementById('panel_mensajes').scrollHeight;
			icon_alarm.style.display = "none";
		};
		
		
		
		// FUNCION ACTIVAR ALARMA POR MENSAJES NO LEIDOS
		function iniciar_get_cantidad_mensajes(){
			
			var datos = {
				"id_usuario_inicial":info_chat.id_usuario
			}
			get_cantidad_mensajes(resultado_get_cantidad_mensajes,datos);
		}
		function resultado_get_cantidad_mensajes(respuesta){
			
			if(respuesta = JSON.parse(respuesta)[0]){
				if(parseInt(respuesta.cantidad_mensajes) > 0){
					
					if(parseInt(respuesta.cantidad_mensajes) > cola_mensajes){
						if(info_open_chat.id_usuario_final == info_chat.id_usuario){
							return;
						}
						cola_mensajes = parseInt(respuesta.cantidad_mensajes);
						icon_alarm.style.display = "inline-block";
						play_notification();
					}
				}
			}
			
		};
		
		
		
		// ATTACH METHODS
		icon_close.addEventListener('click',iniciar_cerrar_chat, true);
		box_main.addEventListener('click',mostrar_chat,false);
		
		box_main.retomar = mostrar_chat;
		this.firstlook = mostrar_chat;
		// ** ACCIONES FIRST LINE ***
	
	};
	
	
	
	// ** FUNCION CONVETIR CHAT GENERAL **
	
	function open_chat_general(){
		
			abierto_por_click = true;
		
			no_seleccionar_chats();
		
			info_open_chat = {
				"id_usuario_final":0,
				"tipo": 0
			};
			
			document.getElementById('panel_mensajes').innerHTML = '<div class="preloader"></div>';
			
			document.getElementById('avatar_current_chat').src = "../imagenes/avatares/12.png";
			document.getElementById('name_current_chat').innerHTML = "Todos";
		
			document.getElementById('btn_chat_general').classList.add("box_user_chat_selected");
	};
	
	function no_seleccionar_chats(){
		
		var elementos = document.getElementsByClassName('box_user_chat');
		
		var len = elementos.length;
		
		for(var i = 0; i < len; i++){
			elementos[i].classList.remove("box_user_chat_selected");
		}
		
		document.getElementById('btn_chat_general').classList.remove("box_user_chat_selected");
		
	};
	
	
	
	

	
	
	
	function resizeHandlerHome(){

		var ancho = window.innerWidth;
		var alto = window.innerHeight;

		var altura_comunidad = alto - 155;
		var altura_chat = alto - 290;

		document.getElementById('contenedorcomunidad').style.maxHeight = altura_comunidad+"px";
		document.getElementById('panel_mensajes').style.maxHeight = altura_chat+"px";
		document.getElementById('panel_mensajes').style.minHeight = altura_chat+"px";

	};
	
	
// FIN SCRIPT MAIN
};