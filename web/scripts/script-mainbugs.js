
window.addEventListener('load', init_mainbugs);
                        
function init_mainbugs() {

    
var estado_bug = "0,1,2,3";
var bugs = [];	
var bug_a_eliminar;
var bug_a_cambiar_estado;
var data_table;
var current_page = 0;
	
document.getElementById('btn_sin_solucion').addEventListener('click',filtrar_bugs);
document.getElementById('btn_pendientes').addEventListener('click',filtrar_bugs);
document.getElementById('btn_solucionados').addEventListener('click',filtrar_bugs);

document.getElementById('btn_cambiar_estado').addEventListener('click',iniciar_set_estado_bug);

document.getElementById('btneliminarbug').addEventListener('click',iniciar_eliminar_bug);

	
	
// iniciar get y render de bugs reportados por categoria
iniciar_get_bugs();
	
	
	

function limpiar_filtros(){

	document.getElementById('btn_sin_solucion').classList.remove('btn_activado');
	document.getElementById('btn_pendientes').classList.remove('btn_activado');
	document.getElementById('btn_solucionados').classList.remove('btn_activado');	
};
	
	
function filtrar_bugs(e){

	limpiar_filtros();
	
	console.log(e.target.id);
	
	switch(e.target.id){
			
		case "btn_sin_solucion":{
			estado_bug = "0";
			document.getElementById('btn_sin_solucion').classList.add('btn_activado');
			break;
		}
		case "btn_pendientes":{
			estado_bug = "1";
			document.getElementById('btn_pendientes').classList.add('btn_activado');
			break;
		}
		case "btn_solucionados":{
			estado_bug = "2";
			document.getElementById('btn_solucionados').classList.add('btn_activado');
			break;
		}
		default :{
			
			estado_bug = "0,1,2,3";
			break;
		}
			
	}
	
	console.log(estado_bug);
	iniciar_get_bugs();
	
};
	

	

	
function iniciar_get_cantidad_bugs(){

	get_cantidad_bugs(resultado_get_cantidad_bugs);
	
};
	
	
function resultado_get_cantidad_bugs(resultado_cantidad){
	
   var cantidad_bugs = JSON.parse(resultado_cantidad);
	
	document.getElementById('btn_sin_solucion').innerHTML = "Sin solucion ("+cantidad_bugs.estado_0+")";
	document.getElementById('btn_pendientes').innerHTML = "Pendientes ("+cantidad_bugs.estado_1+")";
	document.getElementById('btn_solucionados').innerHTML = "Solucionados ("+cantidad_bugs.estado_2+")";
	
};

	
	
function iniciar_get_bugs(){

	var informacion_bug = {
		"estado" : estado_bug	
	};
	get_bugs(resultado_get_bugs,informacion_bug);
	
	iniciar_get_cantidad_bugs();
	
};
	
function resultado_get_bugs(param_bugs){
	
	bugs = JSON.parse(param_bugs);
	
	render_bugs();
	
	console.log(bugs);
};
	
	
    
function render_bugs(){

	var current_scroll = document.body.scrollTop;
	
	var len = bugs.length;
	
	var tabla = document.getElementById('tabla_bugs');
	
	tabla.innerHTML = "";
	
		
	if(data_table){

		var current_pagelength =  data_table.page.info().length;
		
		data_table.clear();
		data_table.destroy();
	}
	
	
	for(var i = 0; i < len; i++){
		
		var tr_bug = document.createElement("tr");
		var td_indice = document.createElement("td");
		var td_descripcion = document.createElement("td");
		var td_fecha_creacion = document.createElement("td");
		var td_estado = document.createElement("td");
		var td_acciones = document.createElement("td");
		
		var icon_informacion = document.createElement("i");
		var icon_editar = document.createElement("i");
		var icon_estado = document.createElement("i");
		var icon_eliminar = document.createElement("i");
		
		td_indice.innerHTML = i+1;
		td_descripcion.innerHTML = bugs[i].descripcion.slice(0,100);
		td_fecha_creacion.innerHTML = bugs[i].fecha_creacion;
		td_estado.innerHTML = get_estado_por_id(bugs[i].estado);
		
		icon_informacion.className = "fa fa-search-plus iconbutton";
		icon_editar.className = "fa fa-gavel iconbutton";
		icon_estado.className = "fa fa-retweet iconbutton";
		icon_eliminar.className = "fa fa-trash iconbutton";
		
		td_acciones.appendChild(icon_informacion);
		td_acciones.appendChild(icon_editar);
		td_acciones.appendChild(icon_estado);
		td_acciones.appendChild(icon_eliminar);
		
		tr_bug.appendChild(td_indice);
		tr_bug.appendChild(td_descripcion);
		tr_bug.appendChild(td_fecha_creacion);
		tr_bug.appendChild(td_estado);
		tr_bug.appendChild(td_acciones);
		
		tabla.appendChild(tr_bug);
		
		(function(){
			
			var bug = bugs[i];
			
			icon_informacion.addEventListener('click',function(){
				
				window.open("detailbug.php?id_bug="+bug.id_bug);
			});
			
			icon_editar.addEventListener('click',function(){
				
				window.open("creategame.php?game="+bug.id_juego+"&question="+bug.id_pregunta);
			});
			
			
			icon_eliminar.addEventListener('click',function(){
				
				bug_a_eliminar = bug.id_bug;
				$("#modal_eliminar_bug").modal("show");
				
			});
			
			
			icon_estado.addEventListener('click',function(){
				
				bug_a_cambiar_estado = bug.id_bug;
				
				$("#modal_cambiar_estado").modal("show");
				
				document.getElementById('seleccion_estado').selectedIndex = bug.estado;
				
			});
			
			
			
		})();
		
	}

	data_table = $('#data_table').DataTable({
		
		"language" : lenguaje_datatable,
		 "pageLength": current_pagelength,
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todo"]]
		
	});
	
	data_table.page(current_page).draw(false);
	
	document.body.scrollTop = current_scroll;
	
};
	
	
	
function get_estado_por_id(id_estado){
	
	id_estado = parseInt(id_estado);
	
	switch(id_estado){
			
		case 0:{
			
			return "sin solucion";
			break;
		}
		case 1:{
			
			return "pendiente";
			break;
		}
		case 2:{
			
			return "solucionado";
			break;
		}
	}
	
};
	
	
	
function iniciar_eliminar_bug(){
	
	eliminar_bug(resultado_eliminar_bug, bug_a_eliminar);
	
};

function resultado_eliminar_bug(param){
	
	iniciar_get_bugs();
	$("#modal_eliminar_bug").modal("hide");
	
	param = parseInt(param);
	
	switch(param){
			
		case 1:{
			
			openAlerta("alert-success","Reporte eliminado con exito");
			break;
		}
		case 2:{
			
			openAlerta("alert-danger","No se ha podido eliminar el reporte");
			break;
		}
			
	}

};
	

	
function iniciar_set_estado_bug(){
	
	var estado = document.getElementById('seleccion_estado').selectedIndex;
	
	var informacion_bug = {
	
		"id_bug" : bug_a_cambiar_estado,
		"estado" : estado
	};
	
	set_estado_bug(resultado_set_estado_bug, informacion_bug);
	
};
	
	
function resultado_set_estado_bug(param){
	
	console.log(param);
	
	$("#modal_cambiar_estado").modal("hide");
	
	iniciar_get_bugs();
};
	

	
};