
var rutaControladores = "../controladores/";

var ruta_imagenes = "../imagenes/listas/";

var ruta_audios = "../audios/";

var imagen_default = "../imagenes/interfaces/94.png";

var imagen_default_audio = "../imagenes/interfaces/83.png";

var idiomas = ["ingles","español","frances"];

var lenguaje_datatable = {
			"decimal":        "",
			"emptyTable":     "No se ha encontrado resultados",
			"info":           "Mostrando _START_ - _END_ de _TOTAL_ resultados",
			"infoEmpty":      "Mostrando 0 - 0 de 0 resultados",
			"infoFiltered":   "(Filtrar desde _MAX_ de resultados)",
			"infoPostFix":    "",
			"thousands":      ",",
			"lengthMenu":     "Ver _MENU_ resultados",
			"loadingRecords": "Cargando...",
			"processing":     "Procesando...",
			"search":         "Buscar:",
			"zeroRecords":    "No se encontraron resultados",
			"paginate": {
				"first":      "Primero",
				"last":       "Ultimo",
				"next":       "Siguiente",
				"previous":   "Anterior"
			}
};

var closeAlerta;
var openAlerta;
var tiempoAlerta;

window.addEventListener('load',init_main);
                        


function init_main(){
	
	



	
var info_usuario_general;
var intervalo_get_cantidad_mensajes_generales = setInterval(iniciar_get_cantidad_mensajes_generales,3000);
var id_current_favorito;
	
var current_directory = window.location.href.split("/").reverse()[0].split(".")[0];
if(current_directory !== "registro" && current_directory !== "login"){
	
	iniciar_get_informacion_usuario();
	iniciar_get_cantidad_mensajes_generales();
}
	
// attach evento de resize para responsive design	
window.addEventListener('resize',resizeHandler);  
	
if(document.getElementById('btn_mis_favoritos')){
	
	document.getElementById('btn_mis_favoritos').addEventListener('click',abrir_panel_favoritos);
}

	
//*** CONSEGUIR INFORMACION JUGADOR Y MOSTRARLA ****

function iniciar_get_informacion_usuario(){

	get_informacion_usuario(resultado_get_informacion_usuario);

};

// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
function resultado_get_informacion_usuario(param){

	info_usuario_general = JSON.parse(param)[0];
	
	render_datos_usuario();
};
	
	
function render_datos_usuario(){
	
	document.getElementById('imagen_top_profile').src = "../imagenes/avatares/"+info_usuario_general.id_avatar+".png";
	document.getElementById('usuario_top_profile').innerHTML = info_usuario_general.usuario;
	
	if(parseInt(info_usuario_general.rango) > 1){
		activar_iconos_level();
	}
}
	
	

	
	

if(document.getElementById('alerta_juego')){
	
	// attach evento para cerrar alerta
	document.getElementById('alerta_juego').addEventListener('click',function(){closeAlerta();});
}
	
// ** RENDER TOOLTIPS **
$('[data-toggle="tooltip"]').tooltip();  
	
// ** RENDER POPOVERS **
$('[data-toggle="popover"]').popover();  
    
setanimations();  // activar animaciones para los formularios

// modificar menu en responsive
resizeHandler();
    
    
    
	
	
	
// ALERTA MENSAJES GENERALES **
	
function iniciar_get_cantidad_mensajes_generales(){

	get_cantidad_mensajes_generales(resultado_get_cantidad_mensajes_generales);
	
};
	
		
function resultado_get_cantidad_mensajes_generales(respuesta){

	if(respuesta.indexOf("error") !== -1){
		
		console.log("error");
		return;
	}
	
	if(JSON.parse(respuesta)){
		respuesta = JSON.parse(respuesta);
		
		if(parseInt(respuesta.cantidad) > 0){
			
			document.getElementById('icon_alarm_general').classList.add("animation_basic1");
			document.getElementById('icon_alarm_general').classList.add("colornegro");	
		}
		else{
			
			document.getElementById('icon_alarm_general').classList.remove("animation_basic1");
			document.getElementById('icon_alarm_general').classList.remove("colornegro");	
		}
		
	}
	
};
	
	
	
	

openAlerta = function(tipo,mensaje){
	
	var alerta = document.getElementById('alerta_juego');
	
	clearTimeout(tiempoAlerta);
	
	alerta.style.display = "block";
	
	alerta.classList.remove("alert-info");
	alerta.classList.remove("alert-warning");
	alerta.classList.remove("alert-danger");
	alerta.classList.remove("alert-success");
	
	alerta.classList.add(tipo);
	
	document.getElementById('contenido_alerta').innerHTML = mensaje;
	
	tiempoAlerta = setTimeout(closeAlerta,10000);
	
};
	
	
// funcion para cerrar la alerta principal
closeAlerta = function(){
	
	clearTimeout(tiempoAlerta);
	
	document.getElementById('alerta_juego').style.display = "none";
}
	




function activar_iconos_level(){

	var elementos = document.querySelectorAll('[data-level="2"]');
	
	elementos.forEach(function(elemento,indice,array){
		
		elemento.classList.remove("hidden");
	});
	
};
	
	


    
    
// funcion para compactar o expandir responsive top menu
    
function resizeHandler(){
    
    var ancho = window.innerWidth;
    
    if(ancho < 380 ){
        
        contractTopMenu();
    }
    else{
        extendTopMenu();
    }
    
};
    
    
function extendTopMenu(){
    
    try{
        
        var menu = document.getElementById('topmenumain');
        var item = document.getElementById('topmenuitem1');

        menu.classList.remove('ocultmenu');
        item.classList.remove('active');
    }
    catch(err){
        console.log(err);
    }
    
};
    
function contractTopMenu(){
  
    try{
        var menu = document.getElementById('topmenumain');
        var item = document.getElementById('topmenuitem1');

        menu.classList.add('ocultmenu');
        item.classList.add('active');
        
    }
    catch(err){
        console.log(err);
    }
};
    
    
    
// funcion para compactar o expandir responsive top menu
    
    
	
	
	
	
    
    
    
    
    
    //   Animaciones para los formularios
                        
function setanimations() {
    var elementos = document.getElementsByClassName("formulary");
    var len = elementos.length;
    
    for(var i = 0; i < len; i++){
	
	   var elemento_input = elementos[i].getElementsByTagName('input')[0];
       elemento_input.addEventListener('focus',showanimationform);
       elemento_input.addEventListener('blur',hideanimationform);
       elemento_input.addEventListener('mouseenter',showanimationform);
       elemento_input.addEventListener('mouseleave',hideanimationform);
       elemento_input.addEventListener('input',showanimationform);
	
	   
		(function(){
			var elemento = elemento_input;
			var indice = i;
			elementos[indice].getElementsByTagName('span')[0].addEventListener('click',function(){
				elemento.focus();	
			});
			
		})();
		
    }    


}

function showanimationform(e){
    
    var item = e.target.parentNode;
    var elemento = item.getElementsByTagName("span")[0];
    var barra = item.getElementsByClassName("barformulary")[0];
    
    barra.classList.remove("barformularynoactive");
    barra.classList.add("barformularyactive");

    elemento.classList.remove("tituloactive");
    elemento.classList.add("titulonoactive");
};
function hideanimationform(e){
    
    var item = e.target.parentNode;
    var elemento = item.getElementsByTagName("span")[0];
    var barra = item.getElementsByClassName("barformulary")[0];

    barra.classList.remove("barformularyactive");
    barra.classList.add("barformularynoactive");

    var palabras = item.getElementsByTagName("input")[0].value.length;
    
    if(palabras < 1){
        elemento.classList.remove("titulonoactive");
        elemento.classList.add("tituloactive");
    }
};

    
    //   Animaciones para los formularios
    
    
	
function abrir_panel_favoritos(){

	$('#modal_favoritos').modal("show");
	iniciar_get_favoritos();	
};
    

    
function iniciar_get_favoritos(){

	get_favoritos(resultado_get_favoritos);
	
};

function resultado_get_favoritos(respuesta){

	if(JSON.parse(respuesta) && JSON.parse(respuesta).type == "success"){
		
		render_favoritos(JSON.parse(respuesta).juegos);
	}
	else{
	
		openAlerta("alert-danger","Ha ocurrido un error, si persiste contactar con soporte");
	}
	
};
	
	
	
function render_favoritos(juegos_favoritos){

	console.log(juegos_favoritos);
	
	var tabla = document.getElementById('tabla_favoritos');
	
	tabla.innerHTML = "";
	
	var len = juegos_favoritos.length;
	
	for(var i = 0; i < len; i++){
	
		var tr_main = document.createElement("tr");
		var td_nombre = document.createElement("td");
		var td_acciones = document.createElement("td");
		var nombre = document.createElement("a");
		var icon_edit = document.createElement("i");
		var icon_delete = document.createElement("i");
		
		nombre.setAttribute("href","play.php?game="+juegos_favoritos[i].id_tokenjuego);
		td_acciones.classList.add("text-right");
		icon_edit.className = "fa fa-pencil iconbutton";
		icon_delete.className = "fa fa-trash colorfail iconbutton";
		
		icon_edit.setAttribute("data-toggle","tooltip");
		icon_delete.setAttribute("data-toggle","tooltip");
		
		icon_edit.setAttribute("title","Renombrar");
		icon_delete.setAttribute("title","Eliminar");
		
		nombre.innerHTML = juegos_favoritos[i].nombre;
		
		td_nombre.appendChild(nombre);
		td_acciones.appendChild(icon_edit);
		td_acciones.appendChild(icon_delete);
		
		tr_main.appendChild(td_nombre);
		tr_main.appendChild(td_acciones);
		
		tabla.appendChild(tr_main);
		
		(function(){
			
			var current_game = juegos_favoritos[i];
			
			click_update_favorito.call(icon_edit,current_game);
			
			click_eliminar_favorito.call(icon_delete,current_game);
			
		})();
		
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	
};
	
if(document.getElementById('btn_aceptar_update_favorito')){
	
	document.getElementById('btn_aceptar_update_favorito').addEventListener('click',iniciar_update_favorito);
}
    
function click_update_favorito(current_game){
	
	this.addEventListener('click',function(){
		
		// ** CAMBIO DE ID PARA MODIFICACIONES EN DB **
		id_current_favorito = parseInt(current_game.id_favorito);
		
		// ** MODAL RENOMBRAR **
		$('#modal_renombrar').modal("show");
		
		// ** FOCO  INPUT **
		setTimeout(function(){$('#txt_nuevo_nombre_favorito').focus();},1000);
	});
	
};

function iniciar_update_favorito(){

	if($('#txt_nuevo_nombre_favorito').val().length < 1){
		
		openAlerta("alert-danger","debes de ingresar un nombre valido");
		return false;
	}
	
	var datos = {
		"id_favorito" : id_current_favorito,
		"nombre_favorito" : $('#txt_nuevo_nombre_favorito').val()
	};
	
	update_favorito(resultado_update_favorito,datos);
};
	
function resultado_update_favorito(respuesta){
	
	$('#modal_renombrar').modal("hide");
	iniciar_get_favoritos();
};

	
function click_eliminar_favorito(current_game){
	
	this.addEventListener('click',function(){
		
		eliminar_favorito(resultado_eliminar_favorito_top, {"id_tokenjuego": parseInt(current_game.id_tokenjuego)});
	});
};
	
function resultado_eliminar_favorito_top(respuesta){
	
	iniciar_get_favoritos();
};
    
    
};  // fin de acciones principales al cargar la pagina *** MAIN LOAD  ***











    //  ACCIONES GENERALES ************
    

    // consulta el valor de una variable pasada por el metodo get

    function getvarinurl(param){
    
    var urlsearch = window.location.search.substring(1);
    var arraytempo = urlsearch.split("&");
    var len = arraytempo.length;
    param = param.toLowerCase();
    
    for(var i = 0; i < len;i++){
        
        arraytempo[i] = arraytempo[i].split("=");
        for(var b = 0; b < arraytempo[i].length; b++){
            arraytempo[i][b] = arraytempo[i][b].toLowerCase().trim();
        }
    }
    
    for(var i = 0; i < len;i++){
        
        if(arraytempo[i][0] == param){
            
            return arraytempo[i][1];
        }
    }
    
    return "";
    
};
    
    
    // funcion para verificar si algun campo esta vacio, se debe de pasar un unico array, devuelve false en caso de encontrar un campo vacio

    function checkEmptyFields(param){
      
        try{
            
            var len = param.length;

            for(var i = 0; i < len; i++){
                if(param[i].length < 1){
                    return false;
                }
            }

            return true;

        }
        catch(err){
            console.log(err.message);
        }
    };
    


function restrict_string(e){

	var letra = String.fromCharCode(e.keyCode);
	var patron = new RegExp("^[a-zA-Z ]","gi");
	
	if(!patron.text(letra)){
		
		e.preventDefault();
		return false;
	}
	
};



Array.prototype.toggle = function(elemento_param,delete_finded){

	var len = this.length;
	
	if(delete_finded){
		delete_finded = delete_finded;
	}
	
	for(var i = 0; i < len; i++){
		
		if(this[i] == elemento_param){
			
			if(delete_finded === true){
				this.splice(i,1);
			}
			return false;
		}
	}
	
	this.push(elemento_param);
	return true;
	
};

Array.prototype.research = function(elemento_busqueda){
	
	var len = this.length;
	var coincidencias = 0;
	
	for(var i = 0; i < len; i++){
		if(parseInt(elemento_busqueda) === parseInt(this[i])){
			return true;
		}
	}
	return false;
};


String.prototype.replaceArray = function(find, replace) {
  var replaceString = this;
  var regex; 
  for (var i = 0; i < find.length; i++) {
    regex = new RegExp(find[i], "g");
    replaceString = replaceString.replace(regex, replace[i]);
  }
  return replaceString;
};


function parseBoolean(str){
	
	if(str === 'true'){
		return true;
	}
	else{
		return false;
	}
};


function noScroll() {
	
	document.body.style.overflow = "hidden";
};

function siScroll() {
	
	document.body.style.overflow = "auto";
};

    
HTMLElement.prototype.dibujar_estrellas = function(cant){
	
	var cantidad = parseInt(cant);
	
	if(isNaN(cantidad)){
		return;
	}
	this.innerHTML = "";
	
	for(var i = 0; i < cantidad; i++){
		
		var icon = document.createElement("i");
		icon.className = "fa fa-star colorprimary";
		
		this.appendChild(icon);
	}
	
};




var create_preloader = function(){

	var elemento = document.createElement("div");
	var imagen = document.createElement("img");
	
	elemento.className = "preloader-main";
	imagen.className = "preloader-animation";
	imagen.src = "../imagenes/interfaces/preloader-gears.svg";
	
	elemento.appendChild(imagen);
	
	window.document.body.appendChild(elemento);
	
	this.destruir = function(){
	
		elemento.parentElement.removeChild(elemento);
	};
	
};


    //  ACCIONES GENERALES ************
    













/*  CONSULTAS HACIA CONTROLADORES  */



 function get_lista_games(callback,informacion){
        
	 		var preloader = new create_preloader();
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"get_lista_games.php",
				  data: informacion,
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
    };



 function get_informacion_juego(callback,id_game,active_preload){
        
	 		if(active_preload !== true){
				
	 			var preloader = new create_preloader();
			}
	 
       
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"get_informacion_juego.php",
                  data:({id_game:id_game}),
                  success:function (msg){
                      
					  if(active_preload !== true){
				
					  	preloader.destruir();
					  }
                      callback(msg);
                  }
                  
              });
        
    };


 function set_token_game(callback,id_games){
        
	 		var preloader = new create_preloader();
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"set_token_game.php",
                  data:({id_game:id_games}),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};

 function get_token_game(callback,id_token){
        
        
	 		var preloader = new create_preloader();
	 
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"get_token_game.php",
                  data:({id_token:id_token}),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};


function set_configuracion_token(callback, configuraciones,id_token){
        
              $.ajax({
                  
                  type: "POST",
                  url: rutaControladores+"set_configuracion_token.php",
                  data: ({
					  
						  'configuraciones' : JSON.stringify(configuraciones),
						   'id_token' : id_token
				  		}),
                  success: function (msg){
                       
                      callback(msg);
                  }
                  
              });
        	
};


 function set_pregunta_y_respuestas(callback,paraminfo){
        
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"set_pregunta.php",
                  data:({
					  		"id_juego" : paraminfo.id_juego,
							"respuestas": paraminfo.respuestas,
							"nueva_pregunta" : paraminfo.nueva_pregunta,
							"id_pregunta" : paraminfo.id_pregunta,
							"pregunta": paraminfo.pregunta,
							"imagen" : paraminfo.imagen,
							"audio" : paraminfo.audio
						}),
                  success:function (msg){
                       
                      callback(msg);
                  }
                  
              });
        
};



 function eliminar_pregunta(callback,id_pregunta){
        
        
	 		var preloader = new create_preloader();
	 
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"eliminar_pregunta.php",
                  data:({"id_pregunta" : id_pregunta}),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};




 function crear_nuevo_juego(callback){
        
	 		var preloader = new create_preloader();
	 
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"crear_nuevo_juego.php",
                  success:function (msg){
					  
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};


function guardar_informacion_juego(callback,informacion){
        
	 		var preloader = new create_preloader();
        
			$.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"guardar_informacion_juego.php",
                  data:({
					  "id_juego" : informacion.id_juego,
					  "nombre" : informacion.nombre,
					  "idioma" : informacion.idioma,
					  "tipo" : informacion.tipo,
					  "quiz" : informacion.quiz,
					  "terminado" : informacion.terminado
					  
				  }),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};

 function eliminar_juego(callback,id_juego){
        
	 		var preloader = new create_preloader();
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"eliminar_juego.php",
                  data:({"id_juego" : id_juego}),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};





 function subir_imagen(callback,informacion){
        
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"subir_imagen.php",
                  data:informacion,
				  dataType : "html",
				  contentType : false,
				  processData : false,
				  cache: false,
                  success:function (msg){
                       
                      callback(msg);
                  }
                  
              });
        
};






 function get_imagenes(callback,busqueda){
        
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"get_imagenes.php",
                  data:({
					  "busqueda" : busqueda
				  }),
				  cache : false,
                  success:function (msg){
                       
                      callback(msg);
                  }
                  
              });
        
};

 function eliminar_imagen(callback,id_imagen){
        
        
	 		var preloader = new create_preloader();
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"eliminar_imagen.php",
                  data:({
					  "id_imagen" : id_imagen
				  }),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};





 function subir_audio(callback,informacion){
        
	 
	 		var preloader = new create_preloader();
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"subir_audio.php",
                  data:informacion,
				  dataType : "html",
				  contentType : false,
				  processData : false,
				  cache: false,
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};






 function get_audios(callback,busqueda){
        
	 
	 		var preloader = new create_preloader();
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"get_audios.php",
                  data:({
					  "busqueda" : busqueda
				  }),
				  cache: false,
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};

 function eliminar_audio(callback,id_audio){
        
	 		var preloader = new create_preloader();
        
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"eliminar_audio.php",
                  data:({
					  "id_audio" : id_audio
				  }),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};



 function set_posicion_juego(callback,datos_orden){
        
	 		var preloader = new create_preloader();
	 
              $.ajax({
                  
                  type:"POST",
                  url:rutaControladores+"set_posicion_juego.php",
                  data:({
					  "datos" : datos_orden
				  }),
                  success:function (msg){
                       
					  preloader.destruir();
                      callback(msg);
                  }
                  
              });
        
};


function set_bug(callback,datos_bug){
	
	 var preloader = new create_preloader();
	
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_bug.php",
		data: datos_bug,
			
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});
	
	
};

function get_bugs(callback,informacion_bugs){
	

	 		var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_bugs.php",
		data: informacion_bugs,
			
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});
	
	
};


function eliminar_bug(callback,id_bug){
	

	 		var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"eliminar_bug.php",
		data: ({
			
			"id_bug" : id_bug
		}),
			
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});
	
};



function set_estado_bug(callback,informacion_bug){
	
	 		var preloader = new create_preloader();

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_estado_bug.php",
		data: informacion_bug,
			
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});
	
};


function get_cantidad_bugs(callback){
	

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_cantidad_bugs.php",
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function get_informacion_usuario(callback,informacion){
	
	
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_informacion_usuario.php",
		cache: false,
		data: informacion,
			
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function set_informacion_basica(callback,informacion){

	 var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_informacion_basica.php",
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});
	
};


function set_estadisticas_juego(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_estadisticas_juego.php",
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function set_estadisticas_pregunta(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_estadisticas_pregunta.php",
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};



function set_errores(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_errores.php",
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};

function set_aciertos(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_aciertos.php",
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function set_racha(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_racha.php",
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function get_avatares(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_avatares.php",
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};

function set_avatar(callback,id_avatar){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_avatar.php",
		data: ({
			"id_avatar" : id_avatar
		}),
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function set_stream_juego(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_stream_juego.php",
		data: informacion,
		cache: false,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};

function update_stream_juego(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"update_stream_juego.php",
		data: informacion,
		cache: false,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function get_stream_juego(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_stream_juego.php",
		data: informacion,
		cache: false,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function delete_stream_juego(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"delete_stream_juego.php",
		cache: false,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};

function agregar_amigo(callback,informacion){

	
	 var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"agregar_amigo.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});	
};


function eliminar_amigo(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"eliminar_amigo.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};



function bloquear_amigo(callback,informacion){

	
	 		var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"bloquear_amigo.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});	
};


function desbloquear_amigo(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"desbloquear_amigo.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});
	
};


function agregar_chat(callback,informacion,info_chat){

	
	 var preloader = new create_preloader();
	
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"agregar_chat.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg,info_chat);
		}
		
	});	
};

function cerrar_chat(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"cerrar_chat.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function set_mensaje(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"set_mensaje.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function get_mensajes(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_mensajes.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function get_cantidad_mensajes(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_cantidad_mensajes.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function get_chats(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_chats.php",
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function get_cantidad_mensajes_generales(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_cantidad_mensajes_generales.php",
		cache: false,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function ver_mensajes_generales(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"ver_mensajes_generales.php",
		cache: false,
		success: function(msg){
			
		}
		
	});	
};



function agregar_favorito(callback,informacion){

	
	 		var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"agregar_favorito.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});	
};

function eliminar_favorito(callback,informacion){

	
	 		var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"eliminar_favorito.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});	
};


function check_favorito(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"check_favorito.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};

function get_favoritos(callback){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"get_favoritos.php",
		cache: false,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function update_favorito(callback,informacion){

	
	 var preloader = new create_preloader();
	$.ajax({
		
		type : "POST",
		url : rutaControladores+"update_favorito.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			preloader.destruir();
			callback(msg);
		}
		
	});	
};


function eliminar_usuario(callback,informacion){

	$.ajax({
		
		type : "POST",
		url : rutaControladores+"delete_usuario.php",
		cache: false,
		data: informacion,
		success: function(msg){
			
			callback(msg);
		}
		
	});	
};


function inyectar_preguntas(callback,datos){

	$.ajax({
		
		type:"post",
		url: rutaControladores+"inyectar_preguntas.php",
		data: datos,
		cache: false,
		success: function(respuesta){
			callback(respuesta);
		}
		
	});
	
};




/*  CONSULTAS HACIA CONTROLADORES  */


