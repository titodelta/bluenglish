window.addEventListener('load',init_registro);


function init_registro() {
    
    
    var fieldEmail = document.getElementById('fieldEmail');
    var fieldUser = document.getElementById('fieldUser');
    var fieldPassword = document.getElementById('fieldPassword');
    var fieldRePassword = document.getElementById('fieldRePassword');
    var fieldNickName = document.getElementById('fieldNickName');
    
    var btnregistro = document.getElementById('btnregistro');
    var panelError = document.getElementById('panelerror');
    
    
    btnregistro.addEventListener('click',enviar_datos);
    btnregistro.addEventListener('keyup',enviar_datos);
    
    fieldEmail.addEventListener('keyup',keyCheckRegister);
    fieldUser.addEventListener('keyup',keyCheckRegister);
    fieldPassword.addEventListener('keyup',keyCheckRegister);
    fieldRePassword.addEventListener('keyup',keyCheckRegister);
    
    
    clearFields(); // limpiar campos cuando se inicia
    closeError();  // cerrar ventana de error
    
    $('[data-toggle="popover"]').popover();  

	
function enviar_datos(){
	
	if(!checkRegister()){
		return false;
	}
	
	var email = fieldEmail.value;
	var usuario = fieldUser.value;
	var password = fieldPassword.value;
	var Repassword = fieldRePassword.value;
	var nickName = fieldNickName.value;
	
	$.ajax({


		type:'POST',
		url: '../controladores/setRegister.php',
		data:({
				email: email,
				usuario: usuario,
				password: password,
				Repassword: Repassword,
				nickName : nickName

			  }),
		success: function(answer){
			console.log(answer);
			errorHandler(answer);
		}


	});
	
};
    
    
    
    
    function checkRegister(){
      
    
        var email = fieldEmail.value;
        var usuario = fieldUser.value;
        var password = fieldPassword.value;
        var Repassword = fieldRePassword.value;
        var nickName = fieldNickName.value;
        
        
        var arraydatos = [email,usuario,password,Repassword,nickName];
        
        if(!checkEmptyFields(arraydatos)){
            
            errorHandler("2");
            return false;
        }
        
        if(password != Repassword){
            
            errorHandler("9");
            return false;
        }

        closeError();
		return true;
        
    };
    
    
    
    
    function keyCheckRegister(e){
      
		checkRegister();
		
        if(e.keyCode == 13){
            checkRegister();
        }
    };
    
    
    /*  Funciones de limpieza para formularios  */
    function clearFields(){
      
        fieldEmail.value = "";
        fieldUser.value = "";
        fieldPassword.value = "";
        fieldRePassword.value = "";
        
    };
    

    
    function showError(descripcion){
      
        panelError.style.display = "block";
        panelError.innerHTML = descripcion;
        
    };
    
    function closeError(){
        
        panelError.style.display = "none";
        panelError.innerHTML = "";
    }
    
    /*  Funciones de limpieza para formularios  */
        
    
    
    
    function errorHandler(error,callback){
      
        var error = parseInt(error);
        
        switch(error){
            
            
            case 0:{
                
                showError("Ha ocurrido un error");
                return;
            }
            case 1:{
                
                window.location.assign("home.php");
                return;        
            }
            case  2: {
                
                showError("Debes de ingresar todos los campos");
                return;
            }
            case 3: {
                
                showError("Datos incorrectos");
                return;
            }
                
            case 4: {
                
                showError("Solo numeros, Letras y guiones permitidos");
                return;
            }
            case 5:{
                
                showError("Debes ingresar un Usuario valido");
                return;
            }
            case 6:{
                
                showError("Debes ingresar un Email valido");
                return;
            }
            case 7:{
                
                showError("El email <strong>"+ fieldEmail.value+ "</strong> ya se encuentra registrado");
                return;
            }
            case 8:{
                
                showError("El usuario <strong>"+ fieldUser.value + "</strong> ya se encuentra registrado");
                return;
            }
            case 9:{
                
                showError("Las contraseñas no coinciden");
                return;
            }
            case 10:{
                
                showError("El usuario debe de contener menos de 20 caracteres");
                return;
            }
            case 11:{
                
                showError("El nickName debe de contener menos de 20 caracteres");
                return;
            }
            case 12:{
                
                showError("El nickName "+fieldNickName.value + " Ya se encuentra registrado");
                return;
            }
            case 20:{
                
                showError("El nickName debe de contener menos de 20 caracteres");
                return;
            }
        }
        
    };
    
    
};