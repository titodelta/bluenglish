

window.addEventListener('load', init_endgame);
                        
function init_endgame() {

var juegos;
var id_juegos;
var tokengame;
var id_juego_stream = undefined;
var totalJuegos = 0;
var totalpreguntas = 0;
var actualPregunta = 0;
var vidasActuales = 0;
var puedeJugar = false;
var contador_promedio = 0;
var preguntas = [];
var preguntas_completadas = [];
var estrellas = 3;
var racha = undefined;
var racha_local = 0;
var ultimo_indice_respuesta = 0;
var arrayganadas = [];
var arrayperdidas = [];
var nombrejuego = "";
var tiempocomienzo = 0;
var ganador = false;
var palabrasrestringidas = ["\\?","\\+",",","¿","!","-","{","}","  "];

var abbr_origin =  ["you're" ,"we're" ,"she's" ,"i'm" ,"he's" , "they're" , "don't" , "didn't", "aren't" , "isn't" , "couldn't", "mustn't","it's"];
var abbr_replace = ["you are","we are","she is","i am","he is", "they are", "do not", "did not","are not", "is not", "could not","must not","it is"];
	
var propietario = false;
var jugar_con_restriciones = false;
var informacion_jugador;
var ganador_pregunta = false;
var info_actual_pregunta = {id_pregunta: 0, id_juego: 0};
var audio = new Audio();
var type_animateID = 1;
	
var configuracionInicial = {
		
		"desordenarPreguntas" : false,
		"vidas" : 5
};
	
	
var configuraciones_avanzadas = {
		
		'configuracion1': 'true',
		'configuracion2': 'true',
		'configuracion3': 'true',
		'configuracion4': 'true',
		'configuracion5': 'true',
		'configuracion6': 'true',
		'configuracion7': 'true',
		'configuracion8': 'true',
		'configurable': 'false',
};
	

// agregar funcion de responsive para moviles
window.addEventListener('resize', resizeHandlerEndgame);   // attach evento de resize para responsive design
    
// acciones de configuracion de lista
document.getElementById('btncorazon1').addEventListener('click',function(){ setVidas(1);  });	
document.getElementById('btncorazon2').addEventListener('click',function(){ setVidas(2);  });	
document.getElementById('btncorazon3').addEventListener('click',function(){ setVidas(3);  });	
document.getElementById('btncorazon4').addEventListener('click',function(){ setVidas(4);  });	
document.getElementById('btncorazon5').addEventListener('click',function(){ setVidas(5);  });	

document.getElementById('buttonsort').addEventListener('click', controlerOrdenarPreguntas);	


//Mostrar lista de juegos a completar
document.getElementById('btn_panel_juegos').addEventListener('click',open_panel_juegos);


// cancelar y volver al inicio
document.getElementById('btnCancelar').addEventListener('click',returnHome);

// click en boton centrar para validar respuestas
document.getElementById('buttonacept').addEventListener('click', aceptar);

// tecla enter para validad respuestas
document.getElementById('buttonacept').addEventListener('keydown', keyAceptar);
	
// tecla enter para validar respuestas
document.getElementById('reply').addEventListener('keydown', keyAceptar);

// cambia el foco cuando no se puede escribir
document.getElementById('answer').addEventListener('click', clickReply); 
	
// reiniciar juego sin configuraciones
document.getElementById('btnresetgame').addEventListener('click',renovarJuego);

// btn reportar bug
document.getElementById('btnreportbug').addEventListener('click',openPanelBug);

// reiniciar juego y sus configuraciones
document.getElementById('btnconfiguregame').addEventListener('click',function(){ delete_stream_juego(resultado_delete_stream_juego); });


	
// ERROR IMAGEN
document.getElementById('imagenplay').addEventListener('error',contingencia_imagen);
	
// ERROR CON AUDIO
audio.addEventListener('error',contingencia_audio);

	

// reporte de bug

document.getElementById('btn_aceptar_reportar_bug').addEventListener('click',inicio_reportar_bug);
	
document.getElementById('btnclosereportbug').addEventListener('click',closePanelBug);

document.getElementById('btncancelreportbug').addEventListener('click',closePanelBug);

document.getElementById('texto_reportar_bug').addEventListener('keydown',key_inicio_reportar_bug);

document.getElementById('btn_reparar_bug').addEventListener('click',solucionar_bug);

	
// ** PANEL FAVORITOS **

document.getElementById('btn_aceptar_nuevo_favorito').addEventListener('click',iniciar_agregar_favorito);

document.getElementById('btn_eliminar_nuevo_favorito').addEventListener('click',iniciar_eliminar_favorito);



	
// adaptar a movil
resizeHandlerEndgame();

// empezar a recoger informacion para token pasado por metodo GET
getInfoToken();
	
// configuracion preestablecida antes de jugar
configuracionAutomatica(); 

// CONSEGUIR INFORMACION DE USUARIO
iniciar_get_informacion_usuario();	

// VERIFICAR SI EXISTE EN FAVORITOS
iniciar_check_favorito();
	
	
	

function resultado_delete_stream_juego(respuesta){
	
	//console.log(respuesta);
	window.location.reload();
};
	
	
function resultado_get_stream_juego(respuesta){

	if(respuesta == undefined || respuesta == null || respuesta == "" || respuesta.indexOf("ninguno") != -1){
		
		//console.log("nuevo stream para crear");
	}
	else{
		
		try{
			
			var stream_info = JSON.parse(respuesta)[0];
			
			
			id_juego_stream = parseInt(stream_info.id_juego_stream);
			
			stream_info.ganadas = stream_info.ganadas.split(",");
			stream_info.perdidas = stream_info.perdidas.split(",");
			stream_info.completadas = stream_info.completadas.split(",");
			
			renovarJuego(stream_info);
			
			//console.log(stream_info);
		}
		catch(err){
			//console.log(err.message);
		}
		
	}
	
};

	

//*** CONSEGUIR INFORMACION JUGADOR Y MOSTRARLA ****

function iniciar_get_informacion_usuario(){

	get_informacion_usuario(resultado_get_informacion_usuario);

};

// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
function resultado_get_informacion_usuario(param){

	informacion_jugador = JSON.parse(param)[0];

	//console.log(informacion_jugador);

	render_datos_perfil();
};
	
	
	
function render_datos_perfil(){

	var porcentaje = Math.round((parseInt(informacion_jugador.cantidad_juegos_aprendidos) * 100) / parseInt(informacion_jugador.cantidad_juegos));
	
	document.getElementById('txt_perfil_aprendidas').innerHTML = informacion_jugador.cantidad_juegos_aprendidos;
	document.getElementById('txt_perfil_nickname').innerHTML = informacion_jugador.nickname;

	document.getElementById('bar_perfil_porcentaje').style.width = porcentaje+"%";
	document.getElementById('txt_perfil_porcentaje').innerHTML = porcentaje + "%";
	document.getElementById('imagen_perfil').setAttribute("src","../imagenes/avatares/"+informacion_jugador.id_avatar+".png");

};
	
	
	
	
	
	
	
function configuracionAutomatica(){
	
	setVidas(5);
	setOrdenarPreguntas(true);
};
	
	
	
// configuracion avanzada
	
function iniciar_set_configuraciones_avanzadas(){

	set_configuracion_token(resultado_set_configuraciones_avanzadas,get_config_avanzada(),tokengame);
};
	
function resultado_set_configuraciones_avanzadas(param){
	
	empezarJuego();
}

	
function get_config_avanzada(){
	
	for(var i = 0; i < 8; i++){
		
		var indice = i+1;
		
		if(document.getElementById('check_config'+indice)){
			if(document.getElementById('check_config'+indice).checked == false){
				configuraciones_avanzadas["configuracion"+indice] = 'false';
			}
		}
	}
	
		if(document.getElementById('check_config_allow')){
			if(document.getElementById('check_config_allow').checked == true){
				configuraciones_avanzadas["configurable"] = 'true';
			}
		}
	
	
	return configuraciones_avanzadas;
	
};
	
	
function set_config_avanzada(param_config){
	
	document.getElementById('check_config1').checked = parseBoolean(param_config.configuracion1);
	document.getElementById('check_config2').checked = parseBoolean(param_config.configuracion2);
	document.getElementById('check_config3').checked = parseBoolean(param_config.configuracion3);
	document.getElementById('check_config4').checked = parseBoolean(param_config.configuracion4);
	document.getElementById('check_config5').checked = parseBoolean(param_config.configuracion5);
	document.getElementById('check_config6').checked = parseBoolean(param_config.configuracion6);
	document.getElementById('check_config7').checked = parseBoolean(param_config.configuracion7);
	document.getElementById('check_config8').checked = parseBoolean(param_config.configuracion8);
	document.getElementById('check_config_allow').checked = parseBoolean(param_config.configurable);
	
};
	
	
	
function desactivar_config_avanzada(){

	document.getElementById('check_config1').setAttribute('disabled',"disabled");
	document.getElementById('check_config2').setAttribute('disabled',"disabled");
	document.getElementById('check_config3').setAttribute('disabled',"disabled");
	document.getElementById('check_config4').setAttribute('disabled',"disabled");
	document.getElementById('check_config5').setAttribute('disabled',"disabled");
	document.getElementById('check_config6').setAttribute('disabled',"disabled");
	document.getElementById('check_config7').setAttribute('disabled',"disabled");
	document.getElementById('check_config8').setAttribute('disabled',"disabled");
	document.getElementById('check_config_allow').setAttribute('disabled',"disabled");
	
};
	
	
	

	
	
	
// acciones de configuracion
	
function setVidas(param){
	
	configuracionInicial.vidas = param;
	
	document.getElementById('textoInicialVidas').innerHTML = param < 5 ? param : "infinitas";
	
	for(var i = 1; i <= 5; i++){
		document.getElementById("btncorazon"+i).classList.remove("active");	
	}
	for(var i = 1; i <= param; i++){
		document.getElementById("btncorazon"+i).classList.add("active");	
	}
	
};
	
	
	
function setOrdenarPreguntas(param) {

	
	
	if(param == false) {
		
		// togglear boton entre activado y no activado
		document.getElementById('circucloOrdenar').classList.remove("colorprimary");
		document.getElementById('circucloOrdenar').classList.add("colorcancel");
		
		// cambiar estado de configuracion inicial
		configuracionInicial.desordenarPreguntas = false;
	}
	else{
		document.getElementById('circucloOrdenar').classList.add("colorprimary");
		document.getElementById('circucloOrdenar').classList.remove("colorcancel");
		
		// cambiar estado de configuracion inicial
		configuracionInicial.desordenarPreguntas = true;
	}
	
};
	
	
function controlerOrdenarPreguntas() {
	
		
	if(configuracionInicial.desordenarPreguntas == true) {
		
		setOrdenarPreguntas(false);
	}
	else{
		
		setOrdenarPreguntas(true);
	}
	
};
	
	
	
	
	
	
	
	
	
	
	
	
// *** CONTINGENCIA IMAGEN ***
function contingencia_imagen(e){
	
	e.target.removeAttribute("src");
}
	
// *** CONTINGENCIA AUDIO ***
function contingencia_audio(e){
	
	document.getElementById('sonido').style.display = "none";
}
		
	
	
// ID TOKEN URL
function getInfoToken(){
	
		tokengame = getvarinurl("game");

		get_token_game(checkInformacionDeJuego,tokengame);
	
}
	

//  recuperar la informacion de el token para conseguir info de los juegos
function checkInformacionDeJuego(param){  
	
	try{
		
		// ids de los juegos de el token
		var jsonJuegosToken = JSON.parse(param);
		
		id_juegos = jsonJuegosToken.juegos;
		
		// consultar la informacion de los juegos conseguidos en el token
		get_informacion_juego(mostrarInformacionDeJuego,jsonJuegosToken.juegos);  
	}	
	catch(err){
		
		window.location.assign("home.php");
	}
	
	
	// informacion de propietario y config avanzada de token
	if(parseInt(jsonJuegosToken.id_usuario_actual) == parseInt(jsonJuegosToken.id_usuario_creador)){
		
		
		set_config_avanzada({
			
			"configuracion1" : jsonJuegosToken.configuracion1,
			"configuracion2" : jsonJuegosToken.configuracion2,
			"configuracion3" : jsonJuegosToken.configuracion3,
			"configuracion4" : jsonJuegosToken.configuracion4,
			"configuracion5" : jsonJuegosToken.configuracion5,
			"configuracion6" : jsonJuegosToken.configuracion6,
			"configuracion7" : jsonJuegosToken.configuracion7,
			"configuracion8" : jsonJuegosToken.configuracion8,
			"configurable" : jsonJuegosToken.configurable,
		});

		// establecer propietario del juego
		propietario = true;
		
		document.getElementById('box_check_config_allow').style.display = "block";
		document.getElementById('btn_guardar_configuraciones').style.display = "inline-block";
		
		document.getElementById('btn_guardar_configuraciones').addEventListener('click',iniciar_set_configuraciones_avanzadas);
	}
	else{
		
		// si el juego esta cerrado para personas externas
		if(parseBoolean(jsonJuegosToken.configurable)){
			
			jugar_con_restriciones = true;
			
			desactivar_config_avanzada();
					
			configuraciones_avanzadas = {

					'configuracion1': jsonJuegosToken.configuracion1,
					'configuracion2': jsonJuegosToken.configuracion2,
					'configuracion3': jsonJuegosToken.configuracion3,
					'configuracion4': jsonJuegosToken.configuracion4,
					'configuracion5': jsonJuegosToken.configuracion5,
					'configuracion6': jsonJuegosToken.configuracion6,
					'configuracion7': jsonJuegosToken.configuracion7,
					'configuracion8': jsonJuegosToken.configuracion8,
					'configurable': jsonJuegosToken.configurable,
			};
			
			set_config_avanzada(configuraciones_avanzadas);
			
		}
	}
	
	
};

		


	

	
// mostrar la informacion del juego a reproducir.
function mostrarInformacionDeJuego(param){  // eliminar , esta funcion ira en preparegame.php
    
    juegos = JSON.parse(param);  // guardar juegos como global
	
	totalJuegos = juegos.length;
	
    //console.log(juegos); // mostrar juegos consultados
	
	
	if(totalJuegos < 1){
	
		// redirige al home cuando no se encuentra ninguna lista a mostrar
		window.location.href = "home.php";
	}
	
	else if(totalJuegos == 1) {  // nombrar listas para juego individual
		
		nombrejuego = juegos[0].nombre;
		
		document.getElementById('nombrelista').innerHTML = nombrejuego;
		document.getElementById('nombrelistaplaying').innerHTML = nombrejuego;
	}
	
	else if(totalJuegos > 1) {  // nombrar listas para Remake
		
		
		nombrejuego = "Remake ("+totalJuegos+") Listas";
		
		document.getElementById('nombrelista').innerHTML = nombrejuego;
		document.getElementById('nombrelistaplaying').innerHTML = nombrejuego;
	};
	
	
	
	document.getElementById('btnEmpezar').addEventListener('click',empezarJuego); // agregar funcion de empezar juego
	
	llenar_panel_juegos();
	
	get_stream_juego(resultado_get_stream_juego,{"id_token":tokengame});
	
};
	
	

	

// llenar la lista de los juegos que se completaran, tabla modal
function llenar_panel_juegos(){

	var tabla = document.getElementById('tablaPreguntas');
	
	var total_preguntas = 0;
	
	for(var i = 0; i < totalJuegos; i++){
		
		var tr_main = document.createElement("tr");
		var td_nombre = document.createElement("td");
		var td_preguntas = document.createElement("td");
		
		td_nombre.innerHTML = juegos[i].nombre;
		td_preguntas.innerHTML = juegos[i].preguntas.length;
		
		tr_main.appendChild(td_nombre);
		tr_main.appendChild(td_preguntas);
		
		tabla.appendChild(tr_main);
		
		total_preguntas+=  juegos[i].preguntas.length;
	}
	
	document.getElementById('txt_total_preguntas').innerHTML = total_preguntas;
	
};
	
	
function open_panel_juegos(){
	
	$('#modal_juegos').modal("show");
};
	
	

function empezarJuego(){


	if(!jugar_con_restriciones){
		
		get_config_avanzada();
		
	}
	
	configuracionInicial.avanzado = configuraciones_avanzadas;
	
	// ** CREACION STREAM Y RETORNAR SU ID **
	set_stream_juego(prepare_renovar_juego,datos_creacion_stream_juego());
	
	
};
	
	
// ** STREAM **
function datos_creacion_stream_juego(){

	var datos = {
	
		"id_tokenjuego" : tokengame,
		"configuracion1" : configuracionInicial.avanzado.configuracion1,
		"configuracion2" : configuracionInicial.avanzado.configuracion2,
		"configuracion3" : configuracionInicial.avanzado.configuracion3,
		"configuracion4" : configuracionInicial.avanzado.configuracion4,
		"configuracion5" : configuracionInicial.avanzado.configuracion5,
		"configuracion6" : configuracionInicial.avanzado.configuracion6,
		"configuracion7" : configuracionInicial.avanzado.configuracion7,
		"configuracion8" : configuracionInicial.avanzado.configuracion8,
		"desordenar" : configuracionInicial.desordenarPreguntas.toString(),
		"vidas_actuales": configuracionInicial.vidas
		
	};
	
	return datos;
};
	
	
function iniciar_update_stream_juego(){
	
	var ganadas = [];
	var perdidas = [];
	
	for(var i = 0; i < arrayganadas.length; i++){
		
		ganadas.push(arrayganadas[i].id_pregunta);
	}
	for(var b = 0; b < arrayperdidas.length; b++){
		
		perdidas.push(arrayperdidas[b].id_pregunta);
	}
	
	var datos = {
		
		"ganadas" : ganadas.join(","),
		"perdidas": perdidas.join(","),
		"completadas": preguntas_completadas.join(","),
		"vidas_actuales": vidasActuales,
		"id_juego_stream": id_juego_stream	
	}
	
	update_stream_juego(resultado_update_stream,datos);
	
	
	
} 

function resultado_update_stream(param){
	//console.log(param);
}

	
// CONSEGUIR NUEVO SOURCE ID STREAM PARA FLUJO DE DATOS CONTINUO **	
function prepare_renovar_juego(source_stream){
	
	id_juego_stream = parseInt(source_stream);
	
	console.log("Stream Source : "+id_juego_stream);
	
	if(id_juego_stream == undefined || id_juego_stream == null || isNaN(id_juego_stream)){
		
		window.location.href="home.php";
		return;
	}
	
	renovarJuego();
	
}
	
function renovarJuego(param){
	
	//console.log(param);
	
	document.getElementById('boxpreparegame').style.display = "none";
	document.getElementById('boxplaying').style.display = "block";
	
	
	if(param && param.type != "click"){
		
		configuraciones_avanzadas.configuracion1 = param.configuracion1;
		configuraciones_avanzadas.configuracion2 = param.configuracion2;
		configuraciones_avanzadas.configuracion3 = param.configuracion3;
		configuraciones_avanzadas.configuracion4 = param.configuracion4;
		configuraciones_avanzadas.configuracion5 = param.configuracion5;
		configuraciones_avanzadas.configuracion6 = param.configuracion6;
		configuraciones_avanzadas.configuracion7 = param.configuracion7;
		configuraciones_avanzadas.configuracion8 = param.configuracion8;	
		
		configuracionInicial.avanzado = configuraciones_avanzadas;
		
		configuracionInicial.desordenarPreguntas = parseBoolean(param.desordenar);
		configuracionInicial.vidas = parseInt(param.vidas_actuales);
		
	}
	
	
	
	 totalpreguntas = 0;
	 actualPregunta = 0;
	 puedeJugar = true;
	 preguntas = [];
	 preguntas_completadas = [];
	 arrayganadas = [];
	 arrayperdidas = [];
	 vidasActuales = parseInt(configuracionInicial.vidas);
	 tiempocomienzo = new Date().getTime();
     dibujarVidas(vidasActuales);
	
	
	// TOMAR PREGUNTAS DE CADA JUEGO Y COMBINARLAS
	for(var i = 0; i < totalJuegos; i++){
		
		for(var j = 0; j < juegos[i].preguntas.length; j++){
			
			
			var current_pregunta = juegos[i].preguntas[j];
			
			preguntas.push(current_pregunta);
			
			if(param && param.type != "click"){
				
				if(param.ganadas.research(current_pregunta.id_pregunta) === true){
					
					agregarPregunta(arrayganadas,current_pregunta); // agregar al array de ganadas
				}
				else if(param.perdidas.research(current_pregunta.id_pregunta) === true){
					
					agregarPregunta(arrayperdidas,current_pregunta); // agregar al array de ganadas
				}
				
				if(param.completadas.research(current_pregunta.id_pregunta) === true){
					
					actualPregunta++;
					preguntas_completadas.push(current_pregunta.id_pregunta);
				}
			}
			
			
		}
		
	}
	
	
	if(param && param.type != "click"){
		vidasActuales = parseInt(param.vidas_actuales);
		dibujarVidas(vidasActuales);
	}
	
	
	dibujarEstrellas();
	
	// *** CANTIDAD PREGUNTAS ***
	totalpreguntas = preguntas.length;
	
	// ** PRECARGA DE IMAGENES **
	precargar_imagenes();
	
	
	// ** UPDATE GANADAS Y PERDIDAS **
	document.getElementById('txtganadas').innerHTML = arrayganadas.length;
	document.getElementById('txtperdidas').innerHTML = arrayperdidas.length;
	
	
	// *** DESORDENAR PREGUNTAS ***
	if(configuracionInicial.desordenarPreguntas){

			preguntas.sort(desordenar);
	}
	
	var len_preg = preguntas.length;
	for(var i = 0; i < len_preg; i++){
		
		for(b = 0; b < len_preg; b++){
			
			if(preguntas_completadas.research(preguntas[b].id_pregunta) === true){
				var resultado = preguntas.splice(b,1);
				preguntas.unshift(resultado[0]);
			} 
			
		}
		
	}
	
	
	console.log(preguntas);
	
	// *** MOSTRAR ACIERTOS Y ERRORES ***
	if(parseBoolean(configuracionInicial.avanzado.configuracion6)){
		
		document.getElementById('estadisticas').classList.remove('hidden');
	}
	
	// *** MOSTRAR VIDAS ***
	if(parseBoolean(configuracionInicial.avanzado.configuracion7)){
		
		document.getElementById('boxhearts').classList.remove('hidden');
	}
	
	// *** MOSTRAR PROGRESO ***
	if(parseBoolean(configuracionInicial.avanzado.configuracion8)){
		
		document.getElementById('contenedor_progreso').classList.remove('hidden');
	}
	
	
	// ** INFORMACION DE USUARIO - RACHA ***
	get_informacion_usuario(function(respuesta){
		
		racha = !isNaN(parseInt(JSON.parse(respuesta)[0].racha)) ? parseInt(JSON.parse(respuesta)[0].racha) : undefined;
	});
	
	
	
	// SIGUIENTE PREGUNTA
	getNextQuestion();
	
};
	

	
// conseguir siguiente pregunta
function getNextQuestion() {
	
	
	if(ganador_pregunta === true){
		actualPregunta++;
	}
	
	// CAMBIAR ESTADO DE JUGADOR
	puedeJugar = true;
	
	// Eliminar AURA TXT ANSWER
	document.getElementById('reply').style.boxShadow = "none";
	
	contador_promedio++;
	
	// **  COMPLETAR JUEGO ***
	
	console.log(actualPregunta + " De "+totalpreguntas);
	
	if(actualPregunta >= totalpreguntas){
		console.log("finalizo");
		ganador = true;
		iniciar_set_estadistica_juego();	
		return;
	}
	
	info_actual_pregunta.id_juego = preguntas[actualPregunta].id_juego;
	info_actual_pregunta.id_pregunta = preguntas[actualPregunta].id_pregunta;
	
	iniciar_update_stream_juego();
	
	// *** ACTUALIZAR PROGRESO  ****
	document.getElementById('textProgress').innerHTML = actualPregunta + " / " + totalpreguntas;	
	
	// *** ACTUALIZAR BARRA DE PROGRESO ****
	document.getElementById('status').style.width = Math.round((actualPregunta * 100) / totalpreguntas)+"%"; 
	
	
	//***  PREGUNTA   *****
	if(parseBoolean(configuracionInicial.avanzado.configuracion1)){
		// mostrar nueva pregunta
		document.getElementById('question').innerHTML = preguntas[actualPregunta].pregunta;
	}

	
	// *** OCULTAR TABLA DE RESPUESTAS ***
	document.getElementById('tableresponses').style.display = "none";

	
	// ** LIMPIAR INPUT REPLY **
	document.getElementById('reply').classList.remove('reply-lose');
	document.getElementById('reply').classList.remove('reply-win');
	
	// CAMBIAR TEXTO BOTON CHECK / NEXT
	document.getElementById('buttonacept').innerHTML = "Check";
	
	
	// ** MOSTRAR IMAGEN **
	if(parseBoolean(configuracionInicial.avanzado.configuracion3)){
		
		// verificar imagen asignada
		if(preguntas[actualPregunta].imagen == null || preguntas[actualPregunta].imagen == undefined){

			document.getElementById('imagenplay').removeAttribute("src");
		}
		else{

			document.getElementById('imagenplay').src = ruta_imagenes + preguntas[actualPregunta].imagen + ".jpg";
		}
	}
	
	
	
	//  ENABLE INPUT RESPUESTA
	document.getElementById('reply').removeAttribute("disabled");
	
	// LIMPIAR CAMPO DE RESPUESTA
	document.getElementById('reply').value = "";
	
	// *** MOSTRAR AUDIO ****
	if(parseBoolean(configuracionInicial.avanzado.configuracion4)){
		
		render_audio();	
	}
	
	
	// ACTIVAR ICONO SI ES PREGUNTA 
	document.getElementById('icon_question').classList.add("hidden");
	
	if(preguntas[actualPregunta].pregunta.indexOf("?") !== -1){
		document.getElementById('icon_question').classList.remove("hidden");
	}
	
	
};


	
// cambia el foco si no puede escribir
function clickReply(){
	
		if(puedeJugar === false && document.getElementById('buttonacept').value == "Check"){
			puedeJugar = true;
			console.log("Error boleano");
		}
	
		if(puedeJugar === false){
			document.getElementById('buttonacept').focus();
		}
		
}
	
// funcion de verificar tecla de enter
	
function keyAceptar(e) {
	
	var codigo = e.keyCode;
	
	if(codigo == 13){
		e.preventDefault();
		aceptar();
		
		if(puedeJugar === true){
		
			document.getElementById('reply').focus();
		}
		else{
			
			document.getElementById('buttonacept').focus();
		}
		
	}
	
};
	

// funcion de aceptar. Verifica respuestas o pasa a la siguiente
		
function aceptar(){
	
	if(puedeJugar === false){
		getNextQuestion();
		return;
	}
	else if(puedeJugar === true){
		checkReply();
		return;
	}
}
	
	
	
//** LIMPIAR RESPUESTA **
	
function limpiarRestringidas(param){
	
	param = param.trim();
	param = param.toLowerCase();
	for(var i = 0; i < palabrasrestringidas.length; i++){
		var patron = new RegExp(palabrasrestringidas[i],"gi");
		param = param.replace(patron,"");
		
		param = param.replaceArray(abbr_origin,abbr_replace);
		
	}
	return param;
}



// ** VERIFICAR RESPUESTA JUGADOR ***	
function checkReply(){
	
	// respuesta de jugador
	var respuestaUser = document.getElementById('reply').value;
	
	if(respuestaUser.length < 1){
		
		document.getElementById('reply').focus();
		return;
	}
	
	puedeJugar = false;
	
	// respuestas a comparar

	var lenRespuestas = preguntas[actualPregunta].respuestas.length;
	
	// coincidencias con las respuestas
	var aciertos = 0;

	
	// restringir campo de respuesta
	document.getElementById('reply').setAttribute("disabled","disabled");
	
	// Cambiar texto de boton aceptar
	document.getElementById('buttonacept').innerHTML = "Next";
	
		
	
	// comparar cada respuesta sql con la respuesta del jugador
	for(var i = 0; i < lenRespuestas; i++){
		
		// limpiar de caracteres erroneos a respuesta sistema y usuario
		if(limpiarRestringidas(respuestaUser) == limpiarRestringidas(preguntas[actualPregunta].respuestas[i].respuesta)){
			
			ultimo_indice_respuesta = i;
			aciertos++;
		}
		
	} // fin for para verificar respuestas
	
	
	if(aciertos > 0){
	   
		ganar();
	}
	else{
		perder();
	}
	
	document.body.scrollTop = document.body.scrollHeight;
	
};
	

	
	

	
// ** GANAR **
function ganar(){
	
	ganador_pregunta = true;
	
	
	agregarPregunta(arrayganadas,preguntas[actualPregunta]); // agregar al array de ganadas
	
	// ** PREGUNTAS TERMINADAS ***
	
	agregar_pregunta_completada(preguntas[actualPregunta]);
	
	correctAnswers();

	iniciar_set_estadisticas_pregunta(true,preguntas[actualPregunta].respuestas[ultimo_indice_respuesta].respuesta);
	
	ultimo_indice_respuesta = 0;
	
	// ** SET RACHA ***
	nueva_racha(true);
	
	// mostrar tabla de respuestas correctas
	
	// config avanzada, mostrar respuestas
	if(parseBoolean(configuracionInicial.avanzado.configuracion2)){
		
		document.getElementById('texto_ganador').innerHTML = "GANASTE";
		document.getElementById('tableresponses').style.display = "block";
		document.getElementById('tableresponses').classList.remove("table_perder");
		document.getElementById('tableresponses').classList.add("table_ganar");
	}
	
	document.getElementById('reply').classList.remove('reply-lose');
	document.getElementById('reply').classList.add('reply-win');
	
	// ** ACIERTOS - ERRORES Y NUEVA PALABRA APRENDIDA **
	
	
	
};
	
// ** PERDER **
function perder(){

	ganador_pregunta = false;
	
	// agregar al array de perdidas
	console.log("Agregar perdida");
	agregarPregunta(arrayperdidas,preguntas[actualPregunta]); 
		
	// ** ACIERTOS - ERRORES Y NUEVA PALABRA APRENDIDA **
	iniciar_set_estadisticas_pregunta(false,preguntas[actualPregunta].respuestas[ultimo_indice_respuesta].respuesta);
	
	ultimo_indice_respuesta = 0;
	
	// ** MODIFICAR RACHA **
	nueva_racha(false);
	
	dibujarEstrellas();
	
	// mostrar tabla de respuestas correctas
	correctAnswers();
	
	info_actual_pregunta.id_juego = preguntas[actualPregunta].id_juego;
	info_actual_pregunta.id_pregunta = preguntas[actualPregunta].id_pregunta;
	
	// Agregar al array de preguntas a repetir
	renovarPregunta();
	

	// config avanzada, mostrar respuestas
	if(parseBoolean(configuracionInicial.avanzado.configuracion2)){

		document.getElementById('texto_ganador').innerHTML = "PERDISTE";
		document.getElementById('tableresponses').style.display = "block";
		document.getElementById('tableresponses').classList.remove("table_ganar");
		document.getElementById('tableresponses').classList.add("table_perder");
	}
	
	
	document.getElementById('reply').classList.remove('reply-win');
	document.getElementById('reply').classList.add('reply-lose');
	
	// restar vidas
	restarVidas();
	
	
	
};
	
	

	
function agregar_pregunta_completada(objeto_pregunta){
	if(preguntas_completadas.research(objeto_pregunta.id_pregunta) !== true){
		
		preguntas_completadas.push(objeto_pregunta.id_pregunta);
	}
};
	
// ** AGREGAR PREGUNTA A GANADAS Y PERDIDAS ***
function agregarPregunta(tipoArray,objetoPregunta){
	
	var pregunta = objetoPregunta.pregunta;
	var respuesta = objetoPregunta.respuestas[0].respuesta;
	var id_pregunta = parseInt(objetoPregunta.id_pregunta);

	// agregar pregunta en array correspondiente
	checkPregunta(tipoArray,pregunta,respuesta,id_pregunta);
	
};

// validar si la pregunta ya se ha ganado o perdido	
function checkPregunta(arrayparam,Rpregunta,Rrespuesta,Rid_pregunta) {
	
	var lenganadas = arrayganadas.length;
	var lenperdidas = arrayperdidas.length;
	
	
	for(var i = 0; i < arrayganadas.length; i++){
		if(parseInt(arrayganadas[i].id_pregunta) == parseInt(Rid_pregunta)){
			return;
		}
	}
	for(var j = 0; j < arrayperdidas.length; j++){
		if(parseInt(arrayperdidas[j].id_pregunta) == parseInt(Rid_pregunta)){
			return;
		}
	}
	
	arrayparam.push({"id_pregunta": Rid_pregunta,"pregunta": Rpregunta,"respuesta": Rrespuesta});
	
	document.getElementById('txtganadas').innerHTML = arrayganadas.length;
	document.getElementById('txtperdidas').innerHTML = arrayperdidas.length;
		
};
	
	
	
function renovarPregunta(){
	
	var preguntanew = preguntas.splice(actualPregunta ,1);
	
	preguntas.push(preguntanew[0]);
	
}
	
	
function correctAnswers(){

	// limpiar tabla de respuestas correctas
	document.getElementById('listarespuestas').innerHTML = "";
	
	var len = preguntas[actualPregunta].respuestas.length;
	
	for(var i = 0; i < len; i++){
		
		var respuesta = document.createElement("li");
		
		respuesta.innerHTML = preguntas[actualPregunta].respuestas[i].respuesta;
		
		document.getElementById('listarespuestas').appendChild(respuesta);
	}
	
};
	
	
	
	
	
	
//** ESTRELLAS ***
function dibujarEstrellas(){
	
	if(arrayperdidas.length == 0){
		estrellas = 3;
	}
	else if(arrayperdidas.length == 1){
		estrellas = 2;
	}
	else if(arrayperdidas.length > 1){
		estrellas = 1;
	}
	
	document.getElementById('estrella1').style.display = "none";
	document.getElementById('estrella2').style.display = "none";
	document.getElementById('estrella3').style.display = "none";
	
	for(var i = 0; i < estrellas; i++){
		
		var indice = i+1;
		document.getElementById('estrella'+indice).style.display = "inline-block";
	}
	
};

function restarVidas(){

	if(vidasActuales < 5){
			
			if(vidasActuales == 1){
				
				ganador = false;
				
				iniciar_set_estadistica_juego();
				
				return;
			}
			else{
				
				vidasActuales--;
				dibujarVidas(vidasActuales);
			}
		
	}
};
	

	
	
function dibujarVidas(param){
	
	for(var i = 1; i <= 4; i++){
		document.getElementById('corazon'+i).style.display = "none";
	}
	
	if(vidasActuales < 5){
			
		for(var i = 1; i <= param; i++){
			document.getElementById('corazon'+i).style.display = "inline-block";
		}
	}
	
};
	
	
// **** REPRODUCIR AUDIO *****
function render_audio(){
		if(preguntas[actualPregunta].audio == null || preguntas[actualPregunta].audio == undefined){
			
		audio.pause();
		audio.src = "";
		
		document.getElementById('btn_volumen_1').removeEventListener('click',audio_normal);
		document.getElementById('btn_volumen_2').removeEventListener('click',audio_lento);
		
		document.getElementById('sonido').style.display = "none";
		
	}else{
		
		document.getElementById('animate-circle').style.display = "block";
		
		audio.currentTime = 0;
		audio.src = ruta_audios + preguntas[actualPregunta].audio+".mp3";
		audio.load();

		document.getElementById('btn_volumen_1').addEventListener('click',audio_normal);
		document.getElementById('btn_volumen_2').addEventListener('click',audio_lento);
		
		document.getElementById('sonido').style.display = "inline-block";
	
		//** REPRODUCIR AUDIO AUTOMATICAMENTE ***
		if(parseBoolean(configuracionInicial.avanzado.configuracion5)){
		
			audio_normal();
			
		}
		
	}
};
	
	
	
	
	
function iniciar_set_estadistica_juego(){

	console.log("enviando estadisticas");
	var tiempofinal = new Date().getTime();
	var segundos =  Math.floor((tiempofinal - tiempocomienzo) / 1000);
	
	var remake = totalJuegos > 1? 1 : 0;
	var number_ganador = ganador == true ? 1 : 0;
	
	var informacion = {
		"id_juego" : id_juegos,
		"segundos" : segundos,
		"aciertos" : arrayganadas.length,
		"errores" : arrayperdidas.length,
		"remake": remake,
		"preguntas": preguntas.length,
		"id_tokenjuego" : parseInt(tokengame),
		"ganador" : number_ganador,
		"promedio" : segundos/contador_promedio,
		"estrellas" : estrellas
	}
	
	
	set_estadisticas_juego(enviarInformacion,informacion);
	
};
	
	
	
// ** ESTADISTICAS ACIERTO ERRORES, Y PALABRA APRENDIDA ***
function iniciar_set_estadisticas_pregunta(tipo, palabra){
	
	var info_aciertos = tipo == true ? 1 : 0;
	var info_errores = tipo == true ? 0 : 1;
	
	var datos = {
		
		"aciertos" : info_aciertos,
		"errores" : info_errores,
		"palabras" : palabra
	}
	
	set_estadisticas_pregunta(resultado_set_estadisticas_pregunta,datos);
}
	
function resultado_set_estadisticas_pregunta(respuesta){

};
	
	
// ** MODIFICAR RACHA ***
function nueva_racha(tipo){
	
	if(tipo == true){
		
		// ** INCREMENTAR RACHA **
		racha_local++;

		if(racha != undefined || racha != null){

			if(racha_local > racha){
				
				set_racha(resultado_nueva_racha,{"racha": racha_local});
			}

		}
	}
	else{
		racha_local = 0;
	}
	
};
	
function resultado_nueva_racha(respuesta){
	//console.log(respuesta);
};
	
	
	
	

// enviar las estadisticas del juego al panel de resultados
function enviarInformacion(respuesta){
	
	console.log("enviando datos fin");
	
	    tiempofinal = new Date().getTime();
	
		var minutos, segundos, duracion;
		var segundos =  Math.floor((tiempofinal -tiempocomienzo) / 1000);
	
		duracion = "";
	
		minutos = Math.floor(segundos / 60);
	
		if(minutos > 0){
			segundos -= (minutos * 60);
			duracion += minutos +" Minutos ";
		}
		   duracion += segundos + " segundos";
			
	
		var formulario = document.createElement("form");
		var estadojuego = document.createElement("input");
		var boton = document.createElement("input");
		
		var jasonbuild = {
			
				"estadojuego":ganador,
				"listaganadas": arrayganadas,
				"listaperdidas": arrayperdidas,
				"nombrejuego": nombrejuego,
				"lenpreguntas": totalpreguntas,
				"duracion": duracion,
				"locacion": window.location.href,
				"promedio" : segundos/contador_promedio,
				"estrellas" : estrellas
		};
		
		var jasonenvio = JSON.stringify(jasonbuild);
		
		formulario.setAttribute("name","form1");
		formulario.setAttribute("action","endgame.php");
		formulario.setAttribute("method","post");
		
		estadojuego.setAttribute("type","hidden");
		estadojuego.setAttribute("value",jasonenvio);
		estadojuego.setAttribute("name","data");
	
		boton.setAttribute('type',"submit");

	
		formulario.appendChild(estadojuego);
		formulario.appendChild(boton);
		window.document.body.appendChild(formulario);
		formulario.submit();
};
	
	
	
// function para retornar al home
function returnHome() {
	
	window.location.assign("home.php");
};
	
	
// iniciar precarga de imagenes en cache
function precargar_imagenes(){
	
	for(var i = 0; i < totalpreguntas; i++){
		
		if(preguntas[i].imagen == null){
			
			continue;
		}
		
		var imagen = new Image();
		
		imagen.src = ruta_imagenes+preguntas[i].imagen+".jpg";

	}
	
};
	
	
	
	
	

// funciones panel reporte de bug
	

function closePanelBug() {
	
	document.getElementById('panelreportbug').style.display = "none";
	document.getElementById('texto_reportar_bug').value = "";
	
};
	
function openPanelBug() {
	
	document.getElementById('panelreportbug').style.display = "block";
	document.getElementById('texto_reportar_bug').value = "";
	document.getElementById('texto_reportar_bug').focus();
	
};
	
function key_inicio_reportar_bug(e){
	
	if(e.keyCode == 13){
		inicio_reportar_bug();
		e.preventDefault();
		return;
	}
};

	
function inicio_reportar_bug() {

	var id_num_bug = puedeJugar === false ? actualPregunta -1 : actualPregunta;

	
	var nuevo_bug = 0;
	var descripcion = document.getElementById('texto_reportar_bug').value;
	var id_juego = preguntas[id_num_bug].id_juego;
	var id_pregunta = preguntas[id_num_bug].id_pregunta;
	var estado = 0;
	
	if(descripcion.length < 1){
		
		openAlerta("alert-danger","Debes ingresar un mensaje valido");
		return;
	}
	
	var datos_bug = {
		
			"nuevo_bug" : nuevo_bug,
			"descripcion" : descripcion,
			"id_juego" : id_juego,
			"id_pregunta" : id_pregunta,
			"estado" : estado
	};
	

	set_bug(resultado_reportar_bug,datos_bug);
	
	
};
	
	
function resultado_reportar_bug(param_respuesta){
	
	closePanelBug();
	
	switch(parseInt(param_respuesta)){
			
		case 1:{
			openAlerta("alert-success","Tu mensaje ha sido enviado");
			break;
		}
		case 2:{
			
			openAlerta("alert-danger","Hemos tenido problemas con el envio de tu mensaje. Intentalo mas tarde");
			break;
		}
			
		default:{
			openAlerta("alert-danger","Hemos tenido problemas con el envio de tu mensaje. Intentalo mas tarde");
			break;
		}
			
	}
	
};
	
	
// ** SOLUCION DE BUG INMEDIAtO ***

function solucionar_bug(){

	
	var id_juego = info_actual_pregunta.id_juego;
	var id_pregunta = info_actual_pregunta.id_pregunta;
	
	var nuevo_bug = 0;
	var descripcion = "Descripcion automatica";
	var estado = 2;
	
	window.open("creategame.php?game="+id_juego+"&question="+id_pregunta,"_blank");
	
};
	
	
	
// ** REPRODUCCION DE AUDIO
function audio_normal(){
	
		
	audio.playbackRate = 1;
	audio.currentTime = 0;
	audio.play();
	
	type_animateID = 1;
}
	
function audio_lento(){
	
	audio.playbackRate = 0.78;
	audio.currentTime = 0;
	audio.play();

	type_animateID = 2;
}	


audio.addEventListener('timeupdate',animate_audio);
audio.addEventListener('ended',des_animate_audio);

function des_animate_audio(){

	$('#animate-circle').fadeOut();

};
	
function animate_audio(tipo){

	$('#animate-circle').fadeIn();
	
	if(type_animateID == 1){
		document.getElementById('animate-circle').style.right = "30px";
	}
	else if(type_animateID == 2){
		document.getElementById('animate-circle').style.right = "-5px";
	}

};

	
	
/// *** DESORDENAR LISTA ***
	
function desordenar(a,b){
	
   if((Math.round(Math.random()) % 2) == 1){
	   return -1
   }
	else{
		return 1;
	}
};
	
	

	
	
// ** VERIFICAR SI SE ENCUENTRA EN FAVORITOS **
	
function iniciar_check_favorito(){

	var datos = {
		"id_tokenjuego": tokengame
	};
	
	check_favorito(resultado_check_favorito,datos);
};
	
	
function resultado_check_favorito(respuesta){

	if(JSON.parse(respuesta) && parseInt(JSON.parse(respuesta).cantidad) > 0){
		
		document.getElementById('btn_nuevo_favorito').addEventListener('click',abrir_eliminar_favorito);
		document.getElementById('btn_nuevo_favorito').removeEventListener('click',abrir_nuevo_favorito);
		
		favorito_existente();
		
	}
	else if(JSON.parse(respuesta) && parseInt(JSON.parse(respuesta).cantidad) == 0){
		
		document.getElementById('btn_nuevo_favorito').addEventListener('click',abrir_nuevo_favorito);
		document.getElementById('btn_nuevo_favorito').removeEventListener('click',abrir_eliminar_favorito);
		
		favorito_no_existente();
	}
	else{
		
	}
	
	$('[data-toggle="tooltip"]').tooltip();
};
	
	
	
function favorito_existente(){
	
	$('#btn_nuevo_favorito').attr("data-original-title","eliminar de favoritos");
	$('#btn_nuevo_favorito').addClass("colorprimary");
	
};
function favorito_no_existente(){
	
	$('#btn_nuevo_favorito').attr("data-original-title","Agregar a favoritos");
	$('#btn_nuevo_favorito').removeClass("colorprimary");
}
	
	

	
	
// ** ABRIR PANEL AGREGAR NUEVO FAVORITO **
	
function abrir_nuevo_favorito(){

	
	// ** ABRIR MODAL **
	$('#modal_nuevo_favorito').modal("show");
	
	// ** TOMAR NOMBRE ACTUAL DE JUEGO **
	$('#txt_nuevo_favorito').val(nombrejuego);
	
	// ** TOMAR FOCO EN TXT **
	var tiempo = setTimeout(function(){document.getElementById('txt_nuevo_favorito').focus();},1000);
	
};
	
	
	
// ** ABRIR PANEL ELIMINAR NUEVO FAVORITO **
	
function abrir_eliminar_favorito(){

	$('#modal_eliminar_favorito').modal("show");
	
};
	
	
	
// ** AGREGAR A FAVORITOS **
	
function iniciar_agregar_favorito(){

	if($('#txt_nuevo_favorito').val().length < 1){
		return false;
	}
	
	var datos = {
		
		"id_tokenjuego" : tokengame,
		"nombre_juego" : $('#txt_nuevo_favorito').val()
	};
	
	agregar_favorito(resultado_agregar_favorito, datos);
	
};
	
	
function resultado_agregar_favorito(respuesta){

	$('#modal_nuevo_favorito').modal("hide");
	
	if(JSON.parse(respuesta)){
		
		respuesta = JSON.parse(respuesta);
		
		if(respuesta.type == "success"){
			
			openAlerta("alert-success","Juego agregado");
		}
		else if(respuesta.type == "exists"){
			
			openAlerta("alert-warning","Ya tenias agregado este juego");
		}
		
		iniciar_check_favorito();
		
	}
	else{
		console.log("error");
	}
	
};
	

	
	
// ** ELIMINAR DE FAVORITOS **
	
function iniciar_eliminar_favorito(){

	var datos = {
	
		"id_tokenjuego" : tokengame
	};
	
	eliminar_favorito(resultado_eliminar_favorito,datos);
	
};

function resultado_eliminar_favorito(respuesta){

	if(JSON.parse(respuesta) && JSON.parse(respuesta).type == "success"){
		
		openAlerta("alert-success","Juego eliminado de tu lista");
	}
	else{
		openAlerta("alert-danger","No has podido eliminar este juego");
	}
	
	$('#modal_eliminar_favorito').modal("hide");
	
	iniciar_check_favorito();
	
};
	
	
	
	
	
	
// funciones para el responsive
    
function resizeHandlerEndgame() {
    
    var ancho = window.innerWidth;
    
    if(ancho < 880 ){
        
        extendProfile();
    }
    else{
        contractProfile();
    }
    
};
    
    
    
function extendProfile(){
    
    var boxplaying = document.getElementById('boxmainplay');
    
    var barstatus = document.getElementById('contenedorstatus'); // barra de status top
    
    var leftmenu = document.getElementById('leftmenu'); // barra de status top
    
    var rightmenu = document.getElementById('rightmenu'); // barra de status top
    
    
    boxplaying.classList.add("responsive");
    barstatus.classList.add("responsivestatus"); 
    leftmenu.classList.add("responsive");
    rightmenu.classList.add("responsive");
    
    
};
    
function contractProfile(){
    
    
    var boxplaying = document.getElementById('boxmainplay');
    
    var barstatus = document.getElementById('contenedorstatus'); // barra de status top
    
    var leftmenu = document.getElementById('leftmenu'); // barra de status top

    var rightmenu = document.getElementById('rightmenu'); // barra de status top
    
    boxplaying.classList.remove("responsive");
    barstatus.classList.remove("responsivestatus");
    leftmenu.classList.remove("responsive");
    rightmenu.classList.remove("responsive");
    
};
    
    

// funciones para el responsive

    
    
    
    
    
};