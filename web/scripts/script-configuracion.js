window.addEventListener('load',inicio_config);

function inicio_config(){

	// informacion obtenida del jugador
	var informacion_jugador;
	
	var informacion_consultante;
	
	var ruta_avatares = "../imagenes/avatares/";
	
	var id_usuario_url = undefined;
	
	var busqueda_general = true;
	
	// referencia de panel alerta
  	var panelError = document.getElementById('panel_error');
  	
	// ** DATA TABLE INFO **
	var data_table;

	var current_page = 0;
	
	// ** VERIFICAR DATOS BASICOS **
	document.getElementById('txt_email').addEventListener('keyup',keyCheckRegister);
	document.getElementById('txt_usuario').addEventListener('keyup',keyCheckRegister);
	document.getElementById('txt_nickname').addEventListener('keyup',keyCheckRegister);
	document.getElementById('txt_password').addEventListener('keyup',keyCheckRegister);

	// ** ENVIAR DATOS INFORMACION BASICA ***
	document.getElementById('btn_enviar_informacion_basica').addEventListener('click',enviar_datos);
	
	
	document.getElementById('btn_buscar_usuario').addEventListener('keyup',render_usuarios);
	
	
	// ** CONSEGUIR INFORMACION DE JUGADOR ***
	iniciar_get_informacion_consultante();
	
	
	
	function iniciar_get_informacion_consultante(){
	
		get_informacion_usuario(resultado_get_informacion_consultante);
		
		
	};
	function resultado_get_informacion_consultante(param){
		
		if(JSON.parse(param)){
			
			informacion_consultante = JSON.parse(param)[0];
			iniciar_get_informacion_usuario();
		}
		
	};
	
	
	
	
	//*** CONSEGUIR INFORMACION JUGADOR Y MOSTRARLA ****
	
	function iniciar_get_informacion_usuario(){
	
		id_usuario_url = parseInt(getvarinurl("profile"));

		
		if(id_usuario_url == "" || id_usuario_url == undefined || id_usuario_url == null || isNaN(id_usuario_url)){
					
			var datos = {
				"palabras" : 'true'
			}
			get_informacion_usuario(resultado_get_informacion_usuario, datos);
		}
		else{
					
			var datos = {
				"id_usuario" :id_usuario_url,
				"palabras" : 'true'
			}
			get_informacion_usuario(resultado_get_informacion_usuario,datos);
		}
		
		
	};
	
	// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
	function resultado_get_informacion_usuario(param){
		
		if(JSON.parse(param).length < 1){
			
			window.location.href = "home.php";
			return;
		}
		
		informacion_jugador = JSON.parse(param)[0];
	
		console.log(informacion_jugador);
		
		
		// *** INFORMACION BASICA
		if(informacion_jugador.id_usuario == informacion_jugador.id_consultante){
			
			mostrar_informacion_personal();
			// ** MOSTRAR INFORMACION BASICA ***
			render_datos_informacion_basica();
			// *** MOSTRAR AVATARES
			get_avatares(render_datos_avatares);
		}
		else{
			document.getElementById('tab2').innerHTML = "";
		}
		
		
		// ** MOSTRAR INFORMACION PERFIL Y ESTADISTICAS ***
		render_datos_perfil();
		
		//** MOSTRAR PALABRAS APRENDIDAS ***
		render_datos_palabras();
		
		
		// CONSEGUIR AMIGOS

		var datos = {
			"todos": 'true'
		}
		get_informacion_usuario(resultado_get_informacion_amigos,datos);

		
	};
	
	
	
	
	
	
	
	
	
	
	
	
	// *** AMIGOS ***
	
	
	
		// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
	function resultado_get_informacion_amigos(param){
		
		if(JSON.parse(param).length < 1){
			
			window.location.href = "home.php";
			return;
		}
		
		jugadores = JSON.parse(param);
	
		render_usuarios();
		
	};
	
	

	function render_usuarios(){
	
		var len = jugadores.length;
		
		var tabla = document.getElementById('contenedorcomunidad');
		
		var palabra_busqueda = document.getElementById("btn_buscar_usuario").value.trim();
		
		
		tabla.innerHTML = "";
		
		for(var i = 0; i < len; i++){
			
			console.log(jugadores[i]);
			
			// CONCIDENCIAS CON LA BUSQUEDA
			if(jugadores[i].nickname.toLocaleLowerCase().indexOf(palabra_busqueda.toLocaleLowerCase()) == -1){
				continue;
			}
			// NO MOSTRARSE ASI MISMO
			if(parseInt(jugadores[i].id_usuario) == (informacion_jugador.id_usuario)){
				continue;
			}
			if(parseInt(jugadores[i].id_usuario) == (parseInt(jugadores[i].id_consultante))){
				continue;
			}
			
			
		// VERIFICAR BUSQUEDA GENERAL O AMIGOS
			if(informacion_jugador.amigos == null){
				return;
			}
			
			if(informacion_jugador.amigos !== null){
				if(informacion_jugador.amigos.split(",").research(jugadores[i].id_usuario) == false){
					continue;
				}
			}

			
			
			var box_main = document.createElement("div");
			
			var box_avatar = document.createElement("div");
			var img_avatar = document.createElement("img");
			var icon_lock = document.createElement("i");
			
			var box_informacion = document.createElement("div");
			var box_nombre = document.createElement("a");
			
			var box_cont_percent = document.createElement("div");
			var box_percent = document.createElement("div");
			var bar_back = document.createElement("div");
			var bar_front = document.createElement("div");
			var number_percent = document.createElement("div");
			
			var box_cont_connect = document.createElement("div");
			var icon_last_connect = document.createElement("i");
			var texto_last_connect = document.createElement("span");
			
			var box_actions = document.createElement("div");
			var box_cont_dropdown = document.createElement("div");
			var btn_dropdown = document.createElement("button");
			var i_caret_btn = document.createElement("i");
			var list_dropdown = document.createElement("ul");
			
			
			var item_1 = document.createElement("li");
			var texto_item_1 = document.createElement("a");
			
			var item_2 = document.createElement("li");
			var texto_item_2 = document.createElement("a");
			
			var item_3 = document.createElement("li");
			var texto_item_3 = document.createElement("a");
			
			
			box_main.className = "perfilcommunity";
			box_avatar.className = "avatar";
			icon_lock.className = "fa fa-lock lock";
			
			box_informacion.className = "informacion";
			box_nombre.className = "nombre";
			box_nombre.setAttribute("target","_blank");
			
			box_cont_percent.className = "percent_container";
			box_percent.className = "percentgame";
			bar_back.className = "barpercentback";
			bar_front.className = "barpercentfront";
			number_percent.className = "numberpercent";
			
			box_cont_connect.className = "lastconnection";
			icon_last_connect.className = "fa fa-circle";
			
			box_actions.className = "actions";
			box_cont_dropdown.className = "dropdown moreoptions";
			btn_dropdown.className = "btn backgroundprimary colorblanco dropdown-toggle";
			btn_dropdown.setAttribute("type","button");
			btn_dropdown.setAttribute("data-toggle","dropdown");
			i_caret_btn.className = "fa fa-caret-down";
			list_dropdown.className = "dropdown-menu pull-right";
			
			
			texto_item_1.innerHTML = "Ver perfil";
			texto_item_2.innerHTML = "Agregar amigo";
			texto_item_3.innerHTML = "Bloquear";
			
			box_avatar.appendChild(img_avatar);
			box_avatar.appendChild(icon_lock);
			
			box_informacion.appendChild(box_nombre);
			
			bar_back.appendChild(bar_front);
			box_percent.appendChild(bar_back);
			box_percent.appendChild(number_percent);
			box_cont_percent.appendChild(box_percent);
			
			box_informacion.appendChild(box_cont_percent);
			
			box_cont_connect.appendChild(icon_last_connect);
			box_cont_connect.appendChild(texto_last_connect);
			
			
			texto_item_1.setAttribute("target","_blank");
			texto_item_2.setAttribute("target","_blank");
			texto_item_3.setAttribute("target","_blank");
			
			item_1.appendChild(texto_item_1);
			item_2.appendChild(texto_item_2);
			item_3.appendChild(texto_item_3);
			
			list_dropdown.appendChild(item_1);
			list_dropdown.appendChild(item_2);
			list_dropdown.appendChild(item_3);
			
			btn_dropdown.appendChild(i_caret_btn);
			
			box_cont_dropdown.appendChild(btn_dropdown);
			box_cont_dropdown.appendChild(list_dropdown);
			
			box_actions.appendChild(box_cont_dropdown);
			
			
			box_main.appendChild(box_avatar);
			box_main.appendChild(box_informacion);
			box_main.appendChild(box_cont_connect);
			box_main.appendChild(box_actions);
			
			var porcentaje = Math.round((parseInt(jugadores[i].cantidad_juegos_aprendidos) * 100) / parseInt(jugadores[i].cantidad_juegos));
			
			// ** INFORMACION BOX USUARIO **
			box_nombre.innerHTML = jugadores[i].nickname;
			box_nombre.href = "configuracion.php?profile="+jugadores[i].id_usuario;
			img_avatar.src = "../imagenes/avatares/"+jugadores[i].id_avatar+".png";
			number_percent.innerHTML = porcentaje+"%";
			bar_front.style.width = porcentaje+"%";
			texto_last_connect.innerHTML = get_mensaje_conexion(jugadores[i].ultima_conexion);
			icon_last_connect.classList.add(get_color_conexion(jugadores[i].ultima_conexion));
			
			
			texto_item_1.href = "configuracion.php?profile="+jugadores[i].id_usuario;
			texto_item_2.href = "#"; 
			texto_item_3.href = "#"; 
			
			
			
			(function(){
				
				var jugador = jugadores[i];
				
				
				// ** BOTON AGREGAR AMIGO **
				check_btn_agregar_amigo.call(texto_item_2,jugador);
				
				// ** BOTON BLOQUEAR AMIGO **
				check_btn_bloquear_amigo.call(texto_item_3,jugador);
				
				// VERIFICAR SI SON AMIGOS
				check_amigo.call(btn_dropdown,jugador);
				
				// VERIFICAR SI ESTA BLOQUEADO
				check_bloqueado.call(icon_lock,jugador);
				
				
			})();
			
			
			tabla.appendChild(box_main);
		}
		// fin loop

		
		$('[data-toggle="tooltip"]').tooltip();
		
		
	};
	
	
	
	

	
	
	
	// ACCIONES BOTONES COMUNIDAD ***
	function check_btn_agregar_amigo(info_amigo){
				
				if(informacion_consultante.amigos !== null && informacion_consultante.amigos.split(",").research(info_amigo.id_usuario) == true){
				
						this.addEventListener('click',function(e){ e.preventDefault(); iniciar_eliminar_amigo.call(this, info_amigo);});
						this.innerHTML = "Eliminar amigo";
				}
				else{
						this.addEventListener('click',function(e){ e.preventDefault(); iniciar_agregar_amigo.call(this, info_amigo);});
						this.innerHTML = "Agregar amigo";
				}
	};
	
	function iniciar_agregar_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		agregar_amigo(resultado_agregar_amigo, datos);
		
	};
	
	function resultado_agregar_amigo(respuesta){

		iniciar_get_informacion_consultante();
			
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Agregado a tu lista de amigos");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido agregarlo a tu lista de amigos");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario ya es tu amigo");
					break;
				}
				default:{
					break;
				}
			}
		}
	};
	
	
	function iniciar_eliminar_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		eliminar_amigo(resultado_eliminar_amigo, datos);
		
	};
	
	function resultado_eliminar_amigo(respuesta){
	
		iniciar_get_informacion_consultante();
		
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Eliminado de tu lista de amigos");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido eliminarlo de tu lista de amigos");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario No es tu amigo");
					break;
				}
				default:{
					break;
				}
			}
		}
	
	};
	
	
	// *** ACCIONES BOTON BLOQUEAR AMIGO **
	
	function check_btn_bloquear_amigo(info_amigo){
		
				if(informacion_consultante.bloqueados !== null && informacion_consultante.bloqueados.split(",").research(info_amigo.id_usuario) == true){
				
						// ** DESBLOQUEAR **
						this.addEventListener('click',function(e){ 
							e.preventDefault(); 
							iniciar_desbloquear_amigo.call(this, info_amigo);
						});
						this.innerHTML = "Desbloquear";
				}
				else{
						//** BLOQUEAR **
						this.addEventListener('click',function(e){ 
							e.preventDefault(); 
							iniciar_bloquear_amigo.call(this, info_amigo);
						});
						this.innerHTML = "Bloquear";
				}
	};
	
	
		function iniciar_bloquear_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		bloquear_amigo(resultado_bloquear_amigo, datos);
		
	};
	
	function resultado_bloquear_amigo(respuesta){

		//console.log(respuesta);
		iniciar_get_informacion_consultante();
			
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Este usuario ha sido bloqueado");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido bloquearlo");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario ya esta bloqueado");
					break;
				}
				default:{
					break;
				}
			}
		}
	};
	
	
	function iniciar_desbloquear_amigo(info_amigo){
		
		var datos = {
			"id_amigo": info_amigo.id_usuario
		};
		desbloquear_amigo(resultado_desbloquear_amigo, datos);
		
	};
	
	function resultado_desbloquear_amigo(respuesta){
	
		iniciar_get_informacion_consultante();
		
		if(!isNaN(parseInt(respuesta))){
			
			respuesta = parseInt(respuesta);
		
			switch(respuesta){
				case 1:{
					openAlerta("alert-success","Has desbloqueado este usuario");
					break;
				}
				case 2:{
					openAlerta("alert-danger","No has podido desbloquear este usuario");
					break;
				}
				case 3:{
					openAlerta("alert-info","Este usuario ya esta desbloqueado");
					break;
				}
				default:{
					break;
				}
			}
		}
	
	};
	

	
		
	// VERIFICAR SI SON AMIGOS
	function check_amigo(info_usuario){
		
		if(informacion_consultante.amigos !== null && informacion_consultante.amigos.split(",").research(info_usuario.id_usuario)){
			this.classList.add("backgroundspecial");
		}
	}
	
	// VERIFICAR SI ESTA BLOQUEADO
	function check_bloqueado(info_usuario){
		
		if(informacion_consultante.bloqueados !== null && informacion_consultante.bloqueados.split(",").research(info_usuario.id_usuario)){
			this.style.display = "inline-block";
		}
	}
	
	
	
	
	
	// FUNCIONES PARA SABER TIEMPO DE CONEXION Y ESTADO ACTUAL
	
	function get_mensaje_conexion(segundos){
			
		if(segundos == null){
			return "";
		}
		
		segundos = Math.round(segundos / 60);
		
		var mensaje = "";
		
		if(segundos < 1){
			mensaje = " Hace un momento";
		}
		else if(segundos == 1){
			mensaje = " Hace 1 minuto";
		}
		else if(segundos > 0 && segundos < 10){
			mensaje = " Hace "+ segundos + " minutos";
		}
		else{
			mensaje = " Desconectado";
		}
		return mensaje;
	};
	
	function get_color_conexion(segundos){

		if(segundos == null){
			return "";
		}

		segundos = Math.round(segundos / 60);
		
		if(segundos < 10){
			return "colorsuccess";
		}
		else{
			return "colorcancel";
		}
	};
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// *** MOSTRAR INFORMACION BASICA ***
	function render_datos_informacion_basica(){
	
		document.getElementById('txt_email').value = informacion_jugador.email;
		document.getElementById('txt_usuario').value = informacion_jugador.usuario;
		document.getElementById('txt_nickname').value = informacion_jugador.nickname;
		document.getElementById('txt_password').value = informacion_jugador.password;
		document.getElementById('txt_nombre').value = informacion_jugador.nombre;
		document.getElementById('txt_apellido').value = informacion_jugador.apellido;
		document.getElementById('txt_pais').value = informacion_jugador.pais;
		document.getElementById('txt_ciudad').value = informacion_jugador.ciudad;
		document.getElementById('txt_telefono').value = informacion_jugador.telefono;
		document.getElementById('txt_fecha_nacimiento').value = informacion_jugador.fecha_nacimiento;
		
	};
	
	
	// *** MOSTRAR INFORMACION PERFIL ***
	
	function render_datos_perfil(){
		
		var porcentaje = Math.round((parseInt(informacion_jugador.cantidad_juegos_aprendidos) * 100) / parseInt(informacion_jugador.cantidad_juegos));
		console.log(porcentaje);
		
		document.getElementById('txt_perfil_aprendidas').innerHTML = informacion_jugador.cantidad_juegos_aprendidos;
		document.getElementById('txt_perfil_nickname').innerHTML = informacion_jugador.nickname;
		
		document.getElementById('imagen_perfil').setAttribute("src","../imagenes/avatares/"+informacion_jugador.id_avatar+".png");
		
		//document.getElementById('imagen_top_profile').setAttribute("src","../imagenes/avatares/"+informacion_jugador.id_avatar+".png");
		
		document.getElementById('bar_perfil_porcentaje').style.width = porcentaje+"%";
		document.getElementById('txt_perfil_porcentaje').innerHTML = porcentaje + "%";
		
		
		document.getElementById('txt_juegos_aprendidos').innerHTML = informacion_jugador.cantidad_juegos_aprendidos + " / " + informacion_jugador.cantidad_juegos;
		document.getElementById('txt_juegos_completados').innerHTML = informacion_jugador.cantidad_juegos_completados;
		document.getElementById('txt_palabras_aprendidas').innerHTML = informacion_jugador.cantidad_palabras;
		document.getElementById('txt_velocidad_promedio').innerHTML = informacion_jugador.promedio;
		document.getElementById('txt_mejor_racha').innerHTML = informacion_jugador.racha;
		document.getElementById('txt_amigos').innerHTML = informacion_jugador.cantidad_amigos;
		
	}
	
	
	// *** MOSTRAR INFORMACION AVATARES ***
	
	function render_datos_avatares(respuesta_avatares){
		
		var avatares = JSON.parse(respuesta_avatares);
		
		var len = avatares.length;
		
		var contenedor = document.getElementById('tab3');
		
		var juegos_aprendidos = parseInt(informacion_jugador.cantidad_juegos_aprendidos);
		
		contenedor.innerHTML = "";
		
		for(var i = 0; i < len; i++){
			
			var div_main = document.createElement("div");
			var imagen = document.createElement("img");
			var boton = document.createElement("div");
			
			div_main.className = "box_avatar";
			
			imagen.setAttribute("src",ruta_avatares+ avatares[i].ruta+".png");
			imagen.setAttribute("height","100px");
			imagen.setAttribute("width","100px");
			
			boton.className = "btn btn-block btn-default btn-sm";
			
			boton.innerHTML = "usar";
			
			div_main.setAttribute("title",avatares[i].nombre);
			div_main.setAttribute("data-toggle","popover");
			div_main.setAttribute("data-placement","top");
			div_main.setAttribute("data-trigger","hover");
			div_main.setAttribute("data-html","true");
			
			
			div_main.appendChild(imagen);
			div_main.appendChild(boton);
			
			contenedor.appendChild(div_main);
			
			(function(){
				
				var avatar_actual = avatares[i];
				
				// ** VERIFICA REQUISITO ***
				if(parseInt(avatar_actual.requisito) <= juegos_aprendidos){
					
					div_main.setAttribute("data-content","Desbloqueado");
				

					
					if(parseInt(avatar_actual.id_avatar) == parseInt(informacion_jugador.id_avatar)){
						boton.innerHTML = "En uso";
						boton.className = "btn btn-block btn-info btn-sm disabled";
					}
					else{
												// ** FUNCION CAMBIAR AVATAR ***
						boton.addEventListener('click',function(){
							set_avatar(resultado_set_avatar,avatar_actual.ruta);
						});
					}
					
				}
				else{
					
					boton.classList.add("disabled");
					div_main.classList.add("box_avatar_disabled");
					div_main.setAttribute("data-content", (parseInt(avatar_actual.requisito) - juegos_aprendidos) + " Juegos ("+ juegos_aprendidos+ " / "+ avatar_actual.requisito+")");
				}
				
				
			})();
			
		}
		
		$('[data-toggle="popover"]').popover();
		
	};
	
	
	function resultado_set_avatar(respuesta){
		
		respuesta = parseInt(respuesta);
		
		switch(respuesta){
				
			case 1:{
				
				iniciar_get_informacion_consultante();
				document.getElementById('imagen_top_profile').setAttribute("src","../imagenes/avatares/"+informacion_jugador.id_avatar+".png");
				openAlerta("alert-info","Cambio de avatar realizado");
				
				break;
			}
			case 2:{
				
				openAlerta("alert-danger","Ha ocurrido un error");
			}
			default:{
				openAlerta("alert-danger","Ha ocurrido un error");
			}
				
		}
		
	};
	
	
	
	function render_datos_palabras(){
		
		if(informacion_jugador.palabras == null || informacion_jugador.palabras == undefined){
			return;
		}
		
		
		var current_scroll = document.body.scrollTop;
		
		var len = informacion_jugador.palabras.length;
		
		var tabla = document.getElementById('tabla_palabras');
		
		if(data_table){
			var current_pagelength =  data_table.page.info().length;
			data_table.clear();
			data_table.destroy();
		}
	
		
		
		document.getElementById('txt_cant_palabras_aprendidas').innerHTML = len;
		
		for(var i = 0; i < len; i++){
			
			var tr_main = document.createElement("tr");
			var td_palabra = document.createElement("td");
			var td_practicas = document.createElement("td");
			var td_last = document.createElement("td");
			
			td_palabra.innerHTML = informacion_jugador.palabras[i].palabra;
			td_practicas.innerHTML = informacion_jugador.palabras[i].practica;
			td_last.innerHTML = informacion_jugador.palabras[i].diferencia;
			
			tr_main.appendChild(td_palabra);
			tr_main.appendChild(td_practicas);
			tr_main.appendChild(td_last);
			
			tabla.appendChild(tr_main);
		}

		data_table = $('#contenedor_tabla_palabras').DataTable({

			"language" : lenguaje_datatable,
			 "pageLength": current_pagelength,
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todo"]]

		});

		data_table.page(current_page).draw(false);

		document.body.scrollTop = current_scroll;
		
	};
	
	
	
	
	function mostrar_informacion_personal(){
		
			document.getElementById('tab_informacion_basica').setAttribute("style","display: block !important");
			
			document.getElementById('tab_avatar').setAttribute("style","display: block !important");
			
	};
	
	
	
	// ** ENVIAR INFORMACION BASICA ***
	function enviar_datos(){

		if(!checkRegister()){
			return false;
		}
	
	var datos_basicos = {
		
		email : document.getElementById('txt_email').value,
		usuario : document.getElementById('txt_usuario').value,
		nickname : document.getElementById('txt_nickname').value,
		password : document.getElementById('txt_password').value,
		nombre : document.getElementById('txt_nombre').value,
		apellido : document.getElementById('txt_apellido').value,
		pais : document.getElementById('txt_pais').value,
		ciudad : document.getElementById('txt_ciudad').value,
		telefono : document.getElementById('txt_telefono').value,
		fecha_nacimiento : document.getElementById('txt_fecha_nacimiento').value

	}
	
	set_informacion_basica(resultado_set_informacion_basica,datos_basicos);
		
	};
    
    
	//** RESULTADO MODIFICACION DATOS BASICOS ***
	
    function resultado_set_informacion_basica(respuesta){
		
		errorHandler(respuesta);
		
		console.log(respuesta);
	};
	
	
	
    
	// *** VERIRFICAR INTEGRIDAD DE DATOS ***
	
    function keyCheckRegister(e){
      
		checkRegister();
		
        if(e.keyCode == 13){
            checkRegister();
        }
    };
    
    
    function checkRegister(){
	
		var email = document.getElementById('txt_email').value;
		var usuario = document.getElementById('txt_usuario').value;
		var nickname = document.getElementById('txt_nickname').value;
		var password = document.getElementById('txt_password').value;
        
        
        var arraydatos = [email,usuario,nickname,password];
        
        if(!checkEmptyFields(arraydatos)){
            
            errorHandler("2");
            return false;
        }
        

        closeError();
		return true;
        
    };
    
    
    
    
	
	//** EVENT HANDLER ERRORES ***
    
    function errorHandler(error,callback){
      
        var error = parseInt(error);
        
        switch(error){
            
            
            case 0:{
                
                showError("Ha ocurrido un error");
                return;
            }
            case 1:{
                
				openAlerta("alert-success","Cambios realizados con exito");
				iniciar_get_informacion_consultante();
                return;        
            }
            case  2: {
                
                showError("Debes de ingresar todos los campos marcados con un asterisco <strong>(*)</strong>");
                return;
            }
            case 3: {
                
                showError("Datos incorrectos");
                return;
            }
                
            case 4: {
                
                showError("Solo numeros, Letras y guiones permitidos");
                return;
            }
            case 5:{
                
                showError("Debes ingresar un Usuario valido");
                return;
            }
            case 6:{
                
                showError("Debes ingresar un Email valido");
                return;
            }
            case 7:{
                
                showError("El email <strong>"+ document.getElementById('txt_email').value+ "</strong> ya se encuentra registrado");
                return;
            }
            case 8:{
                
                showError("El usuario <strong>"+ document.getElementById('txt_usuario').value + "</strong> ya se encuentra registrado");
                return;
            }
            case 10:{
                
                showError("El usuario debe de contener menos de 20 caracteres");
                return;
            }
            case 11:{
                
                showError("El nickName debe de contener menos de 20 caracteres");
                return;
            }
            case 12:{
                
                showError("El nickName "+document.getElementById('txt_nickname').value+ " Ya se encuentra registrado");
                return;
            }
            case 20:{
                
                showError("El nickName debe de contener menos de 20 caracteres");
                return;
            }
			default:{
			
                showError("Ha ocurrido un error");
				return;
			}
        }
        
    };
	
	
	
	//** MOSTRAR ERRORES ***
    
    function showError(descripcion){
      
        panelError.style.display = "block";
        panelError.innerHTML = descripcion;
        
    };
    
    function closeError(){
        
        panelError.style.display = "none";
        panelError.innerHTML = "";
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
};