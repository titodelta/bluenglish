
window.addEventListener('load',inicio_manager);



function inicio_manager(){
	
	
var juegos; 
	
var id_juego;

var cambiando = false;

var categoria = 0;
	
var elemento_from = undefined;
	
var elemento_to = undefined;
	
var tipo_cambio = 0; // 0 up, 1 down
	
var tabla_juegos = document.getElementById('tabla_juegos');
	
var contenedor_tabla_juegos = document.getElementById('contenedor_tabla_juegos');
	
var btn_categoria1 = document.getElementById('btn_categoria1');
var btn_categoria2 = document.getElementById('btn_categoria2');
var btn_categoria3 = document.getElementById('btn_categoria3');
	
var data_table;
	
var current_page = 0;
	
var informacion_envio = {
	"tipo" : 0,
	"idioma" : 0,
	"quiz" : "false"
};
	
btn_categoria1.addEventListener('click',function(){ cambiar_categoria(0) });
	
btn_categoria2.addEventListener('click',function(){ cambiar_categoria(1) });
	
btn_categoria3.addEventListener('click',function(){ cambiar_categoria(2) });
	
document.getElementById('select_idioma').addEventListener('change',select_idioma);
	
document.getElementById('btn_cancelar_change').addEventListener('click',modo_no_changer);
	
document.getElementById('btneliminarjuego').addEventListener('click',iniciar_eliminar_juego); // eliminar juego
	
document.getElementById('btncrearjuego').addEventListener('click',iniciar_crear_nuevo_juego);

	
	
limpiar_informacion();
	
iniciar_conseguir_informacion();
	
	
	
var tiempo_render_juegos = setInterval(iniciar_conseguir_informacion,15000);

	
	
	
	
	
function cambiar_categoria(param_categoria){

	
	btn_categoria1.classList.remove("active");
	btn_categoria2.classList.remove("active");
	btn_categoria3.classList.remove("active");
	
	
	switch(param_categoria){
			
		case 0:{
		
			informacion_envio.tipo = 0; // vocabulario
			informacion_envio.quiz = "false";
			btn_categoria1.classList.add("active");
			break;
		}
		case 1:{
			
			informacion_envio.tipo = 1; // inmersion
			informacion_envio.quiz = "false";
			btn_categoria2.classList.add("active");
			break;
		}
		case 2:{
			
			informacion_envio.quiz = "true"; // quiz
			btn_categoria3.classList.add("active");
			break;
		}
			
	} // fin switch
	
	
	iniciar_conseguir_informacion();
	
};
	
	
function select_idioma(e){
	
	informacion_envio.idioma = e.target.selectedIndex;
	
	iniciar_conseguir_informacion();
};
	

	

	
function iniciar_conseguir_informacion(){
	
	if(data_table){
		current_page = data_table.page();
	}
	
	get_lista_games(resultado_conseguir_informacion,informacion_envio);
	
};

	
function resultado_conseguir_informacion(respuesta){
	
	
	juegos = JSON.parse(respuesta);
	
	juegos.sort(ordenar_por_posicion);
	
	renderizar_juegos();
	
	document.getElementById('resultados_encontrados').innerHTML =  juegos.length+" resultados encontrados";
	
	console.log(juegos);
	
	
}; // fin carga de juegos
	
	
function renderizar_juegos(){

	var current_scroll = document.body.scrollTop;
	
	limpiar_informacion();
	
	var len = juegos.length;
	
	if(data_table){
		var current_pagelength =  data_table.page.info().length;
		data_table.clear();
		data_table.destroy();
	}
	
	
	for(var i = 0; i < len; i++){
	
		
		var tr_main = document.createElement("tr");
		var td_id = document.createElement("td");
		var td_nombre = document.createElement("td");
		var td_idioma = document.createElement("td");
		var td_terminado = document.createElement("td");
		var td_bugs = document.createElement("td");
		var td_acciones = document.createElement("td");
		
		
		var i_editar = document.createElement("i");
		var i_play = document.createElement("i");
		var i_eliminar = document.createElement("i");
		var i_subir = document.createElement("i");
		var i_bajar = document.createElement("i");
		var i_informacion = document.createElement("i");
		
		i_editar.className = "iconbutton fa fa-gavel";
		i_play.className = "iconbutton fa fa-gamepad";
		i_eliminar.className = "iconbutton fa fa-trash";
		i_subir.className = "iconbutton fa fa-level-up";
		i_bajar.className = "iconbutton fa fa-level-down";
		i_informacion.className = "iconbutton fa fa-book";
		
		i_editar.setAttribute("title","Editar");
		i_editar.setAttribute("data-toggle","tooltip");
		i_editar.setAttribute("data-placement","top");
		
		i_play.setAttribute("title","Jugar");
		i_play.setAttribute("data-toggle","tooltip");
		i_play.setAttribute("data-placement","top");
		
		i_eliminar.setAttribute("title","Eliminar");
		i_eliminar.setAttribute("data-toggle","tooltip");
		i_eliminar.setAttribute("data-placement","top");
		
		i_subir.setAttribute("title","Subir");
		i_subir.setAttribute("data-toggle","tooltip");
		i_subir.setAttribute("data-placement","top");
		
		i_bajar.setAttribute("title","Bajar");
		i_bajar.setAttribute("data-toggle","tooltip");
		i_bajar.setAttribute("data-placement","top");
		
		i_informacion.setAttribute("title","Informacion");
		i_informacion.setAttribute("data-toggle","tooltip");
		i_informacion.setAttribute("data-placement","top");
		
		td_id.innerHTML = i+1;
		td_nombre.innerHTML = juegos[i].nombre;
		td_idioma.innerHTML = idiomas[juegos[i].idioma];
		td_terminado.innerHTML = juegos[i].terminado;
		td_bugs.innerHTML = 0;
		
		td_acciones.appendChild(i_editar);
		td_acciones.appendChild(i_play);
		td_acciones.appendChild(i_eliminar);
		td_acciones.appendChild(i_subir);
		td_acciones.appendChild(i_bajar);
		td_acciones.appendChild(i_informacion);
		
		tr_main.appendChild(td_id);
		tr_main.appendChild(td_nombre);
		tr_main.appendChild(td_idioma);
		tr_main.appendChild(td_terminado);
		tr_main.appendChild(td_bugs);
		tr_main.appendChild(td_acciones);
		
		tabla_juegos.appendChild(tr_main);
		
		(function(){
			
			var juego = juegos[i];
			
			var tr_actual = tr_main;
			
			// funciones click juego general
			tr_main.addEventListener('click',function(){
				
				if(cambiando){
					
					elemento_to = parseInt(juego.id_juego);
					
					console.log("from = "+elemento_from);
					console.log("to = "+elemento_to);
					
					realizar_cambio();
				}
				
			});
			
			
			// funciones boton build game
			i_editar.addEventListener('click',function(){

				window.open("creategame.php?game="+juego.id_juego,"_blank");
			});
			

			i_play.addEventListener('click',function(){
				
				setNuevoToken(juego.id_juego);
			});
			
			// funciones intercambio down
			i_subir.addEventListener('click',function(e){
				
				e.stopPropagation();
				
				elemento_from = parseInt(juego.id_juego);
				
				modo_changer(0);
			});
			
			// funciones intercambio down
			i_bajar.addEventListener('click',function(e){
				
				e.stopPropagation();
				
				elemento_from = parseInt(juego.id_juego);
				
				modo_changer(1);
			});
			
					
			i_eliminar.addEventListener('click',function(){
				
				id_juego = juego.id_juego;
				
				$("#modal_eliminar_juego").modal("show");
				
			});
			
			
		})(); // fin auto funcion
		
		
		
	} // fin for render de juegos
	
	
	//**** TOOLTIP ***
	$('[data-toggle="tooltip"]').tooltip();  
	
	data_table = $('#contenedor_tabla_juegos').DataTable({
		
		"language" : lenguaje_datatable,
		 "pageLength": current_pagelength,
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todo"]]
		
	});
	
	data_table.page(current_page).draw(false);
	
	document.body.scrollTop = current_scroll;
	
};
	
	
	
function ordenar_por_posicion(a,b){

		a = parseInt(a.posicion);	
		b = parseInt(b.posicion);
	
		if(a < b){
			return -1;
		}
		else if(a > b){
			return 1;
		}
		else{
			return 0;
		}
		
};

	
function realizar_cambio(){

	var posicion_from = undefined;
	var posicion_to = undefined;
	
	var nodo;
	
	var array_posicion = [];
	
	for(var i = 0; i < juegos.length; i++){
		
		if(elemento_from == juegos[i].id_juego){
			posicion_from = i;
			nodo = juegos.splice(i,1);
			break;
		}
	}
	
	for(var i = 0; i < juegos.length; i++){
		
		if(elemento_to == juegos[i].id_juego){
			posicion_to = i;
			break;
		}
		
	}
	
	
	juegos.splice(posicion_to + tipo_cambio,0,nodo[0]);
	
	
	
	for(var i = 0; i < juegos.length; i++){
		
		array_posicion.push(juegos[i].id_juego);
	}
	
	var datos_posicion = array_posicion.join(",");
	
	set_posicion_juego(iniciar_conseguir_informacion,datos_posicion);
	
	modo_no_changer();
	
};
	
	
function limpiar_informacion(){

	
	// Limpiar tabla de juegos
	tabla_juegos.innerHTML = "";
	
	
};

function modo_no_changer(){
	
	cambiando = false;
	
	elemento_from = undefined;
	
	elemento_to = undefined;
	
	tipo_cambio = 0;
	
	contenedor_tabla_juegos.classList.remove("table_changer");
	contenedor_tabla_juegos.classList.remove("table_changer_down");
	contenedor_tabla_juegos.classList.remove("table_changer_up");
	
	document.getElementById('btn_cancelar_change').style.display = "none";
}
	
function modo_changer(direction){
	
	cambiando = true;
	
	tipo_cambio = direction;
	
	contenedor_tabla_juegos.classList.remove("table_changer_down");
	contenedor_tabla_juegos.classList.remove("table_changer_up");
	
	contenedor_tabla_juegos.classList.add("table_changer");
	
	if(direction == 0){
		
		contenedor_tabla_juegos.classList.add("table_changer_up");
	}
	else{
		
		contenedor_tabla_juegos.classList.add("table_changer_down");
	}
	
	document.getElementById('btn_cancelar_change').style.display = "block";
}
	
	
	
// acciones para crear un nuevo juego
	

function iniciar_crear_nuevo_juego(){
	
	crear_nuevo_juego(respuesta_crear_nuevo_juego);
};
	
function respuesta_crear_nuevo_juego(reply_nuevo_juego){
	
	if(!isNaN(parseInt(reply_nuevo_juego))){
		
		window.open("creategame.php?game="+reply_nuevo_juego,"_blank");
	}
	else{
		console.log(reply_nuevo_juego);
		openAlerta("alert-warning","Ha ocurrido un error");
	}
	
};
	
	
	
	
	
function iniciar_eliminar_juego(){
	
	eliminar_juego(confirmacion_eliminar_juego,id_juego);
};

function confirmacion_eliminar_juego(param){
	
	console.log(param);
	
	$("#modal_eliminar_juego").modal("hide");
	
	iniciar_conseguir_informacion();
	
};
	
	
	

	
function setNuevoToken(param){   // ingreso de token
    
    set_token_game(getActualToken,param);
    
}
    
function getActualToken(param){  // conseguir el id del token la base de datos
    console.log(param);
	window.open("play.php?game="+param,"_blank");
};
    
	
	
	
	
	
};