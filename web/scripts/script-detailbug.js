

window.addEventListener('load', init_detailbug);

                        
function init_detailbug() {

	

var id_bug; 

var bug_a_cambiar_estado;

var bug_a_eliminar;
	
		
	
document.getElementById('btn_cambiar_estado').addEventListener('click',iniciar_set_estado_bug);

document.getElementById('btneliminarbug').addEventListener('click',iniciar_eliminar_bug);
	
document.getElementById('btn_cerrar_detalle').addEventListener('click',cerrar_ventana);
	

	
get_informacion_bug();

	
function cerrar_ventana(){

	self.close();
	
};
	
	
	
function get_informacion_bug(){
	
	id_bug = getvarinurl("id_bug");
	
	iniciar_get_bug();
	
};
	
function iniciar_get_bug(){
	
	var informacion_bug = {
		"id_bug" : id_bug,
	};
	
	get_bugs(resultado_get_bugs,informacion_bug);
	
};
	
	
function resultado_get_bugs(param){
	
	var resultado_bug = JSON.parse(param);
	
	render_informacion_bug(resultado_bug[0]);
	
	console.log(resultado_bug);
	
};
	
	
function render_informacion_bug(info_bug){
	
	document.getElementById('campo_id').innerHTML = info_bug.id_bug;
	document.getElementById('campo_fecha').innerHTML = info_bug.fecha_creacion.split(" ")[0];
	document.getElementById('campo_hora').innerHTML = info_bug.fecha_creacion.split(" ")[1];
	document.getElementById('campo_modificacion').innerHTML = info_bug.fecha_modificacion;
	document.getElementById('campo_usuario').innerHTML = info_bug.nombre_usuario_creador;
	document.getElementById('campo_nombre_juego').innerHTML = info_bug.nombre_juego;
	document.getElementById('campo_id_pregunta').innerHTML = info_bug.pregunta;
	document.getElementById('campo_estado').innerHTML = get_estado_por_id(info_bug.estado);
	
	document.getElementById('campo_descripcion').innerHTML = info_bug.descripcion;
	
	document.getElementById('titulo_id_bug').innerHTML = info_bug.id_bug;
	
	

	icon_editar.addEventListener('click',function(){

		window.open("creategame.php?game="+info_bug.id_juego+"&question="+info_bug.id_pregunta);
	});


	icon_eliminar.addEventListener('click',function(){

		bug_a_eliminar = info_bug.id_bug;
		$("#modal_eliminar_bug").modal("show");

	});


	icon_cambiar_estado.addEventListener('click',function(){

		bug_a_cambiar_estado = info_bug.id_bug;

		$("#modal_cambiar_estado").modal("show");

		document.getElementById('seleccion_estado').selectedIndex = info_bug.estado;

	});
			
	
};
	
	
	
function get_estado_por_id(id_estado){
	
	id_estado = parseInt(id_estado);
	
	switch(id_estado){
			
		case 0:{
			
			return "sin solucion";
			break;
		}
		case 1:{
			
			return "pendiente";
			break;
		}
		case 2:{
			
			return "solucionado";
			break;
		}
	}
	
};
	
	
	
	
	
	
	
function iniciar_eliminar_bug(){

	eliminar_bug(resultado_eliminar_bug, bug_a_eliminar);
	
};

function resultado_eliminar_bug(param){
	
	iniciar_get_bug();
	$("#modal_eliminar_bug").modal("hide");
	
	param = parseInt(param);
	
	switch(param){
			
		case 1:{
			
			self.close();
			break;
		}
		case 2:{
			
			openAlerta("alert-danger","No se ha podido eliminar el reporte");
			break;
		}
			
	}

};
	

	

function iniciar_set_estado_bug(){
	
	var estado = document.getElementById('seleccion_estado').selectedIndex;
	
	var informacion_bug = {
	
		"id_bug" : bug_a_cambiar_estado,
		"estado" : estado
	};
	
	set_estado_bug(resultado_set_estado_bug, informacion_bug);
	
};
	
	
function resultado_set_estado_bug(param){
	
	console.log(param);
	
	$("#modal_cambiar_estado").modal("hide");
	
	iniciar_get_bug();
};
	
	

	
};