

window.addEventListener('load',init_create);
                        
function init_create(){   // empieza cuando se carga la pagina

var id_juego = undefined;  // id de juego
var token_pregunta = undefined; // token de pregunta actual
var informacion_juego; // info juego
var nueva_pregunta = 0; // 0 nueva, 1 existente
var actual_id_pregunta = undefined;
var lista_eliminadas = [];  // 
var imagen_upload = null;   // id de imagen
var audio_upload = null;    // id de audio 
var archivo_subido = null;  // archivo recien subido en panel media
var contenido_audio = true; // panel de audio / imagenes
var cambios = false;
var guardado_por_imagen = false;

	
var media_img_data = [];
var media_img_currentpag = 1;
var media_img_cantpages = 1;
var media_img_maxpag = 30;
	
var audio = new Audio();

getInfoJuego();
set_modo_creacion();

var automatic_question = setInterval(iniciar_cargar_preguntas,10000);



	
document.getElementById('nombre_juego').addEventListener('change',realizar_cambios);
document.getElementById('tipo_juego').addEventListener('change',realizar_cambios);
document.getElementById('tipo_quiz').addEventListener('change',realizar_cambios);
document.getElementById('tipo_idioma').addEventListener('change',realizar_cambios);
document.getElementById('tipo_terminado').addEventListener('change',realizar_cambios);
	

document.getElementById('btnnuevarespuesta').addEventListener('click',crear_respuesta); // crear un nuevo campo de respuesta
	
document.getElementById('btncontrolpregunta').addEventListener('click',ingresar_pregunta); // ingreso de pregunta a la DB

document.getElementById('btncontrolpregunta').addEventListener('keydown',key_ingresar_pregunta); // ingreso de pregunta a la DB

document.getElementById('textopregunta').addEventListener('keydown',checkContinueInput); // ingreso de pregunta a la DB

document.getElementById('btn_nueva_pregunta').addEventListener('click',set_modo_creacion); // Modo creacion. Nueva pregunta

document.getElementById('btncancelarpregunta').addEventListener('click',set_modo_creacion); // Modo creacion. Nueva pregunta

document.getElementById('btneliminarpregunta').addEventListener('click',eliminarPregunta); // eliminar pregunta

document.getElementById('btneliminarjuego').addEventListener('click',iniciar_eliminar_juego); // eliminar juego

document.getElementById('btnguardarinfojuego').addEventListener('click',iniciar_guardar_informacion_juego); // guardar cabezera de juego
document.getElementById('btnsalirjuego').addEventListener('click',verificar_cambios);	

document.getElementById('btn_guardar_cambios').addEventListener('click',guardar_cambios);	

document.getElementById('btn_jugar').addEventListener('click',setNuevoToken);
	

document.getElementById('formularioimagen').addEventListener('submit',iniciar_subir_imagen); // formulario submit imagen

document.getElementById('btnnuevaimagen').addEventListener('click',nueva_imagen); // nueva imagen

document.getElementById('archivo_imagen').addEventListener('change',nombre_uploadimg); // archivo tipo file, cambio de nombre

document.getElementById('nombre_imagen').addEventListener('keyup',check_datos_uploader); // verificar datos antes de subir

document.getElementById('btnsalir_imagen').addEventListener('click',close_uploading_imagen); // cerrar panel uploading imagen
	
document.getElementById('btnclose_imagen').addEventListener('click',close_uploading_imagen); //  cerrar panel uploading imagen

document.getElementById('btnbuscarimagenes').addEventListener('click',iniciar_get_imagenes); // buscar imagenes

document.getElementById('txtbusquedaimagen').addEventListener('keyup',key_iniciar_get_imagenes); // busqueda de imagen por evento de key

document.getElementById('btnclosemediaimagen').addEventListener('click',close_media_imagen);
	
document.getElementById('btn_uploader_img').addEventListener('click',open_uploading_imagen); // cerrar 
	
document.getElementById('containerimagen').addEventListener('error',contingencia_imagen);
	
// acciones para botones de paginacion media imagen
	
document.getElementById('media-img-inicio').addEventListener('click',media_img_inicio);
document.getElementById('media-img-anterior').addEventListener('click',media_img_anterior);
document.getElementById('media-img-siguiente').addEventListener('click',media_img_siguiente);
document.getElementById('media-img-fin').addEventListener('click',media_img_fin);
	

// acciones para boton de buscar imagen
document.getElementById('btnbuscarimagen').addEventListener('click',iniciar_control_imagen);
document.getElementById('btnbuscaraudio').addEventListener('click',iniciar_control_audio);

// archivo recien subido, acciones rapidas
document.getElementById('contenedor_upload_imagen').addEventListener('click',click_media);
document.getElementById('contenedor_upload_imagen').addEventListener('dblclick',dblclick_media);

document.getElementById('btn_traduccion').addEventListener('click',buscar_traduccion);	
	

document.getElementById('btn_set_questions').addEventListener('click',inyeccion);
	
	
get_paginacion_imagenes();
	

	
	
function click_media(){
	
	if(contenido_audio){
		audio.currentTime = 0;
		audio.play();
	}
	else{
		
	}
};
	
function dblclick_media(){

	if(contenido_audio){
		
		audio_upload = archivo_subido;

		close_media_imagen();

		encontrando_audio();
		
		close_uploading_imagen();
		
		if(nueva_pregunta == 1){
			
			guardado_por_imagen = true;
			ingresar_pregunta();
		}
	}
	else{

		imagen_upload = archivo_subido;

		close_media_imagen();

		encontrando_imagen();
		
		close_uploading_imagen();
		
		document.getElementById('containerimagen').src = ruta_imagenes + archivo_subido+".jpg";
		
		if(nueva_pregunta == 1){
			
			guardado_por_imagen = true;
			ingresar_pregunta();
		}
	}
};
	
	
	


	

function contingencia_imagen(){

	console.log("Se ejecuto contingencia");
	document.getElementById('containerimagen').src = imagen_default;
	
};
	
	
	
	
function iniciar_control_imagen(){
	
	if(imagen_upload != null){
	
		buscando_imagen();
		return;
	}
	else{
		
		contenido_audio = false;
		open_media_imagen();
	}
		
};
	
	
function iniciar_control_audio(){
	
	if(audio_upload != null){
	
		buscando_audio();
		return;
	}
	else{
		
		contenido_audio = true;
		open_media_imagen();
	}
		
};
	
	
function buscando_imagen(){
	
	document.getElementById('btnbuscarimagen').classList.remove("backgroundfail");
	document.getElementById('btnbuscarimagen').classList.add("backgroundspecial");
	
	document.getElementById('btnbuscarimagen').innerHTML = "Buscar";
	
	document.getElementById('containerimagen').src = imagen_default;
	
	imagen_upload = null;
};
	
function encontrando_imagen(){
	
	document.getElementById('btnbuscarimagen').classList.remove("backgroundspecial");
	document.getElementById('btnbuscarimagen').classList.add("backgroundfail");
	
	document.getElementById('btnbuscarimagen').innerHTML = "Eliminar";
	
};

	
	
	
	
function buscando_audio(){
	
	document.getElementById('btnbuscaraudio').classList.remove("backgroundfail");
	document.getElementById('btnbuscaraudio').classList.add("backgroundspecial");
	
	document.getElementById('btnbuscaraudio').innerHTML = "Buscar";
	
	document.getElementById('box_audio_actual').removeEventListener('click',reproducir_audio);
	
	audio_upload = null;
};
	
function encontrando_audio(){
	
	document.getElementById('btnbuscaraudio').classList.remove("backgroundspecial");
	document.getElementById('btnbuscaraudio').classList.add("backgroundfail");
	
	document.getElementById('btnbuscaraudio').innerHTML = "Eliminar";
	
	document.getElementById('box_audio_actual').addEventListener('click',reproducir_audio);
	
};
	
function reproducir_audio(){

	if(audio.currentSrc == ""){
		return;
	}
	audio.currentTime = 0;
	audio.play();
};
	


// funciones media imagen
	
function close_media_imagen(){
	
	document.getElementById('panel_uploader_imagen').style.display = "none";
}
	
function open_media_imagen(){
	
	// eliminar contenido existente
	document.getElementById('contenedor_imagenes').innerHTML = "";
	
	// panel visible
	document.getElementById('panel_uploader_imagen').style.display = "block";
	
	
	// ** TEXTO AUTOMATICO POR RESPUESTA
	var elementos = document.getElementById('tableanswers').getElementsByTagName("textarea");
	
	if(elementos.length > 0){
		var palabra =  elementos[0].value.replace(new RegExp(",","gi"),"");
		palabra = palabra.replace(new RegExp("\\?","gi"),"");
		document.getElementById('txtbusquedaimagen').value = palabra;
		console.log(elementos[0].value.replace(new RegExp(",","gi"),""));
		document.getElementById('descripcion_pregunta').innerHTML = elementos[0].value;
	}
		
		
	/*  TEXTO AUTOMATICO POR PREGUNTA
	
	document.getElementById('txtbusquedaimagen').value = document.getElementById('textopregunta').value.replace(new RegExp(",","gi"),"");
	document.getElementById('descripcion_pregunta').innerHTML = document.getElementById('textopregunta').value;
	*/
		
		
	iniciar_get_imagenes();
	
	// iniciar conseguir contenido
	iniciar_get_imagenes();
	
	if(contenido_audio){
		
		document.getElementById('archivo_imagen').setAttribute('accept',".mp3");
		
		document.getElementById('texto_btn_upload_media').innerHTML = "Subir audio";
		document.getElementById('texto_tipo_galeria').innerHTML = "Galeria de audios";
	}
	else{
		
		document.getElementById('archivo_imagen').setAttribute('accept',".jpg, .png");
		
		document.getElementById('texto_btn_upload_media').innerHTML = "Subir imagen";
		document.getElementById('texto_tipo_galeria').innerHTML = "Galeria de imagenes";
		
	}
	
	
}
	
	
	
function key_iniciar_get_imagenes(e){
	
	if(e.keyCode == 13){
		
		iniciar_get_imagenes();
	}
	
};

	
function iniciar_get_imagenes(){
	
	var busqueda = document.getElementById('txtbusquedaimagen').value;
	
	if(contenido_audio){
		
		get_audios(resultado_get_imagenes,busqueda);
	}
	else{
		
		get_imagenes(resultado_get_imagenes,busqueda);
	}
	
	
	
};
	
function resultado_get_imagenes(param){
	
		if(param == "error"){
			
			openAlerta("alert-danger","Ha ocurrido un error");
			return;
		}
	
		var resultado = JSON.parse(param);
		
		// guardar resultado de busqueda
		media_img_data = resultado;
	
		media_img_data.sort(function(a,b){
			if(a.nombre_imagen.length < b.nombre_imagen.length){
				return -1;
			}
			else if(a.nombre_imagen.length > b.nombre_imagen.length){
				return 1;
			}
			else{
				return 0;
			}
		});
		
		// calcular datos de paginacion
		get_paginacion_imagenes();
	
		console.log(resultado);
};
	
	
	
function get_paginacion_imagenes(){
	
	
		media_img_currentpag = 0;
		
		media_img_cantpages =  Math.ceil(media_img_data.length / media_img_maxpag);
		
		document.getElementById('media-img-resultado-from').innerHTML = media_img_currentpag;
	
		document.getElementById('media-img-resultado-to').innerHTML = media_img_cantpages;

		if(media_img_data.length < 1){
			
			document.getElementById('contenedor_imagenes').innerHTML = "No se ha encontrado ningun resultado";
		}
	
		if(media_img_cantpages <= 0){
			
			disable_buttons_img();
			return;
		}
	
	   	media_img_inicio();
	
}
	
	
function actualizar_currentpag(){
	
		document.getElementById('media-img-resultado-from').innerHTML = media_img_currentpag;
}
	
function enable_buttons_img(){

	document.getElementById('media-img-inicio').classList.remove('button-disabled');
	document.getElementById('media-img-anterior').classList.remove('button-disabled');
	document.getElementById('media-img-siguiente').classList.remove('button-disabled');
	document.getElementById('media-img-fin').classList.remove('button-disabled');
};
	
function disable_buttons_img(){

	document.getElementById('media-img-inicio').classList.add('button-disabled');
	document.getElementById('media-img-anterior').classList.add('button-disabled');
	document.getElementById('media-img-siguiente').classList.add('button-disabled');
	document.getElementById('media-img-fin').classList.add('button-disabled');
};	
	
function disable_anterior_img(){

	document.getElementById('media-img-inicio').classList.add('button-disabled');
	document.getElementById('media-img-anterior').classList.add('button-disabled');
};
	
function disable_siguiente_img(){
	
	document.getElementById('media-img-siguiente').classList.add('button-disabled');
	document.getElementById('media-img-fin').classList.add('button-disabled');
};
	
	
function media_img_inicio(){
	
	
	if(media_img_cantpages <= 0){
		return false;
	}
	

	
	
	media_img_currentpag = 1;
	
	actualizar_currentpag();
	
	dibujar_imagenes(media_img_currentpag);
	
	enable_buttons_img();
	
	disable_anterior_img();
	
	if(media_img_cantpages == 1){
		
		disable_buttons_img();
	}
	
};
	
	
function media_img_anterior(){
	
	
	if(media_img_currentpag <= 1){
		return;
	}
	
	enable_buttons_img();
	
	if((media_img_currentpag - 1) <= 1){
		disable_anterior_img();
	}

	media_img_currentpag--;
	
	actualizar_currentpag();
	
	dibujar_imagenes(media_img_currentpag);
};
	
function media_img_siguiente(){
	
	
	if(media_img_currentpag >= media_img_cantpages){
		return;
	}
	
	enable_buttons_img();
	
	if((media_img_currentpag +1) >= media_img_cantpages){
		disable_siguiente_img();
	}
	
	media_img_currentpag ++;
	
	actualizar_currentpag();
	
	dibujar_imagenes(media_img_currentpag);
};
	
function media_img_fin(){
	
	
	if(media_img_cantpages <= 0){
		return
	}
	
	media_img_currentpag = media_img_cantpages;
	
	actualizar_currentpag();
	
	dibujar_imagenes(media_img_currentpag)
	
	enable_buttons_img();
	
	disable_siguiente_img();
	
	if(media_img_cantpages == 1){
		
		disable_buttons_img();
	}
	
};
	
	
	
function dibujar_imagenes(pagina){
	
	var numero_random = new Date().getTime();
	
	var len = media_img_data.length;

	pagina -= 1;
	
	var contenedor = document.getElementById('contenedor_imagenes');
	
	contenedor.innerHTML = "";
	
	var inicio = Math.round(pagina * media_img_maxpag);
	var fin = Math.round((pagina +1)* media_img_maxpag);
	
	
	for(var i = inicio; i < fin; i++){
		
		if(!media_img_data[i]){
			
			return;
		}
		
		var boxmedia = document.createElement("div");
		var opciones = document.createElement("div");
		var drop	 = document.createElement("div");
		var icon = document.createElement("i");
		var opcionesdrop = document.createElement("ul");
		var opcionmod = document.createElement("li");
		var opciondel = document.createElement("li");
		var imagen = document.createElement("img");
		var boxnombre = document.createElement("div");
		var nombre = document.createElement("span");
		
		boxmedia.className = "boxmedia";
		
		opciones.className = "opciones";
		
		drop.className = "dropdown";
		
		icon.className = "fa fa-caret-down dropdown-toggle";
		
		icon.setAttribute("data-toggle","dropdown");
		
		opcionesdrop.className = "dropdown-menu dropdown-menu-right";
		
		boxnombre.className = "descripcion";
		
		opcionmod.innerHTML = '<a href="#">Modificar</a>';
		
		opciondel.innerHTML = '<a href="#">eliminar</a>';
		
		
		// funciones box media
		
		(function(){
			
			var elemento_imagen = imagen;
			
			var elemento_del = opciondel;
			
			var elemento_mod = opcionmod;
			
			var id_actual = media_img_data[i].id_imagen;
			
			var nombre_actual = media_img_data[i].nombre_imagen;
			
			var descripcion_actual = media_img_data[i].descripcion_imagen;
			
			
			// click normal
			elemento_imagen.addEventListener('click',function(){
					
				if(contenido_audio){
					
					
					audio.src = ruta_audios+id_actual+".mp3?cb="+numero_random;
					audio.load();
					audio.currentTime = 0;
					audio.play();
				}
				
			});
			
			// funcion doble click
			elemento_imagen.addEventListener('dblclick',function(){
				
				if(contenido_audio){
					
					audio_upload = id_actual;
					
					close_media_imagen();

					encontrando_audio();
					
					if(nueva_pregunta == 1){
			
						guardado_por_imagen = true;
						ingresar_pregunta();
					}
					
				}
				else{
					
					imagen_upload = id_actual;

					close_media_imagen();

					encontrando_imagen();

					document.getElementById('containerimagen').src = ruta_imagenes + id_actual+".jpg?cb="+numero_random;
					
					if(nueva_pregunta == 1){
			
						guardado_por_imagen = true;
						ingresar_pregunta();
					}
					
				}
				
			});
			
			// funcion eliminar 
			elemento_del.addEventListener('click',function(){
				
				if(contenido_audio){
					
					eliminar_audio(resultado_eliminar_imagen,id_actual);
				}
				else{
					
					eliminar_imagen(resultado_eliminar_imagen,id_actual);
				}
				
			});
			
			// funcion modificar
			elemento_mod.addEventListener('click',function(){
				
				open_uploading_imagen();
				
				document.getElementById('id_contenido').value = id_actual;
				
				document.getElementById('nombre_imagen').value = nombre_actual;
				document.getElementById('descripcion_imagen').value = descripcion_actual;
				
			});
			
			
		})();
		
		
		// carga de contenido media en los items
		if(contenido_audio){
			
			// cargar imagen default de audio para todos los items
			imagen.src = imagen_default_audio;
		}
		else{
			
			// cargar imagenes para todos los items
			imagen.src = "../imagenes/listas/"+media_img_data[i].ruta_imagen+".jpg?"+numero_random;
		}
		
		
		nombre.innerHTML = media_img_data[i].nombre_imagen;
		
		boxnombre.appendChild(nombre);
		opcionesdrop.appendChild(opcionmod);
		opcionesdrop.appendChild(opciondel);
		drop.appendChild(icon);
		drop.appendChild(opcionesdrop);
		opciones.appendChild(drop);
		boxmedia.appendChild(opciones);
		boxmedia.appendChild(imagen);
		boxmedia.appendChild(boxnombre);
		
		contenedor.appendChild(boxmedia);
	}
	
};
	

	
	
	
function resultado_eliminar_imagen(param){
	
	console.log(param);
	
	if(parseInt(param) == 1){
		iniciar_get_imagenes();
		openAlerta("alert-info","La imagen se elimino con exito");
	}
	else{
		
		openAlerta("alert-danger","Error eliminado la imagen");
	}
};
	
//Funciones de panel media imagen
	
	
	
	
	
	
	
function iniciar_subir_imagen(e){
	
	e.preventDefault();
	
	
	
	
	if(document.getElementById('id_contenido').value.length >0){

		if(document.getElementById('nombre_imagen').value.length < 1){
			
			openAlerta("alert-danger","Debes ingresar el nombre de el contenido a subir");
			return;
		}
		
	}else{

		if(document.getElementById('archivo_imagen').value.length < 1){
			
			openAlerta("alert-danger","Debes de seleccionar un archivo a subir");
			return;
		}
		if(document.getElementById('nombre_imagen').value.length < 1){
			
			openAlerta("alert-danger","Debes ingresar el nombre de el contenido a subir");
			return;
		}
		
	}
	
	
	var formulario = new FormData(document.getElementById('formularioimagen'));
	
	if(contenido_audio){
		
		subir_audio(resultado_subir_imagen,formulario);
	}
	else{
		
		subir_imagen(resultado_subir_imagen,formulario);
	}
	
};
	
function resultado_subir_imagen(param){
	
	if(isNaN(parseInt(param))){
		
		if(param.indexOf("extension")){
			
			openAlerta("alert-warning","Verifica que la extension de tu archivo sea la correcta");
		}else{
			
			
			openAlerta("alert-danger","Ha ocurrido un error al subir el archivo");
		}
		
	}
	else{
		
		openAlerta("alert-info","Se ha subido el archivo correctamente");
		old_imagen(param);
	}
	
	
	console.log(param);
}


function old_imagen(srcimagen){
	
	archivo_subido = srcimagen;
	
	iniciar_get_imagenes();
	
	// eliminar archivo escogido
	document.getElementById('archivo_imagen').value = "";
	
	// restringir campo de subida de imagen
	document.getElementById('archivo_imagen').setAttribute("disabled","disabled");
	
	// limpiar campo de archivo subiendo
	document.getElementById('texto_uploading').innerHTML = "";
	
	// ocultar campo de archivo subiendo
	document.getElementById('texto_uploading').style.display = "none";
	
	// ocultar boton para subir imagen
	document.getElementById('btnsubirimagen').style.display = "none";
	
	// Restringir campo de titulo
	document.getElementById('nombre_imagen').setAttribute("disabled","disabled");
	
	// restringir campo de descripcion
	document.getElementById('descripcion_imagen').setAttribute("disabled","disabled");
	

	// Mostrar contenedor de imagen
	document.getElementById("contenedor_upload_imagen").style.display = "block";
	
	if(contenido_audio){
		
		
		audio.src = ruta_audios+archivo_subido+".mp3?cb="+new Date().getTime();
		audio.load();
		
		document.getElementById("img_uploaded").src = imagen_default_audio;
	}
	else{
		
		// cargar imagen subida
		document.getElementById("img_uploaded").src = "../imagenes/listas/"+srcimagen+".jpg?"+Math.random();
	}
	
	
	// mostrar boton para nueva imagen
	document.getElementById('btnnuevaimagen').style.display = "inline-block";
};
	
function nueva_imagen(){
	
	
	// eliminar archivo escogido
	document.getElementById('archivo_imagen').value = "";
	
	// restringir campo de subida de imagen
	document.getElementById('archivo_imagen').removeAttribute("disabled");
	
	// ocultar campo de archivo subiendo
	document.getElementById('texto_uploading').style.display = "block";
	
	// ocultar boton para subir imagen
	document.getElementById('btnsubirimagen').style.display = "inline-block";
	
	// Restringir campo de titulo
	document.getElementById('nombre_imagen').removeAttribute("disabled");
	
	// limpiar campo de nombre
	document.getElementById('nombre_imagen').value = "";
	
	// restringir campo de descripcion
	document.getElementById('descripcion_imagen').removeAttribute("disabled");
	
	// limpiar campo de descripcion
	document.getElementById('descripcion_imagen').value = "";
	
	// Mostrar contenedor de imagen
	document.getElementById("contenedor_upload_imagen").style.display = "none";
	
	// mostrar boton para nueva imagen
	document.getElementById('btnnuevaimagen').style.display = "none";
	
	document.getElementById('id_contenido').value = "";
	
};
	
	
function nombre_uploadimg(){
	
	var ruta = document.getElementById('archivo_imagen').value;
	document.getElementById('texto_uploading').innerHTML = ruta;
	
	try{
		var nombre_archivo = ruta.split("\\").splice(-1)[0].split(".")[0];

		document.getElementById('nombre_imagen').value = nombre_archivo;
	}
	catch(err){
		document.getElementById('nombre_imagen').value = "";
	}
	
	check_datos_uploader();
	
};
	
function check_datos_uploader(){

	var archivo_media = document.getElementById('archivo_imagen').value;
	var nombre_media = document.getElementById('nombre_imagen').value;
	
	// modificacion
	if(document.getElementById('id_contenido').value.length < 1){
		
		if(archivo_media.length < 1 || nombre_media.length < 1){
			document.getElementById('btnsubirimagen').classList.add('button-disabled');
		}
		else{
			
			document.getElementById('btnsubirimagen').classList.remove('button-disabled');
		}
	}
	// creacion
	else{
		if(nombre_media.length < 1){
			document.getElementById('btnsubirimagen').classList.add('button-disabled');
		}
		else{
			
			document.getElementById('btnsubirimagen').classList.remove('button-disabled');
		}
	}
};
	
function open_uploading_imagen(){
	
	nueva_imagen();
	
	check_datos_uploader();
	
	document.getElementById('id_contenido').value = "";
	
	document.getElementById('panel_uploader_nueva_imagen').style.display = "block";
	
	document.getElementById('archivo_imagen').value = "";	
	
	document.getElementById('texto_uploading').innerHTML = "";	
	
	
	
	if(contenido_audio){
		
		document.getElementById('texto_tipo_uploading').innerHTML = "Subir audio";
	}
	else{
		
		document.getElementById('texto_tipo_uploading').innerHTML = "Subir imagen";
		
	}
	
}
	
function close_uploading_imagen(){
	
	document.getElementById('id_contenido').value = "";
	
	document.getElementById('panel_uploader_nueva_imagen').style.display = "none";
}
	
	
	
	
	
	
	
	

function iniciar_guardar_informacion_juego(callback){
	
	var nombre_juego = document.getElementById('nombre_juego').value;
	var idioma_juego = document.getElementById('tipo_idioma').selectedIndex;
	var tipo_juego = document.getElementById('tipo_juego').selectedIndex;
	var quiz_juego = document.getElementById('tipo_quiz').selectedIndex;
	var terminado_juego = document.getElementById('tipo_terminado').selectedIndex;
		
	var informacion = {
		
		"id_juego" : id_juego,
		"nombre" : nombre_juego,
		"idioma" : idioma_juego,
		"tipo" : tipo_juego,
		"quiz" : quiz_juego,
		"terminado" : terminado_juego
		
	};
	
	
	if(typeof(callback) == "function"){
		
		guardar_informacion_juego(callback,informacion);
	}
	else{
		
		guardar_informacion_juego(resultado_guardar_informacion_juego,informacion);
	}
	
};
	
function resultado_guardar_informacion_juego(param){
	
	cambios = false;
	
	if(parseInt(param) == 1){
		openAlerta("alert-info","Informacion guardada con exito");
	}
	else{
		openAlerta("alert-danger","No hemos podido guardar la informacion");
	}
	console.log(param);
};
	
	
function realizar_cambios(){
	cambios = true;
}

function verificar_cambios(){

	
	if(cambios == true){
		$("#modal_guardar_cambios").modal("show");
	}
	else{	
		
		if(window.close()){
			
		}
		else{
			window.location.href="home.php";
		}
		
	}
	
};
	
function guardar_cambios(){

	iniciar_guardar_informacion_juego(function(){
	
		if(window.close()){
			
		}
		else{
			window.location.href="home.php";
		}
	});
};
	

function getInfoJuego() {
	
	// conseguir id de juego por URL
	var tokengame = getvarinurl("game");
	
	//conseguir pregunta
	token_pregunta = parseInt(getvarinurl("question"));
	
	// conseguir informacion de juego en database
	get_informacion_juego(renderJuego,tokengame,true);
}
	
    
// empezar a mostrar informacion del juego
 function renderJuego(param) {
	 
	  // Informacion del juego en formato JSON [0] por array de multiples juegos
	 informacion_juego = JSON.parse(param)[0]; 
	 
	 // conseguir id de el juego
	 id_juego = informacion_juego.id_juego;
	 
	 console.log(informacion_juego);
	 
	 // mostrar informacion del header 
	 mostrar_informacion_basica();
	 
	 // crear lista de preguntas
	 mostrar_lista_preguntas();
 }

	
	
// Refresh para carga de preguntas
function iniciar_cargar_preguntas(){
	
	get_informacion_juego(render_preguntas,id_juego,true);
};
	
function render_preguntas(param){
	
	console.log("Cargo preguntas");
	informacion_juego = JSON.parse(param)[0];
	mostrar_lista_preguntas();
	
};
	
   

	
	
	
function mostrar_informacion_basica(){
	
	
	document.getElementById('nombre_juego').value = informacion_juego.nombre;
	document.getElementById('tipo_juego').selectedIndex = informacion_juego.tipo;
	document.getElementById('tipo_quiz').selectedIndex = informacion_juego.quiz;
	document.getElementById('tipo_idioma').selectedIndex = informacion_juego.idioma;
	
};
    
    

	
function mostrar_lista_preguntas(){

	var len = informacion_juego.preguntas.length;
	
	var tabla = document.getElementById("listapreguntas");
	
	tabla.innerHTML = "";
	
	for(var i = 0; i < len; i++){
		
		var trmain = document.createElement("tr");
		var tdnum = document.createElement("td");
		var tdpregunta = document.createElement("td");
		
		// verifica si es la pregunta seleccionada para remarcarla	
		if(parseInt(informacion_juego.preguntas[i].id_pregunta) == parseInt(actual_id_pregunta)){
			
			trmain.classList.add("tr-hover");
		}
		
		
		tdnum.innerHTML = (i+1);
		tdpregunta.innerHTML = informacion_juego.preguntas[i].pregunta;
		
		trmain.appendChild(tdnum);
		trmain.appendChild(tdpregunta);
		
		tabla.appendChild(trmain);
		
		
		( // auto ejecucion
			function(){
				
				var actual_indice = i;
					
				trmain.addEventListener('click',function(){
					
					// guarda el id para futuras verificaciones
					actual_id_pregunta =  informacion_juego.preguntas[actual_indice].id_pregunta;
					
					// render de pregunta con su informacion
					ver_pregunta(informacion_juego.preguntas[actual_indice]);
					
					// limpiar las preguntas seleccionadas
					limpiar_hover_preguntas();
					
					this.classList.add('tr-hover');
					
				}); // fin listener
				
				
				// verificar si existe un token pasado por URL para mostrar de inmediato
				if(token_pregunta != undefined){
					
					if(informacion_juego.preguntas[actual_indice].id_pregunta == token_pregunta){
						
						ver_pregunta(informacion_juego.preguntas[actual_indice]);
						
						actual_id_pregunta =  informacion_juego.preguntas[actual_indice].id_pregunta;
						
						trmain.classList.add("tr-hover");
					}
				}
				

				
			}
		
		)(); // fin auto ejecucion
		
		
	}
	
	token_pregunta = undefined;
	
	//document.getElementById('contenedorlistanswers').scrollTop = document.getElementById('contenedorlistanswers').scrollHeight;
	
};
	


	
function ver_pregunta(objpregunta){  // recibe objeto de pregunta
	
	set_modo_modificacion();
	
	console.log(objpregunta);
	
	lista_eliminadas = [];
	
	imagen_upload = objpregunta.imagen;
	
	audio_upload = objpregunta.audio;
	
	// cargar pregunta a textarea
	document.getElementById('textopregunta').value = objpregunta.pregunta;
	
	console.log(objpregunta.imagen);
	
	if(objpregunta.imagen != null){
		
		encontrando_imagen();
		
		document.getElementById('containerimagen').src = ruta_imagenes + objpregunta.imagen + ".jpg";
	}
	
	else{
		
		buscando_imagen();
		
		document.getElementById('containerimagen').src = imagen_default;
	}
	
	if(objpregunta.audio != null){
		
		encontrando_audio();
		
		audio.pause();
		audio.currentTime = 0;
		audio.src = ruta_audios+ objpregunta.audio+".mp3";
		audio.load();
	}
	
	else{
		
		buscando_audio();
		
		audio.src = "";
		audio.currentTime = 0;
	}
	
	
	// cargar respuestas asignadas a la pregunta
	ver_respuestas(objpregunta.respuestas);
	
};
	
	
function limpiar_hover_preguntas(){
	
	var elementos = document.querySelectorAll("tr.tr-hover");

	elementos.forEach(function(elemento){
		
		elemento.classList.remove("tr-hover");
	});
	
};
	
	
function ver_respuestas(arrayparam){
	
	var lenrespuestas = arrayparam.length;
	
	var tabla = document.getElementById("tableanswers");
	
	tabla.innerHTML = "";
	
	for(var i = 0; i < lenrespuestas; i++){
		
		var trmain = document.createElement("tr");
		var tddelete = document.createElement("td");
		var tdanswer = document.createElement("td");
		var icondelete = document.createElement("i");
		var txtanswer = document.createElement("textarea");
		
		icondelete.className = "iconbutton pointer colorfail fa fa-close";
		txtanswer.className = "texto colorcancel scrollCustom";
		txtanswer.setAttribute("spellchek","false");
		
		txtanswer.value = arrayparam[i].respuesta;
		
		
		tddelete.appendChild(icondelete);
		tdanswer.appendChild(txtanswer);
		trmain.appendChild(tddelete);
		trmain.appendChild(tdanswer);
		
		tabla.appendChild(trmain);
		
		(function(){
			
			var indice_actual = i;
			
			var toeliminate = trmain;
			
			icondelete.addEventListener('click',function(){
				
				// eliminar respuesta del DOM
				tabla.removeChild(toeliminate);
				
				// reconteo de campos de respuesta
				contar_respuestas();
			});
			
			txtanswer.addEventListener('keydown',checkContinueInput);
			
			
		})();
		
	};

	contar_respuestas();
};
	
	
	
function crear_respuesta() {

		var tabla = document.getElementById("tableanswers");	
	
		eliminar_respuestas_vacias();
			
		var trmain = document.createElement("tr");
		var tddelete = document.createElement("td");
		var tdanswer = document.createElement("td");
		var icondelete = document.createElement("i");
		var txtanswer = document.createElement("textarea");
		
		icondelete.className = "iconbutton pointer colorfail fa fa-close";
		txtanswer.className = "texto colorcancel scrollCustom";
		txtanswer.setAttribute("spellchek","false");
		
		txtanswer.value = "";
		
		tddelete.appendChild(icondelete);
		tdanswer.appendChild(txtanswer);
		trmain.appendChild(tddelete);
		trmain.appendChild(tdanswer);
		
		tabla.appendChild(trmain);
	
		txtanswer.focus();
		
		tabla.scrollTop = tabla.scrollHeight;
	
		(function(){
			
			
			icondelete.addEventListener('click',function(){
				
				// eliminar campo de respuesta de la lista
				tabla.removeChild(trmain);	
				
				// reconteo de campos
				contar_respuestas();
				
			});
			
			txtanswer.addEventListener('keydown',checkContinueInput);
			
		})();
	
	

		
	contar_respuestas();
	
};
	
	
	

	
function getRespuestas(){
	
	var elementos = document.getElementById('tableanswers').getElementsByTagName("textarea");
	
	var len = elementos.length;
	
	var arrayrespuestas = [];
	
	for(var i = 0; i < len; i++){
		
		if(elementos[i].value.length > 0){
			
			arrayrespuestas.push(elementos[i].value.trim());
		}
	}
	
	return arrayrespuestas.join("||");
};
	
	
	
function contar_respuestas(){

	var len = document.getElementById('tableanswers').getElementsByTagName("textarea").length;
	
	document.getElementById('cantidad_respuestas').innerHTML = len;
};
	
	
function eliminar_respuestas_vacias(){
		
	var elementos = document.getElementById('tableanswers').getElementsByTagName("textarea");
	
	var contenedor = document.getElementById('tableanswers');
	
	var len = elementos.length -1;
	

	for(var i = len; i >= 0; i--){
		
		if(elementos[i].value.length < 1){
			
			contenedor.removeChild(elementos[i].parentElement.parentElement);
		}
	}
	
};
	
	
	
// recuperar datos a enviar a la DB
function ingresar_pregunta(){
	
	if(!check_campos_pregunta()){
		return false;
	}
	
	var pregunta = document.getElementById('textopregunta').value.trim();
	
	var respuestas = getRespuestas();
	
	pregunta = pregunta.replace(new RegExp('"',"gi"),"'");
	respuestas = respuestas.replace(new RegExp('"',"gi"),"'");
	
	var informacion = {
		
		"id_juego" : id_juego,
		"respuestas": respuestas,
		"nueva_pregunta" : nueva_pregunta,
		"id_pregunta" : actual_id_pregunta,
		"pregunta": pregunta,
		"imagen" : imagen_upload,
		"audio" : audio_upload

	};
	
	set_pregunta_y_respuestas(confirmacion_ingreso_pregunta,informacion);
	
	
}
	
	
function check_campos_pregunta(){
	
	var pregunta = document.getElementById('textopregunta').value.trim();
	
	var respuestas = getRespuestas();
	
	if(pregunta.length < 1 || respuestas.length < 1){
		return false;
	}
	
	return true;
	
};
	
	
function confirmacion_ingreso_pregunta(param){

		if(guardado_por_imagen == false){
			
			if(param.indexOf("2") != -1){

				openAlerta("alert-danger","Ha ocurrido un error");
			}
			else{
				openAlerta("alert-success","Pregunta registrada correctamente");
				
				if(nueva_pregunta === 0){
					
					document.getElementById('contenedorlistanswers').scrollTop = document.getElementById('contenedorlistanswers').scrollHeight;
				}
			}

			set_modo_creacion();

		}
			iniciar_cargar_preguntas();

	
		console.log(param);
	
		guardado_por_imagen = false;
	
	
		
};
	
	
	
	
	
function eliminarPregunta(){
	
	eliminar_pregunta(confirm_eliminacion,actual_id_pregunta);
}
	
	
function confirm_eliminacion(param){

		set_modo_creacion();
	
		iniciar_cargar_preguntas();		
		
		if(param.indexOf("2") != -1){
			
			openAlerta("alert-danger","Ha ocurrido un error");
		}
		else{
			openAlerta("alert-success","Pregunta eliminada");
		}
};
	
	
	

	
	
	
function set_modo_creacion(){
	
	// crear campo de respuesta
	
	// modo creacion
	nueva_pregunta = 0;
	
	imagen_upload = null;
	
	audio_upload = null;
	
	// limpiar lista de preguntas existentes a eliminar
	lista_eliminadas = [];
	
	//limpiar id de pregunta actual
	actual_id_pregunta = null;
	
	// limpiar lista de respuestas
	document.getElementById('tableanswers').innerHTML = "";
	
	// cambiar texto de boton de accion para la pregunta
	document.getElementById('textobtncontrolpregunta').innerHTML = "Crear";
	
	// limpiar campo de pregunta
	document.getElementById('textopregunta').value = "";
	
	document.getElementById('btneliminarpregunta').style.display = "none";
	
	document.getElementById('btncancelarpregunta').style.display = "none";
	
	crear_respuesta();
	
	document.getElementById('textopregunta').focus();
	
	document.getElementById('textopregunta').select();
	
	// recontear preguntas
	contar_respuestas();
	
	// eliminar imagen cargada
	buscando_imagen();
	
	// eliminar audio cargado
	buscando_audio();
};
	
function set_modo_modificacion(){

	
	crear_respuesta();
	// modo modificacion
	nueva_pregunta = 1;
	
	document.getElementById('textobtncontrolpregunta').innerHTML = "Modificar";
	
	document.getElementById('btneliminarpregunta').style.display = "inline-block";
	
	document.getElementById('btncancelarpregunta').style.display = "inline-block";

	document.getElementById('textopregunta').focus();
	document.getElementById('textopregunta').select();
	
	// contar cantidad de respuestas
	contar_respuestas();
};
	
	
	
function key_ingresar_pregunta(e){
	
	if(e.keyCode == 13){
		
		ingresar_pregunta();
	}
}
	
function checkContinueInput(e){

	var key = e.keyCode;
	
	
	if((e.shiftKey) && (key == 13)){
		
		e.preventDefault();
		
		ingresar_pregunta();
	}
	
	if(key == 13){
		
		e.preventDefault();
	}
	
	
};
	
	
	
function iniciar_eliminar_juego(){
	
	eliminar_juego(confirmacion_eliminar_juego,id_juego);
};

function confirmacion_eliminar_juego(param){
	
	console.log(param);
	window.location.href = "home.php";
};
	
	
	
	
function setNuevoToken(){   // ingreso de token
    
	var param = id_juego;
    set_token_game(getActualToken,param);
    
}
    
function getActualToken(param){  // conseguir el id del token la base de datos
    
	window.open("play.php?game="+param,"_blank");
};
    
	
	
function buscar_traduccion(){

	var pregunta_actual = document.getElementById('textopregunta').value.trim();
	
	if(pregunta_actual.length < 1){
		
		openAlerta("alert-danger","Escribe una palabra valida");
		return false;
	}
	
	window.open('https://translate.google.com/?langpair=en%7Ces#es/en/'+pregunta_actual,"_blank");
};
	
	
	
	
	
	
	
function inyeccion(){
	$('#modal_set_questions').modal("show");
	
	var texto = $('#texto_inyeccion').val();
	
	texto = texto.split("\n");
	
	
	
	for(var i = 0; i < texto.length; i++){
		texto[i] = texto[i].split("\t")[1].split("-----------");
		for(var b = 0; b < texto[i].length; b++){
			
			if(texto[i][b].length > 0){
				
				texto[i][b] = texto[i][b].trim("");
				texto[i][b] = texto[i][b].split(".").join("");
			}
			else{
				texto[i][b] = "";
			}
		}
	}
	
	texto = texto.reverse();
	
	var datos = {
	
		"id_juego" : id_juego,
		"preguntas" : JSON.stringify(texto)
		
	};
	
	inyectar_preguntas(resultado_inyectar_preguntas,datos);
	
	//console.log(JSON.stringify(datos));
	
};

function resultado_inyectar_preguntas(respuesta){

	console.log(respuesta.trim());
	
};
	
	
	
	
    
};   // empieza cuando se carga la pagina



