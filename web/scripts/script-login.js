window.addEventListener('load',init_login);


function init_login() {
    
    var fieldUser = document.getElementById('fieldUser');
    var fieldPassword = document.getElementById('fieldPassword');
    
    var panelError = document.getElementById('panelerror');
    
    var btnlogin = document.getElementById('btnlogin');
    
    btnlogin.addEventListener('click',checkConnection);
    
    
    /*   Agregar funcion de verificacion de datos por tecla   */
    btnlogin.addEventListener('keyup',keycheckConnection);
    fieldUser.addEventListener('keyup',keycheckConnection);
    fieldPassword.addEventListener('keyup',keycheckConnection);
    
    
    
    clearFields(); // limpiar campos cuando se inicia
    
    
    
    
    
    
    
    
    function checkConnection(){
      
        var usuario = fieldUser.value;
        var password = fieldPassword.value;
        
        var arraycampos = [usuario,password];
        
        if(!checkEmptyFields(arraycampos)){
            
            errorHandler("2");
            return;
        }
        
        $.ajax({
            
            type:'POST',
            url: '../controladores/checkLogin.php',
            data:({user: usuario, password: password}),
            success: function(answer){
                
                console.log(answer);
                errorHandler(answer);
            }
            
            
        });
        
        
    };
    
    
    
    function keycheckConnection(e){
      
        if(e.keyCode == 13){
            checkConnection();
        }
    };
    

    
    
    
    /*  Funciones de limpieza para formularios  */
    function clearFields(){
      
        clearPassword();
        clearUser();
        closeError();
        
    };
    
    function clearPassword(){
      
        fieldPassword.value = "";
        
    };
    function clearUser(){
      
        fieldUser.value = "";
    };
    
    function showError(descripcion){
      
        panelError.style.display = "block";
        panelError.innerHTML = descripcion;
        
    };
    
    function closeError(){
        
        panelError.style.display = "none";
        panelError.innerHTML = "";
    }
    
    /*  Funciones de limpieza para formularios  */
    
    
    
    
    function errorHandler(error,callback){
      
        var error = parseInt(error);
        
        switch(error){
            
            
            case 0:{
                
                showError("Ha ocurrido un eror");
                return;
            }
            case 1:{
                
                    window.location.assign("home.php");
                return;        
            }
            case  2: {
                
                showError("Debes de ingresar todos los campos");
                return;
            }
            case 3: {
                
                showError("Email o contraseña incorrectos");
                return;
            }
        }
        
    };
    
    
};