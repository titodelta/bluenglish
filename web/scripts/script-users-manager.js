

window.addEventListener('load', init_users_manager);
                        
function init_users_manager() {
 
	
	var id_current_usuario = undefined;
	
	document.getElementById('btn_eliminar_usuario').addEventListener('click',iniciar_eliminar_usuario);
	
	document.getElementById('btn_modificar_usuario').addEventListener('click',enviar_datos);
	
	
	
	iniciar_get_usuarios();
    
	
	
	
	function iniciar_get_usuarios(){
	
		var datos = {
		
			"todos": "true"
		};
		
		get_informacion_usuario(resultado_get_usuarios, datos);
		
	};
	
	
	
	function resultado_get_usuarios(respuesta){
		
		if(JSON.parse(respuesta)){
			usuarios = JSON.parse(respuesta);
			
			console.log(usuarios);
			
			var contenedor = document.getElementById('tabla_usuarios');
			
			var len = usuarios.length;
			
			contenedor.innerHTML = "";
			
			for(var i = 0; i < len; i++){
			
				
				var tr_main = document.createElement("tr");
				var td_id = document.createElement("td");
				var td_avatar = document.createElement("td");
				var td_usuario = document.createElement("td");
				var td_apodo = document.createElement("td");
				var td_rango = document.createElement("td");
				var td_acciones = document.createElement("td");
				
				var avatar = document.createElement("img");
			
				var icon_edit = document.createElement("i");
				var icon_perfil = document.createElement("i");
				var icon_delete = document.createElement("i");
				
				icon_edit.className = "iconbutton fa fa-pencil";
				icon_perfil.className = "iconbutton fa fa-user";
				icon_delete.className = "iconbutton fa fa-trash";
				
				icon_edit.setAttribute("title","detalle de usuario");
				icon_perfil.setAttribute("title","ver perfil");
				icon_delete.setAttribute("title","eliminar usuario");
				
				icon_edit.setAttribute("data-toggle","tooltip");
				icon_perfil.setAttribute("data-toggle","tooltip");
				icon_delete.setAttribute("data-toggle","tooltip");
				
				
				avatar.setAttribute("width","40px");
				avatar.setAttribute("height","40px");
				
				td_id.innerHTML = i +1;
				td_usuario.innerHTML = usuarios[i].usuario;
				td_apodo.innerHTML = usuarios[i].nickname;
				td_rango.innerHTML = usuarios[i].rango;
			
				avatar.src = "../imagenes/avatares/"+ usuarios[i].id_avatar+".png";
				
				td_avatar.appendChild(avatar);
				
				td_acciones.appendChild(icon_edit);
				td_acciones.appendChild(icon_perfil);
				td_acciones.appendChild(icon_delete);
				
				tr_main.appendChild(td_id);
				tr_main.appendChild(td_avatar);
				tr_main.appendChild(td_usuario);
				tr_main.appendChild(td_apodo);
				tr_main.appendChild(td_rango);
				tr_main.appendChild(td_acciones);
				
				contenedor.appendChild(tr_main);
				
				
				(function(){
					
					var current_user = usuarios[i];
					
					call_click_ver_detalle.call(icon_edit,current_user);
					
					call_click_ver_perfil.call(icon_perfil,current_user);
					
					call_click_eliminar_usuario.call(icon_delete,current_user);
					
					
				})();
				
			}
			// ** FIN LOOP **
			
			
			$('[data-toggle="tooltip"]').tooltip();
			
		}
		// ** FIN RESPUESTA OK
		
	};
	
	
	
	
	
	/*  ** FUNCIONES DETALLE DE PERFIL */
	
	
	function call_click_ver_detalle(current_user){
		
		this.addEventListener('click',function(){
			
			iniciar_detalle_usuario(current_user);
		});
		
	};
	
	function iniciar_detalle_usuario(current_user){
		
		var datos = {
			"id_usuario": current_user.id_usuario
			
		};
		
		id_current_usuario = current_user.id_usuario;
		
		get_informacion_usuario(resultado_detalle_usuario, datos);
	};
	
	function resultado_detalle_usuario(respuesta){
	
		if(JSON.parse(respuesta)){
			console.log(JSON.parse(respuesta));
			$('#modal_modificar_usuario').modal("show");
			render_datos_informacion_basica(JSON.parse(respuesta)[0]);
		}
	
		
	};
	
	
			
	// *** MOSTRAR INFORMACION BASICA ***
	function render_datos_informacion_basica(informacion_jugador){
	
		document.getElementById('txt_email').value = informacion_jugador.email;
		document.getElementById('txt_usuario').value = informacion_jugador.usuario;
		document.getElementById('txt_nickname').value = informacion_jugador.nickname;
		document.getElementById('txt_password').value = informacion_jugador.password;
		document.getElementById('txt_nombre').value = informacion_jugador.nombre;
		document.getElementById('txt_apellido').value = informacion_jugador.apellido;
		document.getElementById('txt_pais').value = informacion_jugador.pais;
		document.getElementById('txt_ciudad').value = informacion_jugador.ciudad;
		document.getElementById('txt_telefono').value = informacion_jugador.telefono;
		document.getElementById('txt_fecha_nacimiento').value = informacion_jugador.fecha_nacimiento;
		document.getElementById('txt_rango').value = informacion_jugador.rango;
		
	};
	
	
		
	
	// ** ENVIAR INFORMACION BASICA ***
	function enviar_datos(){
		
		if(!checkRegister()){
			return false;
		}

		var datos_basicos = {

			"id_usuario" : id_current_usuario,
			email : document.getElementById('txt_email').value,
			usuario : document.getElementById('txt_usuario').value,
			nickname : document.getElementById('txt_nickname').value,
			password : document.getElementById('txt_password').value,
			nombre : document.getElementById('txt_nombre').value,
			apellido : document.getElementById('txt_apellido').value,
			pais : document.getElementById('txt_pais').value,
			ciudad : document.getElementById('txt_ciudad').value,
			telefono : document.getElementById('txt_telefono').value,
			fecha_nacimiento : document.getElementById('txt_fecha_nacimiento').value,
			rango : document.getElementById('txt_rango').value,

		}
	
		set_informacion_basica(resultado_set_informacion_basica,datos_basicos);
		
	};
    
    
	//** RESULTADO MODIFICACION DATOS BASICOS ***
	
    function resultado_set_informacion_basica(respuesta){
		
		$('#modal_modificar_usuario').modal("hide");
		
		iniciar_get_usuarios();
		
		console.log(respuesta);
	};
	
	
	
	
	    function checkRegister(){
	
		var email = document.getElementById('txt_email').value;
		var usuario = document.getElementById('txt_usuario').value;
		var nickname = document.getElementById('txt_nickname').value;
		var password = document.getElementById('txt_password').value;
        var rango = document.getElementById('txt_rango').value;
        
        
        var arraydatos = [email,usuario,nickname,password,rango];
        
        if(!checkEmptyFields(arraydatos)){
            
            errorHandler("2");
            return false;
        }
        

        closeError();
		return true;
        
    };
	
	
	
		
	//** EVENT HANDLER ERRORES ***
    
    function errorHandler(error,callback){
      
        var error = parseInt(error);
        
        switch(error){
            
            
            case 0:{
                
                showError("Ha ocurrido un error");
                return;
            }
            case 1:{
                
				openAlerta("alert-success","Cambios realizados con exito");
				iniciar_get_informacion_consultante();
                return;        
            }
            case  2: {
                
                showError("Debes de ingresar todos los campos marcados con un asterisco <strong>(*)</strong>");
                return;
            }
            case 3: {
                
                showError("Datos incorrectos");
                return;
            }
                
            case 4: {
                
                showError("Solo numeros, Letras y guiones permitidos");
                return;
            }
            case 5:{
                
                showError("Debes ingresar un Usuario valido");
                return;
            }
            case 6:{
                
                showError("Debes ingresar un Email valido");
                return;
            }
            case 7:{
                
                showError("El email <strong>"+ document.getElementById('txt_email').value+ "</strong> ya se encuentra registrado");
                return;
            }
            case 8:{
                
                showError("El usuario <strong>"+ document.getElementById('txt_usuario').value + "</strong> ya se encuentra registrado");
                return;
            }
            case 10:{
                
                showError("El usuario debe de contener menos de 20 caracteres");
                return;
            }
            case 11:{
                
                showError("El nickName debe de contener menos de 20 caracteres");
                return;
            }
            case 12:{
                
                showError("El nickName "+document.getElementById('txt_nickname').value+ " Ya se encuentra registrado");
                return;
            }
            case 20:{
                
                showError("El nickName debe de contener menos de 20 caracteres");
                return;
            }
			default:{
			
                showError("Ha ocurrido un error");
				return;
			}
        }
        
    };
	
	
	
	//** MOSTRAR ERRORES ***
    
    function showError(descripcion){
      
		openAlerta("alert-danger",descripcion);
        
    };
    
    function closeError(){
        
    }
	
	
	
	
	
	
		
	// ** FUNCIONES VER PERFIL **
	
	function call_click_ver_perfil(current_user){
		
		this.addEventListener('click',function(){
			
			window.location.href = "configuracion.php?profile="+current_user.id_usuario;
		});
	};
	
	
	
	
	// ** FUNCIONES ELIMINAR PERFIL **
	
	function call_click_eliminar_usuario(current_user){
		
		this.addEventListener('click',function(){
			
			$('#modal_eliminar_usuario').modal("show");
			id_current_usuario = current_user.id_usuario;
		});
		
	};
	
	function iniciar_eliminar_usuario(){
		
		var datos ={
		
			"id_usuario" : id_current_usuario
		};
		
		eliminar_usuario(resultado_eliminar_usuario, datos);
		
	};
	
	function resultado_eliminar_usuario(respuesta){
		
		$('#modal_eliminar_usuario').modal("hide");
		if(JSON.parse(respuesta)){
			
			if(JSON.parse(respuesta).type == "success"){
			
				openAlerta("alert-success","Usuario eliminado con exito");
				
				iniciar_get_usuarios();
			}
			else{
				openAlerta("alert-success","Ha ocurrido un error");
			}
		}
	};
	

	
	
};