
window.addEventListener('load',init_endgame);
                        
function init_endgame(){

	
var informacion_jugador;
	
window.addEventListener('resize',resizeHandlerEndgame);   // attach evento de resize para responsive design
    
document.getElementById('btnclosepreguntas').addEventListener('click',closePanelPreguntas); // cerrar panel de preguntas
	
document.getElementById('btncancelpreguntas').addEventListener('click',closePanelPreguntas); // cerrar panel de preguntas
	
document.getElementById('btnshowpreguntas1').addEventListener('click',function(){llenarPreguntas(1);}); // mostrar preguntas
	
document.getElementById('btnshowpreguntas2').addEventListener('click',function(){llenarPreguntas(2);}); // mostrar preguntas

document.getElementById('btnreiniciarlista').setAttribute("href", estadojuego.locacion);

document.getElementById('textoaciertos').innerHTML = estadojuego.listaganadas.length;
	
document.getElementById('textoerrores').innerHTML = estadojuego.listaperdidas.length;
	
document.getElementById('nombrejuego').innerHTML = estadojuego.nombrejuego;
	
document.getElementById('lenpreguntas').innerHTML = estadojuego.lenpreguntas;
	
document.getElementById('duracion').innerHTML = estadojuego.duracion;

document.getElementById('promedio').innerHTML = estadojuego.promedio.toFixed(2);

document.getElementById('estrellas').dibujar_estrellas(estadojuego.estrellas);

console.log(estadojuego);


resizeHandlerEndgame();

verificar_ganador();

iniciar_get_informacion_usuario();
	
delete_stream_juego(resultado_delete_stream_juego);



	

function resultado_delete_stream_juego(respuesta){
	
};
	
//*** CONSEGUIR INFORMACION JUGADOR Y MOSTRARLA ****

function iniciar_get_informacion_usuario(){

	get_informacion_usuario(resultado_get_informacion_usuario);

};

// *** GUARDAR INFORMACION OBTENIDA PARA RENDERIZAR ***
function resultado_get_informacion_usuario(param){

	informacion_jugador = JSON.parse(param)[0];


	render_datos_perfil();
};
	
	
	
function render_datos_perfil(){

	var porcentaje = Math.round((parseInt(informacion_jugador.cantidad_juegos_aprendidos) * 100) / parseInt(informacion_jugador.cantidad_juegos));


	document.getElementById('txt_perfil_aprendidas').innerHTML = informacion_jugador.cantidad_juegos_aprendidos;
	document.getElementById('txt_perfil_nickname').innerHTML = informacion_jugador.nickname;

	document.getElementById('bar_perfil_porcentaje').style.width = porcentaje+"%";
	document.getElementById('txt_perfil_porcentaje').innerHTML = porcentaje + "%";
	document.getElementById('imagen_perfil').setAttribute("src","../imagenes/avatares/"+informacion_jugador.id_avatar+".png");
	
};

	
	

	
function verificar_ganador(){
	
	var estado = estadojuego.estadojuego;
	
	var campo_ganador = document.getElementById('campo_ganador');
	
	campo_ganador.classList.remove("backgroundfail");
	campo_ganador.classList.remove("backgroundsuccess");
	

	if(estado == false){
		campo_ganador.innerHTML = "Perdiste";
		campo_ganador.classList.add("backgroundfail");
	}
	else{
		
		campo_ganador.innerHTML = "Ganaste";
		campo_ganador.classList.add("backgroundsuccess");
	}
	
};
	
   
function llenarPreguntas(param) {

	openPanelPreguntas();

	if(param == 1){
		
		llenarDatagridPreguntas(estadojuego.listaganadas);
		document.getElementById('tituloPanelPreguntas').innerHTML = "Preguntas Ganadas";
	}
	else{
		
		llenarDatagridPreguntas(estadojuego.listaperdidas);
		document.getElementById('tituloPanelPreguntas').innerHTML = "Preguntas Perdidas";
	}
	
};

	
function llenarDatagridPreguntas(param) {
	
	var len = param.length;
	
	var tabla = document.getElementById('tablaPreguntas');
	
	tabla.innerHTML = "";
	
	for(var i = 0; i < len; i++){
		
		var tr = document.createElement("tr");
		var tdpregunta = document.createElement("td");
		var tdrespuesta = document.createElement("td");
		
		tdpregunta.innerHTML = param[i].pregunta;
		tdrespuesta.innerHTML = param[i].respuesta;
		
		tr.appendChild(tdpregunta);
		tr.appendChild(tdrespuesta);
		
		tabla.appendChild(tr);
		
	} // fin for de creacion de preguntas
	
};
	
	
function closePanelPreguntas(){

	siScroll();
	document.getElementById('panelPreguntas').style.display = "none";
	
};
	
function openPanelPreguntas(){

	noScroll();
	document.getElementById('panelPreguntas').style.display = "block";
	
};
	
	
	
    
	
	
	
	
	
	
//  Responsive movile

    
function resizeHandlerEndgame(){
    
    var ancho = window.innerWidth;
    
    if(ancho < 880 ){
        
        extendProfile();
    }
    else{
        contractProfile();
    }
    
};
    
    
function extendProfile(){
    
    var perfil = document.getElementById('perfil');
    var informacion = document.getElementById('informacion');
    
    
    perfil.classList.add("responsive");
    informacion.classList.add("responsive");
    
};
    
function contractProfile(){
    
    var perfil = document.getElementById('perfil');
    var informacion = document.getElementById('informacion');
    
    perfil.classList.remove("responsive");
    informacion.classList.remove("responsive");
};
    
    
//  Responsive movile

    
    
    
    
    
};