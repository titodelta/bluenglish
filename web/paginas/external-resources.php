
        
        <!-- CSS MAIN -->
        <link rel="stylesheet" href="../estilos/estilosmain.css">

        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    

        <!-- JS MAIN -->
        <script src="../scripts/script-main.js"></script>


        <!-- Jquery -->
        <script src="../scripts/jquery.js"></script>

        <!-- Bootstrap css -->
        <link rel="stylesheet" href="../estilos/boostrap/css/bootstrap.min.css">

        <!-- Bootstrap js -->
        <script src="../estilos/boostrap/js/bootstrap.min.js"></script>

