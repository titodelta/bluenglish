c<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-create_avatar.css">
        <script src="../scripts/script-create-avatar.js"></script>

        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
                
                <!--  Titulo de aplicacion -->
 
                <div class="boxtype1 aligncenter titulotop" id="titulo">
                    
                    <span class="titulo"> Creacion de avatar </span>
                        
                </div>
                
                <!--  Titulo de aplicacion -->
                
                <br><br>
                
                
                
                <!-- inicio de panel de configuracion -->
                <div class="boxtype1 borderradius padding" id="boxmainprepared">
                
                    
                    <div>
                    
                        <p class="subtitulo"><span class="colornegro">1</span>. Escoge el nombre de tu avatar</p>
                        <div class="formulary" id="boxnombreavatar">
                        <i class="fa fa-check colorsuccess"></i>
                            <div class="right">
                                <span class="titulo tituloactive">Nombre</span>
                                <input type="text">
                                <div class="barformulary barformularynoactive"></div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="separador"></div>
                    
                    
                    <p class="subtitulo"><span class="colornegro">2</span>. Escoge la imagen de tu avatar. </p>
                    
                    <div>
                        
                        <div id="boxselectorimagen">
                        
                             <img id="" src="../imagenes/avatares/11.png">
                             <img id="" src="../imagenes/avatares/13.png">
                        
                        </div>
                        
                    
                        <div class="aligncenter">
                            <img id="mainpicture" src="../imagenes/avatares/11.png">
                        
                        </div>
                        
                    </div>
                    
                    
                    <div class="separador"></div>
                    
                    
                    <div class="button backgroundprimary colorblanco buttonblock">Continuar</div>
                    
                    
                    
                </div>
            
                <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
    
</html>