﻿<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Smart english</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-home.css">
        <script src="../scripts/script-home.js"></script>

        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <div class="contenidoapp container-fluid">
            
				<div class="row">
					<div id="boxoptionsplay" class="boxtype1 padding">


							<div class="dropdown float-right">

								<div class="button backgroundprimary buttonline colorblanco dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-globe"></i>
									<i class="fa fa-caret-down"></i>
								</div>

								<ul class="dropdown-menu pull-right">
									<li><a href="#" id="select_idioma_1">Ingles</a></li>
									<li><a href="#" id="select_idioma_2">Español</a></li>
									<li><a href="#" id="select_idioma_3">Frances</a></li>
								</ul>
							</div>

							<div class="button buttonline backgroundprimary colorblanco button-disabled" id="btn_vocabulario">Vocabulario <i class="fa fa-mortar-board"></i></div> 
							<div class="button buttonline backgroundprimary colorblanco" id="btn_inmersion">Inmersion <i class="fa fa-rocket"></i></div> 
							<div class="button buttonline backgroundprimary colorblanco" id="btn_quiz">Quiz <i class="fa fa-deaf"></i></div>  
							<div class="button buttonline backgroundspecial colorblanco" id="btn_remake">Remake </i></div> 

					</div>
				</div>
                
                <br>
			
			
			
                <div class="row">
                
                    
                    <div class="col-sm-4">
						<div id="profileuser" class="boxtype1 profileUserScreen">

							<!-- Box profile -->
							<div class="boxprofile">
									<!-- top profile -->
									<div class="titleprofile">
										<div class="level">
											<span id="txt_perfil_aprendidas">25</span>
										</div>
										<div class="name">
											<span id="txt_perfil_nickname"></span>
										</div>
									</div>
									<!-- top profile -->

									<div class="avatar">

										<img id="imagen_perfil">

									</div>

									<div class="percentgame">

										<div class="barpercentback">
											<div class="barpercentfront" id="bar_perfil_porcentaje"></div>
										</div>
										<div class="numberpercent" id="txt_perfil_porcentaje">28%</div>
									</div>

							</div>
							<!-- Box profile -->


							<div class="separador"></div>

							<!-- Box info profile -->
							<div class="boxinfoprofile">

								<a href="configuracion.php">
									<div class="titulo overflow hidden-sm" id="informacion-homwe">
										<div class="float-left"> 
											<i class="fa fa-pie-chart colorprimary"></i> <span class="colorcancel">Informacion</span>
										</div>

										<div class="float-right horizontalmargin">
											<i class="fa fa-caret-down"></i>
										</div>
									</div>
								</a>
								
								<div class="hidden-xs">
										<div class="list-group">
											<div class="list-group-item hover-background">juegos aprendidos
												<span class="badge" id="txt_juegos_aprendidos">0</span>
											</div>
											<div class="list-group-item hover-background">juegos completados 
												<span class="badge" id="txt_juegos_completados">0</span>
											</div>
											<div class="list-group-item hover-background">Palabras aprendidas 
												<span class="badge" id="txt_cantidad_palabras">0</span>
											</div>
											<div class="list-group-item hover-background">Velocidad promedio
												<span class="badge" id="txt_velocidad_promedio">0</span>
											</div>
											<div class="list-group-item hover-background">Mejor racha
												<span class="badge" id="txt_mejor_racha">0</span>
											</div>
											<div class="list-group-item hover-background">Amigos
												<span class="badge" id="txt_amigos">0</span>
											</div>
										</div>

								</div>

							</div>

							<!-- Box info profile -->


						</div>
					</div>
                    
                    
                                    
                    
                   <div class="col-sm-8">     
						<!-- Box listas -->
						<div id="contenedorlistas" class="boxtype1 listasScreen">


							<div class="inforemake" id="inforemake">

								<p class="texto">Escoge las listas que desees completar y luego preciona el boton Comenzar</p>

							</div>


							<div class="button buttonblock backgroundprimary colorblanco transladation" id="scrollUp"><i class="fa fa-caret-up"></i></div>


							<div id="scrollerlist" class="scrollCustom" draggable="true">
								<table class="tablegame">

									<tbody id="tabla_juegos">
										<!-- listas -->    
									</tbody>

								</table>

							</div>


							<div class="button buttonblock backgroundprimary colorblanco transladation" id="scrollDown"><i class="fa fa-caret-down"></i></div>



							<div class="acceptremake" id="dialogo_remake">
								<p class="quantitylist subtitulo" id="cantidad_remake"> (34) listas escogidas </p>
								<div class="boxbuttons">
									<div class="button buttonline backgroundprimary colorblanco" id="btn_aceptar_remake"> Comenzar </div>
									<div class="button buttonline backgroundcancel colorblanco" id="btn_cancelar_remake"> Cancelar </div>
								</div>
							</div>


						</div>
						<!-- Box listas -->
					
					</div>
                    
                    
                    
                    
                </div>
			 	<!-- FIN ROW-->
            
			
						<!-- MODAL GUARDAR CAMBIOS PENDIENTES -->
				<div class="modal fade" id="modal_stream_juego">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Juego sin terminar</h3>
							</div>
							<div class="modal-body">
								<p>No has terminado tu ultimo juego. Deseas continuarlo ?</p>
							</div>
							<div class="modal-footer">
								<button class="btn btn-info" data-dismiss="modal" id="btn_retomar_stream">Seguir jugando</button>
								<button class="btn btn-default" data-dismiss="modal" id="btn_cancelar_stream">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- FIN MODAL GUARDAR CAMBIOS PENDIENTES-->

			
            </div>
			<!-- FIN contenido app-->
            
        </div>
        
    </body>
    
    
</html>