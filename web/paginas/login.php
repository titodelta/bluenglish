﻿<?php
session_start();
if(isset($_SESSION["id_usuario"])){
	header("Location: home.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       
        <?php
            require_once("external-resources.php");
        ?>
        
        <link rel="stylesheet" href="../estilos/estiloslogin.css">
        <script src="../scripts/script-login.js"></script>
        
    </head>


    <body>
    
    
        <div id="mainpanel" class="boxtype1 col-xs-12 col-md-6 col-md-offset-3"> 
        
            
            <div id="maintitle" class="topmenuprimary backgroundprimary"> 
                
                <div class="logo">
                    <i class="fa fa-fort-awesome"></i>
                    <span class="marca">Bluenglish</span>
                </div>  
                
            </div>
            
            <div id="titulo-login">
                <span id="word1">INICIAR</span> <span id="word2">SESION</span>
            </div>
            
            <div id="contenido-login">
            
				<form autocomplete="off">
					
					<div class="formulary">

						<i class="fa fa-user"></i>
						<div class="righter">
							<span class="titulo tituloactive">Usuario / Email</span>
							<input type="text" id="fieldUser" tabindex="1" maxlength="40" autocomplete="off">
							<div class="barformulary barformularynoactive"></div>
						</div>

					</div>

					<div class="formulary">

						<i class="fa fa-lock"></i>
						<div class="righter">
							<span class="titulo tituloactive">Password</span>

								<input type="text" id="fieldPassword" tabindex="2" autocomplete="off">

							<div class="barformulary barformularynoactive"></div>
						</div>

					</div>


					<!-- <a class="linkprimary" href="#"> Olvide mi contrase&ntilde;a</a> -->
					<br><br>


					<div class="button buttonblock backgroundprimary colorblanco" id="btnlogin" tabindex="3">Ingresar</div>

					<br>

					<div class="alert alert-danger" id="panelerror">
						 <span>Datos incorrectos </span>
					</div>
				</form>
            </div>
            
            
            
            
                <br><br>
                
                <div class="dualcontainer">
                    <div class="dualoption option1">
                        <a class="linkprimary" href="registro.php">
                            <i class="fa fa-users"> </i>   Crear una cuenta
                        </a>
                    </div>
                </div>
            
        
        </div>
    
		
    
    </body>

</html>