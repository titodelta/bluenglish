<?php
session_start();
if(isset($_SESSION["id_usuario"])){
	header("Location: home.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <?php
            require_once("external-resources.php");
        ?>
        <link rel="stylesheet" href="../estilos/estilos-registro.css">
        <script src="../scripts/script-registro.js"></script>
        
    </head>


    <body>
    
    
        <div id="mainpanel" class="boxtype1"> 
        
            
            <div id="maintitle" class="topmenuprimary backgroundprimary"> 
                
                <div class="logo">
                    <i class="fa fa-fort-awesome"></i>
                    <span class="marca">Bluenglish</span>
                </div>  
                
            </div>
            
            <div id="titulo-login">
                <span id="word1">REGISTRO</span> <span id="word2">BLUENGLISH</span>
            </div>
            
            <div id="contenido-login">
            
				<form autocomplete="off">
				
                <div class="formulary">
                
                    <i class="fa fa-envelope" data-toggle="popover" title="Email" data-content="Maximo <strong>40</strong> caracteres" data-trigger="hover" data-placement="top" data-html="true"></i>
                    <div class="righter">
                        <span class="titulo tituloactive">Email</span>
                        <input type="text" id="fieldEmail" tabindex="1" maxlength="40">
                        <div class="barformulary barformularynoactive"></div>
                    </div>
                                
                </div>
                
                <div class="formulary">
                
                    <i class="fa fa-user" data-toggle="popover" title="Usuario" data-content="Maximo <strong>20</strong> caracteres <br>Solo letras, numeros y guiones" data-trigger="hover" data-placement="top" data-html="true"></i>
                    <div class="righter">
                        <span class="titulo tituloactive">Usuario</span>
                        <input type="text" id="fieldUser" tabindex="2" maxlength="20">
                        <div class="barformulary barformularynoactive"></div>
                    </div>
                                
                </div>
					
                <div class="formulary">
                    <i class="fa fa-user" data-toggle="popover" title="NickName" data-content="Maximo <strong>20</strong> caracteres <br>Solo letras, numeros y guiones" data-trigger="hover" data-placement="top" data-html="true"></i>
                    <div class="righter">
                        <span class="titulo tituloactive">NickName</span>
                        <input type="text" id="fieldNickName" tabindex="2" maxlength="20">
                        <div class="barformulary barformularynoactive"></div>
                    </div>          
                </div>
                
                <div class="formulary">
                    <i class="fa fa-lock"></i>
                    <div class="righter">
                        <span class="titulo tituloactive">Password</span>
                        <input type="text"id="fieldPassword" tabindex="3">
                        <div class="barformulary barformularynoactive"></div>
                    </div>       
                </div>
                
                <div class="formulary">
                    <i class="fa fa-lock"></i>
                    <div class="righter">
                        <span class="titulo tituloactive">Confirmar Password</span>
                        <input type="text" id="fieldRePassword" tabindex="4">
                        <div class="barformulary barformularynoactive"></div>
                    </div>       
                </div>
                
                <div class="button buttonblock backgroundprimary colorblanco" id="btnregistro" tabindex="5">Registrarse</div>
                
                            
                <div class="alert alert-danger" id="panelerror">
                     <span>Datos incorrectos </span>
                </div>
					
                </form>
                
            </div>
            
            
                <br>
                
                <div class="dualcontainer">
                    <div class="dualoption option1">
                        <a class="linkprimary" href="login.php">
                            <i class="fa fa-edit"> </i>   Ya tengo una cuenta
                        </a>
                    </div>
                    
                    <div class="dualoption option2">
                        <a class="linkprimary" href="#">
                               Iniciar como invitado <i class="fa fa-user-secret"> </i>
                        </a>
                    </div>
                </div>
            
        
        </div>
    
    
    </body>

</html>