<?php
session_start();

header('Expires: Thu, 01-Jan-70 00:00:01 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-creategame.css">
		<script src="../scripts/script-create-game.js"></script>
        
      <!--  
        <script src="../scripts/script-play.js"></script>
        -->

        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
                
				
				
				
				
					<!-- Panel uploading imagen -->
                <div class="backpanel" id="panel_uploader_nueva_imagen">
				

					<!-- panel white -->
					<div class="infopanel">


						<i class="fa fa-close btnclose" id="btnclose_imagen"></i>

						<div class="titulopanel">
							 <span id="texto_tipo_uploading">Subir imagen</span>
						</div>


						<div class="separador"></div>
						
						

						<div class="contenidopanel texto colorcancel">

							<div class="imagen-toupload" id="contenedor_upload_imagen">
							
								<!-- Visualizacion de archivo subido-->
								<img id="img_uploaded">
								
							</div>
							<div class="separador"></div>
						
							<!-- Formulario de upload imagen-->
							<form name="formularioimagen" id="formularioimagen" enctype="multipart/form-data" autocomplete="off">
								
								<input type="file" name="archivo_imagen" id="archivo_imagen" class="inputfile" accept="image/*"> <!-- Input file-->
								
								<label draggable="true" for="archivo_imagen" class="">
								
								<span class="button colorblanco backgroundprimary" style="display: block">Seleccionar archivo <i class="fa fa-search-plus"> </i></span>
								<span class="texto colorprimary" id="texto_uploading"></span>
								</label>


								<div class="form-group">
									<input type="text" class="form-control" placeholder="titulo" name="nombre_imagen" id="nombre_imagen">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="descripcion" name="descripcion_imagen" id="descripcion_imagen">
								</div>							
							
								<input type="hidden" value="" id="id_contenido" name="id_contenido"/>
								<!-- Btn Nueva imagen-->
								<div class="button buttonline backgroundprimary colorblanco" id="btnnuevaimagen">Nuevo</div>
								
								<!-- btn submit-->
								<button type="submit" name="btnsubirimagen" id="btnsubirimagen" class="button buttonline backgroundprimary colorblanco button-disabled">subir</button> 
								
								<!-- btn salir-->
								<div class="button buttonline backgroundcancel colorblanco" id="btnsalir_imagen">Salir</div>
							</form>
							
						</div>

						

					</div>

					<!-- panel white -->


					<!-- Panel back panelwhite para clickear y salir.   -->
					<div class="frontpanel">

					</div>
					<!-- Panel back panelwhite para clickear y salir.   -->
					
				
				</div>
				<!-- Panel uploading imagen -->
				
				
				
				
				
				
				
				
				<!-- Panel subir contenido -->
                <div class="backpanel panel_media" id="panel_uploader_imagen">
				

					<!-- panel white -->
					<div class="infopanel panel_info_media">


						<i class="fa fa-close btnclose" id="btnclosemediaimagen"></i>

						<div class="titulopanel">
							<span id="texto_tipo_galeria">Galeria de imagenes</span>
							<div class="alignleft texto" id="descripcion_pregunta"></div>
						</div>


						<div class="separador"></div>
						
						<!-- Busqueda -->
						<div class="input-group">
							
							<!-- Input busqueda de imagen-->
							<div class="input-group-addon"><i class="fa fa-search-plus"></i></div>
							<input type="text" class="form-control" placeholder="buscar" id="txtbusquedaimagen"/>
							
							<div class="input-group-btn">
								<!-- Btn busqueda imagen-->
								<div class="btn btn-info" id="btnbuscarimagenes"> Buscar</div>
							</div>
							
						</div>
						
						<div class="separador"></div>
						

						<div class="contenidopanel texto colorcancel scrollCustom" id="contenedor_imagenes">

							<!--Carga de imagenes -->			
						</div>


						<div class="separador"></div>


						<div class="overflow">

							<div class="inline-block button buttonline backgroundspecial colorblanco float-left" id="btn_uploader_img"><span id="texto_btn_upload_media">Subir imagen</span> <i class="fa fa-search-plus"></i></div>
							
							<div class="float-right" draggable="true">
								
								<div class="button buttonline backgroundprimary colorblanco" id="media-img-inicio">Inicio</div>
								<div class="button buttonline backgroundprimary colorblanco" id="media-img-anterior">Anterior</div>
								<span class="texto colorcancel"> <span id="media-img-resultado-from">1</span> de <span id="media-img-resultado-to">5</span></span>
								<div class="button buttonline backgroundprimary colorblanco" id="media-img-siguiente">Siguiente</div>
								<div class="button buttonline backgroundprimary colorblanco" id="media-img-fin">FIN</div>
							</div>
						</div>

					</div>

					<!-- panel white -->


					<!-- Panel back panelwhite para clickear y salir.   -->
					<div class="frontpanel">

					</div>
					<!-- Panel back panelwhite para clickear y salir.   -->
					
				
				</div>
				<!-- Panel subir contenido -->
				
				
				
				

				
				
				
                
                <!-- Panel top contenedor de opciones -->
             
                <div id="boxtitulo" class="boxtype1 form-inline titulotop">
                
                    <div class="optiongame form-group">
                        <span class="texto leftformulary">Nombre</span> 
						<input type="text" class="defaultformulary form-control" spellcheck="false" id="nombre_juego">
                    </div>       
                    
                    <div class="form-group">
                        <span class="texto leftformulary">Tipo</span> 
                        <select class="defaultformulary form-control" id="tipo_juego">
                            <option>Vocavulario</option>
                            <option>Inmersion</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <span class="texto leftformulary">Quiz</span> 
                        <select class="defaultformulary form-control" id="tipo_quiz">
                            <option>No</option>
                            <option>Si</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <span class="texto leftformulary">Idioma</span> 
						<select class="defaultformulary form-control" id="tipo_idioma">
							<option>ingles</option>
							<option>español</option>
							<option>frances</option>
						</select>
                    </div>
                    <div class="form-group">
                        <span class="texto leftformulary">Terminado</span> 
						<select class="defaultformulary form-control" id="tipo_terminado">
							<option>No</option>
							<option>Si</option>
						</select>
                    </div>
					
					<div class="btn btn-line backgroundprimary colorblanco" id="btnguardarinfojuego">Guardar</div>
					
                </div>

                <!-- Panel top contenedor de opciones -->
                
                <br>
				
				
				<div class="container-fluid">
			<!-- principio row -->
                <div class="row">
				
                <!-- opciones del juego-->
				
				<div class="col-md-8 col-sm-9 col-xs-8">
					
                <div class="boxtype1 padding" id="contentgame">
                	<div class="row">
						
						<div class="col-sm-2 col-xs-3">
							<div id="panelmedia">

								<div>
									<p class="texto">Imagen</p>
									<div class="contenedorimagen">
										<img src="../imagenes/interfaces/94.png" id="containerimagen"/>
									</div>
										<div class="btn btn-sm backgroundspecial btn-block colorblanco" id="btnbuscarimagen">Buscar</div>
								</div>
								<br>
								<div>
									<p class="texto">Audio</p>
									<div class="contenedorimagen" id="box_audio_actual">
										<img src="../imagenes/interfaces/83.png" id="containeraudio"/>
									</div>
										<div class="btn btn-sm backgroundspecial btn-block colorblanco" id="btnbuscaraudio">Buscar</div>
								</div>

							</div>

						</div>


						<!-- Principio contenido-->
						<div class="col-sm-10 col-xs-9">
							<div id="contenido">


								<!-- Pregunta -->
								<div id="contenedorpregunta">
									<span class="texto">Pregunta</span> <i class="iconbutton fa fa-globe" data-toggle="tooltip" title="Traduccion" data-placement="right" id="btn_traduccion"></i>
									<br>
									<textarea lang="es" id="textopregunta" class="texto colorcancel" spellcheck="true" id="textopregunta"></textarea>
								</div>
								<!-- Pregunta -->

								<div class="separador"></div>


								<!-- Respúestas -->
								<div>

									<div>
										<span>Respuestas (<span id="cantidad_respuestas">0</span>) </span> 
										<div draggable="true" class="button buttonblock backgroundprimary colorblanco" id="btnnuevarespuesta">Nueva</div>
									</div>


									<!-- tablad respuestas -->

									<div id="contenedortable" class="scrollCustom">
										<table class="tableinfo " id="tableanswers"></table> <!-- Lista de respuestas -->

									</div>

									<!-- tablad respuestas -->

									<div class="separador verticalmargin"></div>

									<!-- botones de accion -->

									<div class="alignright">
										<div draggable="true" class="button buttonline backgroundspecial colorblanco" id="btncontrolpregunta" tabindex="0">
											<i class="fa fa-check"></i>
											<span id="textobtncontrolpregunta">Crear</span>
										</div>
										<div class="button buttonline backgroundfail colorblanco" id="btneliminarpregunta">
											<i class="fa fa-trash"></i>
											<span>Eliminar</span>
										</div>
										<div class="button buttonline backgroundcancel colorblanco" id="btncancelarpregunta">
											<span>Cancelar</span>
										</div>
									</div>

									<!-- botones de accion -->


								</div>
								<!-- Respúestas -->

							</div>
						</div>
						<!-- Fin contenido-->

					</div>  
                </div>
				</div> <!-- fin bootstrap-->
                <!-- opciones del juego  1-->
				
				
					
                   
                 <!-- lista de preguntas-->
				<div class="col-md-4 col-sm-3 col-xs-4">
					<div class="boxtype1 padding" id="">

						<div class="button buttonblock backgroundprimary colorblanco" id="btn_nueva_pregunta">Nueva pregunta</div>

						<div id="contenedorlistanswers" class="scrollCustom">
							<table class="tableinfo" id="listapreguntas"></table>  <!-- lista de preguntas -->
						</div>

					</div>    
				</div>
				
                 <!-- lista de preguntas-->     
                
				
			</div>
			<!-- fin row -->
			<div>
                
                <div class="separador"></div>
                
				
				
				<!--Acciones bottom -->
				
                <div class="alignright">
					
					
					<div class="button buttonline backgroundprimary colorblanco" id="btn_jugar"><i class="fa fa-gamepad"></i>Jugar</div>
                    <div class="button buttonline backgroundfail colorblanco" data-toggle="modal" data-target="#mimodal">
						<i class="fa fa-trash"></i> 
						<span>Eliminar </span>
					</div>
                    <div class="button buttonline backgroundcancel colorblanco" id="btnsalirjuego">Salir</div>
					<!--<div class="btn btn-info" data-toggle="modal" data-target="#modal_set_questions">Inyectar</div>-->
                </div>
				
				<!--Acciones bottom -->
					
					
				
					
				<!-- inicio modal de eliminacion -->
				<div class="modal fade" id="mimodal">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Eliminar Juego</h3>
							</div>
							<div class="modal-body">
								<p>Estas seguro de que deseas eliminar este juego ?. Los cambios no se podran revertir.</p>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" data-dismiss="modal" id="btneliminarjuego">Eliminar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- fin modal de eliminacion -->
				
			
				<!-- MODAL GUARDAR CAMBIOS PENDIENTES -->
				<div class="modal fade" id="modal_guardar_cambios">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Cambios pendientes</h3>
							</div>
							<div class="modal-body">
								<p>No has guardado tus ultimos cambios. preciona guardar para salir y guardarlos</p>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" data-dismiss="modal" id="btn_guardar_cambios">Guardar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- FIN MODAL GUARDAR CAMBIOS PENDIENTES-->
				
			
				<!-- MODAL GUARDAR CAMBIOS PENDIENTES -->
				<div class="modal fade" id="modal_set_questions">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Cambios pendientes</h3>
							</div>
							<div class="modal-body">
								<p>Ingresa los datos y preciona inyectar</p>
								<div class="form-group">
									<textarea class="form-control" id="texto_inyeccion"></textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" data-dismiss="modal" id="btn_set_questions">Guardar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- FIN MODAL GUARDAR CAMBIOS PENDIENTES-->
				
			

					
					
                
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
		
    
</html>