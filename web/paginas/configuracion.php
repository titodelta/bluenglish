<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
		<link rel="stylesheet" href="../estilos/estilos-configuracion.css">
        <script src="../scripts/script-configuracion.js"></script>
		
		
        
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
 
		<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
		
		
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <div class="contenidoapp">
            
  
				<!-- TITULO -->
             
                <div id="boxtitulo" class="boxtype1 titulotop aligncenter">
                
                    <span>CONFIGURACIONES</span>
                
                </div>

                <!-- TITULO -->
				<br>

				
				
				<!-- CONFIGURACIONES-->
				
            	<div class="boxtype1 container-fluid" id="main_configuracion">
					
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1" data-toggle="tab" id="tab_perfil">Perfil</a></li>
						<li id="tab_informacion_basica" class="hidden"><a href="#tab2" data-toggle="tab">Informacion basica</a></li>
						<li id="tab_avatar" class="hidden"><a href="#tab3" data-toggle="tab">Avatar</a></li>
						<li id="tab_amigos"><a href="#tab4" data-toggle="tab">Amigos</a></li>
						<li><a href="#tab5" data-toggle="tab">Palabras</a></li>
					</ul>
					
					
					<br>
					
					<div class="padding tab-content">
						
						
						
						<!-- CONTENIDO 1 PERFIL-->
						<div id="tab1" class="tab-pane fade in active">
							
							<div class="row">
							
								<div class="col-sm-4">
								                    
										<!-- Box profile -->

										<div id="profileuser" class="boxtype1 profileUserScreen">

											<div class="boxprofile">
													<!-- top profile -->
													<div class="titleprofile">
														<div class="level">
															<span id="txt_perfil_aprendidas">25</span>
														</div>
														<div class="name">
															<span id="txt_perfil_nickname">Dextronx</span>
														</div>
													</div>
													<!-- top profile -->

													<div class="avatar">

														<img id="imagen_perfil">

													</div>

													<div class="percentgame">

														<div class="barpercentback">
															<div class="barpercentfront" id="bar_perfil_porcentaje"></div>
														</div>
														<div class="numberpercent" id="txt_perfil_porcentaje">28%</div>
													</div>

											</div>

										</div>
										<!-- Box profile -->
								</div>
								<div class="col-sm-8">
								
									<div class="list-group">
										<div class="list-group-item hover-background">juegos aprendidos
											<span class="badge" id="txt_juegos_aprendidos">0</span>
										</div>
										<div class="list-group-item hover-background">juegos completados 
											<span class="badge" id="txt_juegos_completados">0</span>
										</div>
										<div class="list-group-item hover-background">Palabras aprendidas 
											<span class="badge" id="txt_palabras_aprendidas">0</span>
										</div>
										<div class="list-group-item hover-background">Velocidad promedio
											<span class="badge" id="txt_velocidad_promedio">0</span>
										</div>
										<div class="list-group-item hover-background">Mejor racha
											<span class="badge" id="txt_mejor_racha">0</span>
										</div>
										<div class="list-group-item hover-background">Amigos
											<span class="badge" id="txt_amigos">0</span>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						<!-- CONTENIDO 1 -->
						
						<!-- CONTENIDO 2 INFORMACION BASICA-->
						<div id="tab2" class="tab-pane fade">
							<div class="row">
								
								<!-- LADO IZQUIERDO-->
								<div class="col-sm-6">
									<div class="form-horizontal">
										
										<div class="form-group">
											<label class="control-label col-sm-3" for="txt_email">
												<span class="colorprimary">*</span> 
												Email:
											</label>
											<div class="col-sm-9">
												<input type="text" id="txt_email" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="txt_usuario">
												<span class="colorprimary">*</span> 
												Usuario:
											</label>
											<div class="col-sm-9">
												<input type="text" id="txt_usuario" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="txt_nickname">
												<span class="colorprimary">*</span> 
												Apodo:
											</label>
											<div class="col-sm-9">
												<input type="text" id="txt_nickname" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="txt_password">
												<span class="colorprimary">*</span>
												Password:
											</label>
											<div class="col-sm-9">
												<input type="text" id="txt_password" class="form-control">
											</div>
										</div>

									</div>
								</div>
								<!-- LADO IZQUIERDO-->
								<div class="col-sm-6">
									<div class="form-horizontal">
										
										<div class="form-group">
											 <label class="control-label col-sm-3" for="txt_nombre">Nombre:</label>
											<div class="col-sm-9">
												<input type="text" id="txt_nombre" class="form-control">
											</div>
										</div>
										<div class="form-group">
											 <label class="control-label col-sm-3" for="txt_apellido">Apellido:</label>
											<div class="col-sm-9">
												<input type="text" id="txt_apellido" class="form-control">
											</div>
										</div>
										<div class="form-group">
											 <label class="control-label col-sm-3" for="txt_pais">Pais:</label>
											<div class="col-sm-9">
												<input type="text" id="txt_pais" class="form-control">
											</div>
										</div>
										<div class="form-group">
											 <label class="control-label col-sm-3" for="txt_ciudad">Ciudad:</label>
											<div class="col-sm-9">
												<input type="text" id="txt_ciudad" class="form-control">
											</div>
										</div>
										<div class="form-group">
											 <label class="control-label col-sm-3" for="txt_telefono">Telefono:</label>
											<div class="col-sm-9">
												<input type="text" id="txt_telefono" class="form-control">
											</div>
										</div>
										<div class="form-group">
											 <label class="control-label col-sm-8" for="txt_fecha_nacimiento">Fecha de nacimiento:</label>
											<div class="col-sm-4">
												<input type="date" id="txt_fecha_nacimiento" class="form-control">
											</div>
										</div>

									</div>
								</div>
								
							</div>
							
							<div class="alignright">
								<div class="btn btn-info btn-md" id="btn_enviar_informacion_basica">Guardar</div>
							</div>
							
						</div>
						<!-- CONTENIDO 2 -->
						
						<!-- CONTENIDO 3 AVATAR-->
						<div id="tab3" class="tab-pane fade">

						</div>
						<!-- CONTENIDO 3 -->
						
						
						<!-- CONTENIDO 4 AMIGOS-->
						<div id="tab4" class="tab-pane fade">
						
							<div class="padding" id="panelcomunidad">

									<div class="verticalmargin">
										<div class="input-group">
											<div class="form-group">
												<input type="text" placeholder="buscar usuario" class="form-control" id="btn_buscar_usuario">
											</div>
											<div class="input-group-btn">
												<div class="btn btn-info"> buscar</div>
											</div>
										</div>
									</div>

								<div class="separador"></div>


								<!-- Cierre de contenedor comunidad-->
								<div id="contenedorcomunidad" class="scrollCustom">

									<!-- COMMUNITY USERS-->
							</div>
							<!-- Cierre de contenedor comunidad-->

						</div>      
						
						</div>
						<!-- CONTENIDO 4 -->
						
						<!-- CONTENIDO 5 PALABRAS -->
						
						<div id="tab5" class="tab-pane fade">
						
							<blockquote>Palabras aprendidas : <span id="txt_cant_palabras_aprendidas">0</span></blockquote>
							
							
							<table class="table table-hover table-striped" id="contenedor_tabla_palabras">
								<thead>
									<tr>
										<th>Palabra</th> <th>Practicas</th> <th>Ultima practica</th>
									</tr>
								</thead>
								<tbody id="tabla_palabras">
								</tbody>
							</table>
							
						</div>
						
						<!-- CONTENIDO 5 -->
						
						
						
						
						<div class="alert alert-danger" id="panel_error">
                     		<span>Datos incorrectos </span>
						</div>
						
					</div>
					<!-- FIN CONTENIDO TAB -->
					<div class="separador"></div>
					
					
					<br>
				</div>
				<!-- CONFIGURACIONES-->
				
				
				
				
            </div>
            
        </div>
        
    </body>
    
    
</html>