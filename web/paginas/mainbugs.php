<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
       
       <link rel="stylesheet" href="../estilos/estilos-mainbugs.css">
		
		<script src="../scripts/script-mainbugs.js"></script>
        

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
 
		<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
		
		
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
                
                
                
                <!-- Panel top contenedor de opciones -->
             
                <div id="boxtitulo" class="boxtype1 titulotop">
                
					<div class="aligncenter">
                    	<i class="fa fa-bug"></i> <span class="subtitulo"> REPORTE DE BUGS</span>
					</div>
                
                    <div class="separador"></div>
                    <div class="container-fluid">
						<div class="row">
							<div class="button buttonline colorcancel col-xs-12 col-md-2" id="btn_sin_solucion">Sin solucion</div>
							

							<div class="button buttonline colorcancel col-xs-12 col-md-2" id="btn_pendientes">Pendientes</div>

							<div class="button buttonline colorcancel col-xs-12 col-md-2" id="btn_solucionados">Solucionados</div>

						</div>
					</div>
                </div>

                <!-- Panel top contenedor de opciones -->
                
                <br>
                
                <!-- TABLA DE BUGS -->
                <div class="boxtype1 padding table-responsive">
                
                    <table class="tableinfo tableborder" id="data_table">
                    
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Descripciones</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
						<tbody id="tabla_bugs">
							<!-- BUGS -->
						</tbody>
                    </table>
                    
                </div>      
                <!-- TABLA DE BUGS -->
              
                   
                
                
                
				
									
				
					
				<!-- inicio modal de eliminacion -->
				<div class="modal fade" id="modal_eliminar_bug">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Eliminar Reporte</h3>
							</div>
							<div class="modal-body">
								<p>Estas seguro de que deseas eliminar este reporte ?. Los cambios no se podran revertir.</p>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" data-dismiss="modal" id="btneliminarbug">Eliminar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- fin modal de eliminacion -->
				
				
				
					
					
				<!-- inicio modal de eliminacion -->
				<div class="modal fade" id="modal_cambiar_estado">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Cambio de estado</h3>
							</div>
							<div class="modal-body">
								<p>Escoge el estado actual que deseas aplicarle al reporte.</p>
								
								<div class="form-group">
									<select class="form-control" id="seleccion_estado">
										<option>Sin solucion</option>
										<option>Pendiente</option>
										<option>Solucionado</option>
									</select>
								</div>
								
							</div>
							<div class="modal-footer">
								<button class="btn btn-info" data-dismiss="modal" id="btn_cambiar_estado">Aceptar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
				<!-- fin modal de eliminacion -->
				
				
				
				
				
				
                
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
    
</html>