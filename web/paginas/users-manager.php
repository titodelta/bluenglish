<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-users-manager.css">
        <script src="../scripts/script-users-manager.js"></script>
	
        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp container-fluid">
                         
                <div id="boxtitulo" class="boxtype1 titulotop aligncenter">
                	<span class="titulo" id="nombrelista">Administracion de usuarios</span>
                </div>
				
				<br>
							
				<!-- box white -->
				<div class="boxtype1 container-fluid">

							<div id="scrollerlist" class="scrollCustom table-responsive">
								<table class="tableinfo table-bordered table" id="contenedor_tabla_usuarios">

									<thead>
										<tr>
											<th>ID</th>
											<th>avatar</th>
											<th>Usuario</th>
											<th>Apodo</th>
											<th>Rango</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody id="tabla_usuarios">

											<!--  Listas -->

									</tbody>

								</table>

							</div>

				</div>
				<!-- Box white-->
                
				
				<!--  ** MODAL ELIMINAR USUARIO **-->
				<div class="modal fade" id="modal_eliminar_usuario">
				
					<div class="modal-dialog modal-lg">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<h3>Eliminar usuario <a href="#" data-dismiss="modal" class="close">X</a></h3>
							</div>
							
							<div class="modal-body">
								<p>Estas seguro de eliminar este usuario ?. Recuerda que los cambios no se podran revertir</p>
							</div>
							
							<diV class="modal-footer">
								<div class="btn btn-info" id="btn_eliminar_usuario">Aceptar</div>
								<div class="btn btn-default" data-dismiss="modal">Cancelar</div>
							</diV>
						</div>
						
					</div>
					
				</div>
				<!--  ** MODAL ELIMINAR USUARIO **-->
                
				
				<!--  ** MODAL MODIFICAR USUARIO **-->
				<div class="modal fade" id="modal_modificar_usuario">
				
					<div class="modal-dialog modal-lg">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<h3>Informacion  de usuario<a href="#" data-dismiss="modal" class="close">X</a></h3>
							</div>
							
							<div class="modal-body">
								

																<!-- CONTENIDO 2 INFORMACION BASICA-->
								<div id="tab2" class="tab-pane">
									<div class="row">

										<!-- LADO IZQUIERDO-->
										<div class="col-sm-6">
											<div class="form-horizontal">

												<div class="form-group">
													<label class="control-label col-sm-3" for="txt_email">
														<span class="colorprimary">*</span> 
														Email:
													</label>
													<div class="col-sm-9">
														<input type="text" id="txt_email" class="form-control">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3" for="txt_usuario">
														<span class="colorprimary">*</span> 
														Usuario:
													</label>
													<div class="col-sm-9">
														<input type="text" id="txt_usuario" class="form-control">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3" for="txt_nickname">
														<span class="colorprimary">*</span> 
														Apodo:
													</label>
													<div class="col-sm-9">
														<input type="text" id="txt_nickname" class="form-control">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3" for="txt_password">
														<span class="colorprimary">*</span>
														Password:
													</label>
													<div class="col-sm-9">
														<input type="text" id="txt_password" class="form-control">
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3" for="txt_rango">
														<span class="colorprimary">*</span>
														Rango:
													</label>
													<div class="col-sm-9">
														<input type="text" id="txt_rango" class="form-control">
													</div>
												</div>

											</div>
										</div>
										<!-- LADO IZQUIERDO-->
										<div class="col-sm-6">
											<div class="form-horizontal">

												<div class="form-group">
													 <label class="control-label col-sm-3" for="txt_nombre">Nombre:</label>
													<div class="col-sm-9">
														<input type="text" id="txt_nombre" class="form-control">
													</div>
												</div>
												<div class="form-group">
													 <label class="control-label col-sm-3" for="txt_apellido">Apellido:</label>
													<div class="col-sm-9">
														<input type="text" id="txt_apellido" class="form-control">
													</div>
												</div>
												<div class="form-group">
													 <label class="control-label col-sm-3" for="txt_pais">Pais:</label>
													<div class="col-sm-9">
														<input type="text" id="txt_pais" class="form-control">
													</div>
												</div>
												<div class="form-group">
													 <label class="control-label col-sm-3" for="txt_ciudad">Ciudad:</label>
													<div class="col-sm-9">
														<input type="text" id="txt_ciudad" class="form-control">
													</div>
												</div>
												<div class="form-group">
													 <label class="control-label col-sm-3" for="txt_telefono">Telefono:</label>
													<div class="col-sm-9">
														<input type="text" id="txt_telefono" class="form-control">
													</div>
												</div>
												<div class="form-group">
													 <label class="control-label col-sm-6" for="txt_fecha_nacimiento">Fecha de nacimiento:</label>
													<div class="col-sm-6">
														<input type="date" id="txt_fecha_nacimiento" class="form-control">
													</div>
												</div>

											</div>
										</div>

									</div>

								</div>
								<!-- CONTENIDO 2 -->

								
							</div>
							
							<diV class="modal-footer">
								<div class="btn btn-info" id="btn_modificar_usuario">Guardar</div>
								<div class="btn btn-default" data-dismiss="modal">Cancelar</div>
							</diV>
						</div>
						
					</div>
					
				</div>
				<!--  ** MODAL MODIFICAR USUARIO **-->
				
				
				
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
		
    </body>
    
    
</html>