<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
		
		<?php
		
		$respuesta = str_replace("'","\'",$_POST["data"]);
		
		echo "<script>  
		
					var estadojuego = JSON.parse('$respuesta');
		
		     </script>"; 
		?>
		
		
		
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-endgame.css">
        <script src="../scripts/script-endgame.js"></script>

        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
             
                <br><br>
				
				
				
				<div class="backpanel scrollCustom" id="panelPreguntas">
    
					<div class="container"> <!-- container bootstrap -->
    				
								  <!--*** PANEL BASICO ALERT CONFIRM -->
					<div class="infopanel col-md-8 col-sm-12 col-xs-12" >


						<i class="fa fa-close close" id="btnclosepreguntas"></i>

						<div class="titulopanel">
							<i class="fa fa-trophy icontitulopanel"></i> <span id="tituloPanelPreguntas">Lista ganadas</span>
						</div>


						<div class="separador"></div>


						<div class="contenidopanel  colorcancel">

							<table class="table table-hover table-striped">
							
								<thead>
									<tr>
										<th>Pregunta</th>
										<th>Respuesta</th>
									</tr>
								</thead>
								
								<tbody id="tablaPreguntas">
									<!-- Preguntas ganadas - perdidas -->
								</tbody>
								
							</table>
							
						</div>


						<div class="separador"></div>


						<div>
							<div class="button buttonblock backgroundcancel colorblanco" id="btncancelpreguntas">SALIR</div>
						</div>

					</div>
    
	
					</div> <!-- container bootstrap -->
	
				</div>
                
                
                
                <!-- inicio de panel de configuracion -->
                <div class="boxtype1 borderradius" id="boxmainprepared">
                
                    
                    
                    <div class="boxtype1 backgroundcancel colorblanco aligncenter" id="titulo">
                    
                        <span class="titulo"> Juego terminado</span>
                        
                    </div>
                    
                    
                    
                    
                    
                    <!-- Panel de perfil y alerta de ganado o perdido-->
                    
                    <div id="perfil">

                                   <!-- Box profile -->

                        <div id="profileuser" class="boxtype1 profileUserScreen">

                            <div class="boxprofile">
                                    <!-- top profile -->
                                    <div class="titleprofile">
                                        <div class="level">
                                            <span id="txt_perfil_aprendidas">25</span>
                                        </div>
                                        <div class="name">
                                            <span id="txt_perfil_nickname">Dextronx</span>
                                        </div>
                                    </div>
                                    <!-- top profile -->

                                    <div class="avatar">

                                        <img id="imagen_perfil">

                                    </div>

                                    <div class="percentgame">

                                        <div class="barpercentback">
                                            <div class="barpercentfront" id="bar_perfil_porcentaje"></div>
                                        </div>
                                        <div class="numberpercent" id="txt_perfil_porcentaje">28%</div>
                                    </div>

                            </div>

                        </div>
                        <!-- Box profile -->
                        
                        <div class="aligncenter backgroundsuccess titulo colorblanco" id="campo_ganador">
                        
                            <span class="">Ganaste </span> <!-- GANASTE -->
                        </div>
                        
                    </div>
					
                    
                    <!-- Panel de perfil y alerta de ganado o perdido-->
                    
                    
                    
                    
                    
                    <!-- Panel de informacion y estadisticas-->
                    
                    <div id="informacion">
                    
                        <div class="boxtype1 boxprofile">
                            
                            <div class="padding aligncenter titulo">
                                <i class="fa fa-trophy"></i> <span> INFORMACION</span>
                            </div>
                            
                            <table class="tableinfo" id="boxinfogame">
                            
                                <tr>
                                    <td>Juego</td>
                                    <td class="colorprimary" id="nombrejuego"></td> <!-- Nombre juego -->
                                </tr>
                                <tr>
                                    <td>Preguntas</td>
                                    <td class="colorprimary" id="lenpreguntas">0</td>  <!-- Cantidad preguntas -->
                                </tr>
                                <tr>
                                    <td>Duracion</td>
                                    <td class="colorprimary" id="duracion">0</td>  <!-- Duracion -->
                                </tr>
                                <tr>
                                    <td>Velocidad promedio</td>
                                    <td class="colorprimary" id="promedio">0</td>  <!-- Duracion -->
                                </tr>
                                <tr>
                                    <td>Estrellas</td>
                                    <td class="colorprimary" id="estrellas"> <i class="fa fa-star colorprimary"></i> </td>  <!-- Estrellas -->
                                </tr>
                            
                            </table>
                        
                            
                            <br>
                            
                            
                            
                            <div>
                            
                                <div class="boxinfoquests" id="btnshowpreguntas1">
                                    <div class="titulo">Aciertos</div>
                                    <div class="contenido" id="textoaciertos">25</div>
                                </div>
                            
                                <div class="boxinfoquests" id="btnshowpreguntas2">
                                    <div class="titulo">Errores</div>
                                    <div class="contenido" id="textoerrores">5</div>
                                </div>
                            
                            </div>
                            
                            <div class="separador"></div>
                            <br>
							
                            <a href="#" id="btnreiniciarlista">
                            <div class="button buttonline backgroundsuccess colorblanco"> 
								<i class="fa fa-history horizontalmargin">
								</i> Reiniciar</div>
							</a>
                            
							<a href="home.php">
                                <div class="button buttonline backgroundprimary colorblanco">Terminar <i class="fa fa-mail-forward horizontalmargin"></i></div>
                            </a>
                            
                            <br>
                            <br>
                        </div>
                    </div>
                    
                    <!-- Panel de informacion y estadisticas-->
                  
                    
                    
                    
                    
                </div>
                    <!-- Fin de panel de configuracion -->
                    
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            

    </body>
    
    
</html>