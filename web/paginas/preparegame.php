<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-preparegame.css">
        <!--<script src="../scripts/script-home.js"></script>-->

        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
				
				<div>
				
             
                <div id="boxtitulo" class="boxtype1 titulotop aligncenter">
                
                    <span class="titulo">Configuracion inicial</span>
                    
                </div>
                
                
                
                
                <!-- inicio de panel de configuracion -->
                <div class="boxtype1 borderradius" id="boxmainprepared">
                
                    
                    <span>Jugaras: </span>  <span class="colornegro">Animales 2</span>
                    
                    
                    <div class="separador"></div>
                  
                    
                    <div id="buttonsort">
                        <i class="fa fa-circle colorprimary"></i><span> Desordernar </span>
                    </div>
                    
                    <div class="separador"></div>
                    
                    <div>
                        <span class="">Vidas </span> <span class="colorprimary"> 3 </span> 
                    </div>
                    <div id="boxhearts" class="primarycolor" draggable="true">
                        <i class="fa fa-heart heart"><span class="labelheart">1</span></i>
                        <i class="fa fa-heart heart"><span class="labelheart">2</span></i>
                        <i class="fa fa-heart heart"><span class="labelheart">3</span></i>
                        <i class="fa fa-heart heart"><span class="labelheart">4</span></i>
                        <i id="infiniteheart" class="fa fa-heart heart"><span id="labelinfiniteheart" class="labelheart">8</span></i>
                    </div>
                    
                
                    
                    
                    <!-- Box profile -->
                    
                    <div id="profileuser" class="boxtype1 profileUserScreen">
        
                        <div class="boxprofile">
                                <!-- top profile -->
                                <div class="titleprofile">
                                    <div class="level">
                                        <span>25</span>
                                    </div>
                                    <div class="name">
                                        <span>Dextronx</span>
                                    </div>
                                </div>
                                <!-- top profile -->
                            
                                <div class="avatar">
                            
                                    <img src="../imagenes/avatares/42.png">
                            
                                </div>
                                
                                <div class="percentgame">
                            
                                    <div class="barpercentback">
                                        <div class="barpercentfront"></div>
                                    </div>
                                    <div class="numberpercent">28%</div>
                                </div>
                            
                        </div>
                        
                    </div>
                    <!-- Box profile -->
                    
                    <div class="separador"></div>
                    
                    <div>
                    
                        <div>
                            <i class="fa fa-comments"></i>
                        </div>
                        <div>
                            <span>Obtendras </span> <span class="colorprimary">200</span> <span> monedas</span>
                        </div>
                    
                    </div>
                    
                    <div class="separador"></div>
                    
                    <div class="aligncenter">
                        <div class="button buttonline backgroundprimary colorblanco">Empezar</div>
                        <div class="button buttonline backgroundcancel colorblanco">Empezar</div>
                    </div>
                    
                    
                </div>
                    <!-- Fin de panel de configuracion -->
                
				
			</div>
				
                    
				
				
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
    
</html>