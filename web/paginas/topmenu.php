<!-- Alerta de menu principal -->
<div class="alert alert-info" id="alerta_juego">
	<div class="close">x</div>
	<span class="texto" id="contenido_alerta">Info</span>
</div>

<!-- Alerta de menu principal-->




<!-- MENU -->

<div id="topmenumain" class="topmenuprimary ocultmenu">
            
                <div class="topmenuprimary backgroundprimary"> 

                    <div class="logo">
						<a href="home.php" class="colorblanco">
							<i class="fa fa-fort-awesome"></i>
							<span class="marca">Smart english</span>
						</a>
                    </div>  
                    
                <div id="topmenuitem1" class="item active">
                    <i class="fa fa-align-justify movil"></i>
                    <span class="screen"><img width="28" height="28" id="imagen_top_profile"><span id="usuario_top_profile"></span></span>
                    <i class="fa fa-chevron-down screen"></i>
                    &nbsp;
                    
                    <ul>
                        <a class="colorcancel" href="home.php"><li><i class="fa fa-mortar-board"></i> Jugar</li></a>
                        <a class="colorcancel" href="configuracion.php"><li><i class="fa fa-child"></i>Perfil</li></a>
                        <a class="colorcancel" href="community.php"><li><i class="fa fa-users"></i> Comunidad</li></a>
                        <a class="colorcancel" href="cerrar_session.php"><li><i class="fa fa-lock"></i> Salir</li></a>
                    </ul>
                    
                </div>

               <a href="community.php"> 
				  	<div class="item" title="comunidad" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-users colornegro" id="icon_alarm_general"></i>
					</div>
				</a>
               <a href="configuracion.php"> 
				  	<div class="item" title="Perfil" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-child" id=""></i>
					</div>
				</a>
					
               <a href="#" id="btn_mis_favoritos"> 
				  	<div class="item" title="Favoritos" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-star" id=""></i>
					</div>
				</a>
					
               <a href="home.php"> 
				  	<div class="item" title="Jugar" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-mortar-board" id=""></i>
					</div>
				</a>

               <a href="mainbugs.php" data-level="2" class="hidden"> 
				  	<div class="item" title="Bugs" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-bug colorblanco"></i>
					</div>
				</a>
					
               <a href="home-manager.php" data-level="2" class="hidden"> 
				  	<div class="item" title="Editar juegos" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-pencil colorblanco"></i>
					</div>
				</a>
               <a href="users-manager.php" data-level="2" class="hidden"> 
				  	<div class="item" title="Administrar usuarios" data-toggle="tooltip" data-placement="left">
						<i class="fa fa-universal-access colorblanco"></i>
					</div>
				</a>
					
                </div>
     

</div>


<!-- MENU -->


	
	
<!-- PANEL NUEVO FAVORITO -->
<div class="modal fade" id="modal_renombrar" style="z-index:2000">
	<div class="modal-dialog">

		<div class="modal-content">

			<div class="modal-header text-center">
				<h4> MODIFICAR FAVORITO <a href="#" class="close" data-dismiss="modal">X</a></h4> 
			</div>

			<div class="modal-body">

				<div class="form-group">
					<label class="" for="txt_nuevo_nombre_favorito">Nuevo nombre</label>
					<input type="text" class="form-control" id="txt_nuevo_nombre_favorito">
				</div>

			</div>

			<div class="modal-footer text-right">
				<div class="btn btn-info" id="btn_aceptar_update_favorito">Aceptar</div>
				<div class="btn btn-default" data-dismiss="modal">Cancelar</div>
			</div>

		</div>

	</div>

</div>
<!-- PANEL NUEVO FAVORITO -->



<div class="modal fade" id="modal_favoritos">
	<div class="modal-dialog">
	
		<div class="modal-content">
		
			<div class="modal-header text-center">
				<h4> <i class="fa fa-star colorprimary"></i> Juegos favoritos  <a href="#" class="close" data-dismiss="modal">X</a></h4> 
			</div>
			
			<div class="modal-body">
				
				<table class="table-info table table-striped table-hover">
					<thead>
						<tr>
							<th>Juegos</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="tabla_favoritos">
					
						<tr>
							<td><a href="#">Animales de prueba uno</a></td>
							<td class="text-right"><i class="fa fa-pencil iconbutton"></i> <i class="fa fa-trash colorfail iconbutton"></i></td>
						</tr>
						<tr>
							<td><a href="#">Animales de prueba uno</a></td>
							<td class="text-right"><i class="fa fa-pencil iconbutton"></i> <i class="fa fa-trash colorfail iconbutton"></i></td>
						</tr>
						<tr>
							<td><a href="#">Animales de prueba uno</a></td>
							<td class="text-right"><i class="fa fa-pencil iconbutton"></i> <i class="fa fa-trash colorfail iconbutton"></i></td>
						</tr>
					</tbody>
					
				</table>
				
			</div>
			
			<div class="modal-footer text-right">
				<div class="btn btn-default" data-dismiss="modal">Cerrar</div>
			</div>
			
		</div>
		
	</div>

</div>
