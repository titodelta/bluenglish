<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        
        <?php
            require_once("external-resources.php");
        ?>
        
		<link rel="stylesheet" href="../estilos/estilos-home-manager.css"/>
		<script src="../scripts/script-home-manager.js"></script>
        

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
 
		<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
	

        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <div class="contenidoapp">
            
            
			
			
			<!-- box white -->
			<div class="boxtype1 container-fluid">
						
						<div class="form-inline verticalmargin">
						
							<div class="btn-group">
								<div class="button buttonline backgroundprimary colorblanco" id="btncrearjuego" style="inline-block">Crear Juego</i></div> 
							</div>
                    
							<div class="btn-group">
								<div class="btn btn-default active" id="btn_categoria1">Vocavulario</div>
								<div class="btn btn-default" id="btn_categoria2">Inmesion</div>
								<div class="btn btn-default" id="btn_categoria3">Quiz</div>
							</div>

							<div class="form-group vertical-margin">
									<select class="form-control" id="select_idioma">
										<option value="0">Ingles</option>
										<option value="0">Español</option>
										<option value="0">Frances</option>
									</select>
							</div>
						
							<span id="resultados_encontrados"></span>
						</div>
				
						<div class="verticalmargin">
							<div class="button buttonblock backgroundspecial colorblanco" id="btn_cancelar_change">Cancelar intercambio</div>
						</div>

                        <div id="scrollerlist" class="scrollCustom table-responsive">
                            <table class="tableinfo table-bordered table" id="contenedor_tabla_juegos">
									
								<thead>
									<tr>
										<th>ID</th>
										<th>Nombre</th>
										<th>Idioma</th>
										<th>Terminado</th>
										<th>Bugs</th>
										<th>Acciones</th>
									</tr>
								</thead>
                                <tbody id="tabla_juegos">
                                    
										<!--  Listas -->
									
                                </tbody>

                            </table>
                        
                        </div>
				
			</div>
			<!-- Box white-->
			
			
			
								
				<!-- inicio modal de eliminacion -->
				<div class="modal fade" id="modal_eliminar_juego">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Eliminar Juego</h3>
							</div>
							<div class="modal-body">
								<p>Estas seguro de que deseas eliminar este juego ?. Los cambios no se podran revertir.</p>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" data-dismiss="modal" id="btneliminarjuego">Eliminar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- fin modal de eliminacion -->
			
				<br>

			
            </div>
			<!-- Fin contenido APP -->
            
        </div>
        
    </body>
    
    
</html>