<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-play.css">
        <script src="../scripts/script-play.js"></script>

		
		<link rel="stylesheet" href="../estilos/estilos-preparegame.css"> <!--// pendiente por verificar-->
		
		
        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
				
				
				
				
				
				
				
				<!-- Menu reportar bug -->

				<div class="backpanel" id="panelreportbug">


					<!-- panel white -->
					<div class="infopanel">


						<i class="fa fa-close btnclose" id="btnclosereportbug"></i>

						<div class="titulopanel">
							<i class="fa fa-bug icontitulopanel"></i> <span>Reportar bug</span>
						</div>


						<div class="separador"></div>


						<div class="contenidopanel">

							<textarea class="texto colorcancel scrollCustom" id="texto_reportar_bug">Reporte de bug</textarea>
						</div>
						

						<div class="separador"></div>


						<div>

							<div class="button buttonline backgroundprimary colorblanco" id="btn_aceptar_reportar_bug">Aceptar</div>
							<div class="button buttonline backgroundcancel colorblanco" id="btncancelreportbug">Cancelar</div>
						</div>

					</div>

					<!-- panel white -->


					<!-- Panel back panelwhite para clickear y salir.   -->
					<div class="frontpanel">

					</div>
					<!-- Panel back panelwhite para clickear y salir.   -->

				</div>


				 <!-- Menu reportar bug --> 
				
				
				
				
				
				
				<div id="boxpreparegame">  <!-- inicio de aplicacion prepare game antes de jugar -->
				
             
                <div id="boxtitulo" class="boxtype1 titulotop aligncenter">
                
                    <i class="fa fa-gamepad" id="btn_panel_juegos" data-toggle="tooltip" title="Ver lista de juegos escogidos" data-placement="right"></i><span class="titulo" id="nombrelista">Lista</span>
                    
                </div>
                
                
                
                
                <!-- inicio de panel de configuracion -->
                <div class="boxtype1 borderradius" id="boxmainprepared">
                
					<!--Button favoritos -->
					<i class="iconbutton fa fa-star" id="btn_nuevo_favorito" data-toggle="tooltip" data-placement="left" title="agregar a favoritos"></i>
					<!--Button favoritos-->
					
					<ul class="nav nav-tabs">
						<li class="active"><a href="#info_basica" data-toggle="tab">Basica</a></li>
						<li><a href="#info_avanzada" data-toggle="tab">Avanzada</a></li>
					</ul>
					<div class="tab-content">
					
						<div class="tab-pane fade in active" id="info_basica">
							<blockquote class="collapse in" id="configuracion_basica">
								<div id="buttonsort">
									<i id="circucloOrdenar" class="fa fa-circle"></i><span> Desordernar </span>
								</div>

								<div class="separador"></div>


								<div id="boxheartsprepare" class="primarycolor" draggable="true">
									<i id="btncorazon1" class="fa fa-heart heart"><span class="labelheart">1</span></i>
									<i id="btncorazon2" class="fa fa-heart heart"><span class="labelheart">2</span></i>
									<i id="btncorazon3" class="fa fa-heart heart"><span class="labelheart">3</span></i>
									<i id="btncorazon4" class="fa fa-heart heart"><span class="labelheart">4</span></i>
									<i id="btncorazon5" class="fa fa-heart heart"><span id="labelinfiniteheart" class="labelheart">8</span></i>
								</div>
								<div>
									<span class="">Vidas </span> <span id="textoInicialVidas" class="colorprimary"> 3 </span> 
								</div>

							</blockquote>
						</div>
					
						<div class="tab-pane" id="info_avanzada">

							<blockquote>
						
								<div class="checkbox" id="box_check_config_allow">
									<label><input type="checkbox" id="check_config_allow">Bloquear configuracion a otros usuarios <i class="iconbutton fa fa-lock"></i></label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config1" checked>Mostrar preguntas</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config2" checked>Mostrar respuestas</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config3" checked>Mostrar imagen</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config4" checked>Activar audio</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config5" checked>Reproducir audio automaticamente</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config6" checked>Mostrar aciertos y errores</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config7" checked>Mostrar vidas</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" id="check_config8" checked>Mostrar progreso</label>
								</div>
								<div class="btn backgroundprimary colorblanco" id="btn_guardar_configuraciones">Guardar y empezar</div>
								
							</blockquote>
							
						</div>
						
					</div>
					

                

                    
                    
                    <!-- Box profile -->
                    
                    <div id="profileuser" class="boxtype1 profileUserScreen">
        
                        <div class="boxprofile">
                                <!-- top profile -->
                                <div class="titleprofile">
                                    <div class="level">
                                        <span id="txt_perfil_aprendidas"></span>
                                    </div>
                                    <div class="name">
                                        <span id="txt_perfil_nickname"></span>
                                    </div>
                                </div>
                                <!-- top profile -->
                            
                                <div class="avatar">
                            
                                    <img id="imagen_perfil">
                            
                                </div>
                                
                                <div class="percentgame">
                            
                                    <div class="barpercentback">
                                        <div class="barpercentfront" id="bar_perfil_porcentaje"></div>
                                    </div>
                                    <div class="numberpercent" id="txt_perfil_porcentaje">%</div>
                                </div>
                            
                        </div>
                        
                    </div>
                    <!-- Box profile -->
                    

                    
                    <div class="separador"></div>
                    
                    <div class="aligncenter">
                        <div class="button buttonline backgroundprimary colorblanco" id="btnEmpezar">Empezar</div>
                        <div class="button buttonline backgroundcancel colorblanco" id="btnCancelar">Cancelar</div>
                    </div>
                    
                    
                </div>   <!-- fin panel de configuracion de lista -->
                
				
			</div>
                     <!-- inicio de aplicacion prepare game antes de jugar -->
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
                
                <!-- Panel top contenedor de opciones -->
				<div id="boxplaying">
				
				
                <div id="boxtitulo" class="boxtype1 titulotop">
                
                    <div class="leftmenu" id="leftmenu">
                        <a href="home.php"><i class="fa fa-mail-reply colorspecial" data-toggle="tooltip" title="volver al inicio" data-placement="right"></i></a>
                        <span class="titulo" id="nombrelistaplaying"> Lista</span>
                    </div>
                    
                    <div class="rightmenu" id="rightmenu">
                        <i class="fa fa-gavel hidden" id="btn_reparar_bug" data-toggle="tooltip" title="Editar pregunta" data-placement="right" data-level="2"></i>
                        <i class="fa fa-bug" id="btnreportbug" data-toggle="tooltip" title="Reportar bug" data-placement="left"></i>
                        <i class="fa fa-undo" id="btnresetgame" data-toggle="tooltip" title="Reiniciar juego" data-placement="left"></i>
                        <i class="fa fa-gear" id="btnconfiguregame" data-toggle="tooltip" title="configurar juego" data-placement="left"></i>
                    </div>
                    
                </div>

                <!-- Panel top contenedor de opciones -->
                
                
                
                
                <!-- inicio de panel de configuracion -->
                <div class="boxtype1 borderradius" id="boxmainplay">
                
                    
                    <div>
                    
						<div class="container-fluid hidden" id="contenedor_progreso">
						
						<div class="row margin-auto">
							
							<div id="contenedorstatus" class="col-xs-6 col-sm-9">

								<div id="status"></div>
							</div>
                    
                        
                            <div id="countstatus" class="col-xs-6 col-sm-3">
                                <span id="textProgress">1900 / 2000</span>
                            </div>
						</div>
						
						</div>
						
                        
                        <div style="overflow:hidden">
                            <!--  Corazones -->
                            <div class="colorfail verticalmargin hidden" id="boxhearts">
                                <i id="corazon1" class="fa fa-heart"></i>
                                <i id="corazon2" class="fa fa-heart"></i>
                                <i id="corazon3" class="fa fa-heart"></i>
                                <i id="corazon4" class="fa fa-heart"></i>
                            </div>
                            <!--  Corazones -->
                            
                            <!--  Estrellas -->
                            <div class="colorprimary verticalmargin" id="boxstars">
                                <i class="fa fa-star" id="estrella1"></i>
                                <i class="fa fa-star" id="estrella2"></i>
                                <i class="fa fa-star" id="estrella3"></i>
                            </div>
                            <!--  Estrellas -->
                            
                        </div>
                        
                        
                        
                    </div>
                    
                    
                    <div class="separador" style="clear:both"></div>
                    
                    <div id="contenedor-interactivo">
                    
                        <div id="estadisticas" class="float-left hidden">
                            
                            <div title="Aciertos" data-toggle="tooltip" data-placement="right">
                                <i class="fa fa-thumbs-up"></i> <span id="txtganadas"></span>  <!-- ganadas -->
                            </div>
                            <div title="Errores"  data-toggle="tooltip" data-placement="right">
                                <i class="fa fa-thumbs-down"></i> <span id="txtperdidas"></span>  <!-- perdidas -->
                            </div>
                            
                        </div>
                        <div id="sonido" class="colorprimary float-right">
                            <i class="fa fa-volume-up" id="btn_volumen_1"></i>
                            <i class="fa fa-volume-down" id="btn_volumen_2"></i>
                            <div id="animate-circle"></div>
                        </div>
                    
                    </div>
                    
                    
                    <img src="../imagenes/listas/default.jpg" width="100" style="position: absolute;display:none">
                    <!-- Contenedor de imagen central -->
                    <div id="boximage" class="aligncenter">
                    
                            <img id="imagenplay" alt="">
                        
                    </div>
                    <!-- Contenedor de imagen central -->
                    
                    
                    <div id="question"></div> <!-- campo donde se dibuja la pregunta -->
                    
                    <div id="answer">
						
						<!-- ICONO INDICADOR DE PREGUNTA -->
						<i class="fa fa-question-circle colorspecial hidden" id="icon_question"></i>
						
                        <textarea spellcheck="false" class="text" id="reply" tabindex="1" autofocus="autofocus"></textarea>  <!-- Campo de respuesta-->
                    
                    </div>
                    
                    
                            <div tabindex="2" class="button buttonblock backgroundprimary colorblanco" id="buttonacept">Check</div> <!-- boton aceptar -->
                    
							
                    
                </div>
                    <!-- Fin de panel de configuracion -->
                    
                <br>
                
                <table class="tableinfo" id="tableresponses">
                
                    <tr class="">
                        <td class="" id="texto_ganador">GANASTE</td>
                        <td>
                        
                            <ul id="listarespuestas" class=""></ul> <!-- Lista de respuestas -->
                        </td>
                        
                    </tr>
                
                </table>
                
                
				</div>
				
				<!-- fin box playing -->
				
				
				
				
				<div class="modal fade" id="modal_juegos">
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" data-dismiss="modal" class="close">x</a>
								
								<div class="titulo aligncenter">
									<i class="fa fa-trophy icontitulopanel"></i> 
									<span id="tituloPanelPreguntas">Juegos a completar</span>
								</div>

								
							</div>
							
							<div class="modal-body">
								<table class="table table-hover table-striped">

									<thead>
										<tr>
											<th>Juego</th>
											<th>Preguntas</th>
										</tr>
									</thead>

									<tbody id="tablaPreguntas">
										<!-- Lista de juegos a completar -->
									</tbody>

								</table>
								<table class="table table-hover table-striped">

									<thead>
										<tr>
											<th>Total</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td id="txt_total_preguntas">20</td>
										</tr>
									</tbody>

								</table>
							</div>
							
							<div class="modal-footer">
								<div class="btn btn-info" data-dismiss="modal">Salir</div>
							</div>
							
						</div>
						
					</div>
				
				</div>
				
				
				
                <!-- PANEL NUEVO FAVORITO -->
				<div class="modal fade" id="modal_nuevo_favorito">
					<div class="modal-dialog">

						<div class="modal-content">

							<div class="modal-header text-center">
								<h4> Agregar a mis favoritos <a href="#" class="close" data-dismiss="modal">X</a></h4> 
							</div>

							<div class="modal-body">

								<div class="form-group">
									<label class="" for="nombre_juego">Escoge un nombre que describa tu juego</label>
									<input type="text" class="form-control" id="txt_nuevo_favorito">
								</div>
								
							</div>

							<div class="modal-footer text-right">
								<div class="btn btn-info" id="btn_aceptar_nuevo_favorito">Agregar</div>
								<div class="btn btn-default" data-dismiss="modal">Cancelar</div>
							</div>

						</div>

					</div>

				</div>
                <!-- PANEL NUEVO FAVORITO -->
			
				
				
				
                <!-- PANEL ELIMINAR FAVORITO -->
				<div class="modal fade" id="modal_eliminar_favorito">
					<div class="modal-dialog">

						<div class="modal-content">

							<div class="modal-header text-center">
								<h4> ELIMINAR DE MIS FAVORITOS <a href="#" class="close" data-dismiss="modal">X</a></h4> 
							</div>

							<div class="modal-body">

								<p>Estas seguro de que deseas eliminar este juego de tu lista de favoritos ?.</p>
								
							</div>

							<div class="modal-footer text-right">
								<div class="btn btn-info" id="btn_eliminar_nuevo_favorito">Agregar</div>
								<div class="btn btn-default" data-dismiss="modal">Cancelar</div>
							</div>

						</div>

					</div>

				</div>
                <!-- PANEL ELIMINAR FAVORITO -->
				
                
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
		
    </body>
    
    
</html>