<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp container-fluid">
            

                <canvas width="200" height="200" id="prueba"></canvas>
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
    
</html>