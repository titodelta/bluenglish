<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-community.css">
         
        <script src="../scripts/script-community.js"></script>
        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp container-fluid">
            

                <!-- Row Comunidad - Chat -->
				<div class="row">
					
					
					<div class="col-md-5">
						<!-- COMUNIDAD-->
						<div class="padding" id="panelcomunidad">

								<ul class="nav nav-tabs texto">
									<li class="active" id="tab_todos"><a href="#">Todos</a></li>
									<li id="tab_amigos"><a href="#">Amigos</a></li>
								</ul>

								<div class="verticalmargin">
									<div class="input-group">
										<div class="form-group">
											<input type="text" placeholder="buscar usuario" class="form-control" id="btn_buscar_usuario">
										</div>
										<div class="input-group-btn">
											<div class="btn btn-info"> buscar</div>
										</div>
									</div>
								</div>

							<div class="separador"></div>


							<!-- Cierre de contenedor comunidad-->
							<div id="contenedorcomunidad" class="scrollCustom">

									<!-- COMMUNITY USERS-->
							</div>
							<!-- Cierre de contenedor comunidad-->

						</div>      
						<!-- COMUNIDAD -->
					</div>
					
					
					<!-- ***  CHAT *** -->
					<div class="col-md-7">
						
						<div id="panel_chat">
							
							<!-- HEADER CHAT -->
							<div class="header-chat">
								
									<!-- CHAT GENERAL -->
									<div class="box_user_chat" id="btn_chat_general">
										<img src="../imagenes/avatares/12.png" width="45" height="45">
										<span class="nombre_usuario">Todos</span>
									</div>
								
								<!-- Lista Usuarios on chat -->
								<div class="top_chat scrollCustom" id="list_chat_users">

								</div>
								<!-- Lista Usuarios on chat -->

							
							</div>
							<!-- HEADER CHAT -->
							
							<div class="clear"></div>
							
							<!-- BODY CHAT-->
							<div class="body-chat">
								
								<div class="current-user">
									<img width="45" height="45" id="avatar_current_chat">
									<span class="nombre" id="name_current_chat"></span>
								</div>
								
								<div class="separador"></div>
								
								
								<!-- MENSAJES -->
								<div class="content-message-chat padding scrollCustom" id="panel_mensajes">
									
								</div>
								<!-- MENSAJES -->
								
								
								<div class="separador"></div>
								
								
								
								<div class="reply-chat">
									<div class="form-group">
									
										<textarea class="form-control" id="txt_mensaje_chat"></textarea>
									</div>
									
									<div class="btn btn-block btn-info" id="btn_enviar_mensaje">Enviar</div>
								</div>
								
							</div>
							<!-- BODY CHAT -->
							
							
							
						</div>
					</div>
					<!-- *** CHAT *** -->
					
					
				</div>
                <!-- Row Comunidad - Chat -->
                

              
                
                
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
    
</html>