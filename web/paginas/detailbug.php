<?php
session_start();

if(!isset($_SESSION["id_usuario"])){
	header("Location: login.php");
}
?>
<!DOCTYPE html>

<html>


    <head>
        <title>Bluenglish</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width"/>
        

        
        <?php
            require_once("external-resources.php");
        ?>
        
        
        <link rel="stylesheet" href="../estilos/estilos-detail-bug.css">
        
        <script src="../scripts/script-detailbug.js"></script>
        
        
    </head>


    <body class="scrollCustom">
    
        <div class="contenedormain">
        
            <!--  MENU TOP -->
            
            <?php
                require_once("topmenu.php");
            ?>
            
            <!--  MENU TOP -->
            
            
            <!--  Contenido aplicacion  individual-->
            
            <div class="contenidoapp">
            
                
                
                
                <!-- Panel top contenedor de opciones -->
             
                <div id="boxtitulo" class="boxtype1 titulotop">
                
                    <i class="fa fa-mail-reply iconbutton" id="btn_cerrar_detalle"></i> <span class="subtitulo"> DETALLE DE BUG ( <span id="titulo_id_bug"></span>)</span>
                
                </div>

                <!-- Panel top contenedor de opciones -->
                
                <br>
                
                <!-- TABLA DE BUGS -->
                <div class="boxtype1 padding container-fluid">
                
                    
                    
                    <div id="optionsdetail" class="alignright">
                                <i class="fa fa-gavel iconbutton colorspecial" id="icon_editar"></i>
                                <i class="fa fa-retweet iconbutton" id="icon_cambiar_estado"></i>
                                <i class="fa fa-trash iconbutton" id="icon_eliminar"></i>
                    </div>
                    
                    <div id="tabsdetail" class="alignleft">
                        <span class="tab tabactive colorprimary" >Informacion</span>
                        <span class="tab">Historial</span>
                    </div>
                    
                    <div class="separador" style="clear:both"></div>
                    
                    
                    <div class="row">
                        <!-- Seccion de listado de informacion sobre el bug -->
                        <div class="col-md-6" id="informacionbug"> 
                             <table class="tableinfo tableborder alignleft">
                    
                                <tr>
                                    <td>ID</td>
                                    <td id="campo_id">3818281</td>
                                </tr>
                                <tr>
                                    <td>Fecha</td>
                                    <td id="campo_fecha">2016-02-01</td>
                                </tr>
                                <tr>
                                    <td>Hora</td>
                                    <td id="campo_hora">20:18</td>
                                </tr>
                                <tr>
                                    <td>Ultima modificacion</td>
                                    <td id="campo_modificacion">2016-02-03 20:30</td>
                                </tr>
                                <tr>
                                    <td>Usuario</td>
                                    <td id="campo_usuario">Dextronx</td>
                                </tr>
                                <tr>
                                    <td>Lista</td>
                                    <td id="campo_nombre_juego">Animales 2</td>
                                </tr>
                                <tr>
                                    <td>Pregunta</td>
                                    <td id="campo_id_pregunta">5</td>
                                </tr>
                                <tr>
                                    <td>Estado</td>
                                    <td id="campo_estado">Solucionado</td>
                                </tr>
                        
                            </table>
                        </div>
                        <!-- Seccion de listado de informacion sobre el bug -->
                        
                        <div class="col-md-6" id="descripcionbug"> 
                        
                            <div class="subtitulo colorprimary">Descripcion</div>
                            <br>
                            
                            <textarea class="colorcancel" spellcheck="false" id="campo_descripcion" readonly="readonly">This is the bug description</textarea>
                            
                        </div>
                        
                        
					</div>
                        
                    
                    
                </div>      
                <!-- TABLA DE BUGS -->
              
                   
				
				
					
				<!-- inicio modal de eliminacion -->
				<div class="modal fade" id="modal_eliminar_bug">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Eliminar Reporte</h3>
							</div>
							<div class="modal-body">
								<p>Estas seguro de que deseas eliminar este reporte ?. Los cambios no se podran revertir.</p>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" data-dismiss="modal" id="btneliminarbug">Eliminar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
					<!-- fin modal de eliminacion -->
				
				
				
					
					
				<!-- inicio modal de eliminacion -->
				<div class="modal fade" id="modal_cambiar_estado">
				
					<div class="modal-dialog">
					
						<div class="modal-content">
						
							<div class="modal-header">
								<a href="#" class="close" data-dismiss="modal">x</a>
								<h3>Cambio de estado</h3>
							</div>
							<div class="modal-body">
								<p>Escoge el estado actual que deseas aplicarle al reporte.</p>
								
								<div class="form-group">
									<select class="form-control" id="seleccion_estado">
										<option>Sin solucion</option>
										<option>Pendiente</option>
										<option>Solucionado</option>
									</select>
								</div>
								
							</div>
							<div class="modal-footer">
								<button class="btn btn-info" data-dismiss="modal" id="btn_cambiar_estado">Aceptar</button>
								<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
							</div>
							
						</div>
						
					</div>
				
				</div>
				<!-- fin modal de eliminacion -->
				
				
			
                
                
                
                
                    
            </div>
            
            <!--  Contenido aplicacion  individual-->
            
            
        </div>
            
        
    </body>
    
    
</html>